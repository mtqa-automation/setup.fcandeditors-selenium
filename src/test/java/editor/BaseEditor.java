package editor;

import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.gf.CommonWebEditor.WE_MainPage;
import com.gf.WEBase.ChapterEditorPage;
import com.gf.WEBase.WEB_HomePage;
import com.gf.common.CommonScript;
import com.gf.common.GlobalVaraibles;
import com.gf.common.RandomValueGenerator;
import com.gf.common.SFCReportCategory;
import com.gf.setupFC.LoginPage;

import setupFC.Specifications;

@Listeners(com.gf.common.TestNGExtendedListners.class)	
public class BaseEditor {

	@BeforeClass()
	public static void PreSetup_CreateSpec() {
		GlobalVaraibles.SuiteName = "BaseEditor";
		// Specifications.SFC_SP002__CreateSpec_EditType_ContentChange();
	}

	@BeforeMethod
	public void GenerateRandomValues() {
		RandomValueGenerator.AssignRandomValues();
	}

	
	@AfterClass
	public void ResetSuite() {
		CommonScript.SuiteReportEnd();

	}
	

	@Test(groups = "WEBASE,Smoke", enabled = false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE001__ValidateErrorMessageForMandateFields() {
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.ClickOnValidateButton();
		WEB_HomePage.ValidateChangeDescription_ErrorMsg();
		WEB_HomePage.ValidateChangeReason_ErrorMsg();
		WEB_HomePage.ValidatePurpose_ErrorMsg();
		WEB_HomePage.ValidateScope_ErrorMsg();
	}
	
	@Test(groups = "WEBASE,Smoke", enabled = false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Smoke")
	public static void WEBASE002__Common_VerifySpecification_Headerdetails_fromWebeditor() {
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.VerifySpecMetaInformation();
		
	}
	
	@Test(groups = "WEBASE,smoke", enabled = false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Smoke")
	public static void WEBASE003__common_Validate_MainViewDetails() {
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.ClickOnValidateButton();
		WEB_HomePage.EnterPCRBValue(GlobalVaraibles.Input.get("PCRB"));
		WEB_HomePage.EnterChangeDescriptionValue(GlobalVaraibles.Input.get(""));
		WEB_HomePage.EnterChangeReasonValue(GlobalVaraibles.Input.get("Change-Reason"));
		WEB_HomePage.EnterPurposeValue(GlobalVaraibles.Input.get("Change-Desc"));
		WEB_HomePage.EnterScopeValue(GlobalVaraibles.Input.get("Scope"));
		WEB_HomePage.EnterDefinitionValue(GlobalVaraibles.Input.get("Definitions"));
		WEB_HomePage.ClickOnValidateButton();
		WEB_HomePage.ValidateChangeDescription();
		WEB_HomePage.ValidateChangeReason();
		WEB_HomePage.ValidatePurpose();
		WEB_HomePage.ValidateScope();

	}

	@Test(enabled=false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE004__Common_ValidateAddandRemovereference()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.ClickOnReferenceButton();
		WEB_HomePage.AddReference_SearchSpecID(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.AddReference_VerifyFirstRecord(GlobalVaraibles.Input.get("Parameter2"));
		WEB_HomePage.AddReference_ClickonOkButton();
		WEB_HomePage.VerifyAddedSpecReferenceName(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.VerifyReferenceViewOption(GlobalVaraibles.Input.get("Parameter1"));
		WE_MainPage.SwitchToNewWindow();
		WE_MainPage.GetCurrentWindowTitle();
		WE_MainPage.CloseCurrentWindow();
		WEB_HomePage.ReferenceRemoveAndCancel(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.ReferenceRemoveAndConfirm(GlobalVaraibles.Input.get("Parameter1"));
		
	}
	@Test(enabled=false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE005__Common_ValidateAddandRemoverePLMference()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.ClickOnPLMReferenceButton();
		WEB_HomePage.AddPLMReference(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.VerifyAddedSpecPLMReferenceName(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.PLMReferenceRemoveAndCancel(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.PLMReferenceRemoveAndConfirm(GlobalVaraibles.Input.get("Parameter1"));
		
	}
	@Test(enabled=false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE006__Common_ValidateAddandRemoveAttachment()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.ClickOnAttachmentButton();
		WEB_HomePage.UploadFile(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.VerifyAddedSpecAttachmentName(GlobalVaraibles.Input.get("Parameter2"));
		WEB_HomePage.VerifyAttachmentViewOption(GlobalVaraibles.Input.get("Parameter2"));
		WE_MainPage.SwitchToNewWindow();
		WE_MainPage.GetCurrentWindowTitle();
		WE_MainPage.CloseCurrentWindow();
		WEB_HomePage.AttachmentRemoveAndCancel(GlobalVaraibles.Input.get("Parameter2"));
		WEB_HomePage.AttachmentRemoveAndConfirm(GlobalVaraibles.Input.get("Parameter2"));
	}
	
	@Test(enabled=false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE007__Common_ValidateAddandRemoveKeywords()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.ClickOnKeyWordButton();
		WEB_HomePage.AddKeyWords(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.VerifyAddedSpecKeywordName(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.KeywordRemoveAndCancel(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.KeywordRemoveAndConfirm(GlobalVaraibles.Input.get("Parameter1"));
		
	}
	@Test(enabled=false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE007__Common_AddAndRemoveChapter()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.AddNewChapter();
		WEB_HomePage.ClickOnValidateButton();
		WEB_HomePage.ValidateInvalidChapterTitle();
		WEB_HomePage.EnterChapterTitle("5","Siddharth");
		WEB_HomePage.EnterChapterDesc("5","Testing in mtqa");
		WEB_HomePage.ClickOnValidateButton();
		WEB_HomePage.ValidatValidChapterTitle();
		WEB_HomePage.DeleteChapter("5");		
	}
	
	@Test(enabled=false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE007__AddChapterAboveAndBelow()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WE_MainPage.ScrollToBottom();
		WEB_HomePage.AddNewChapter();
		WEB_HomePage.EnterChapterTitle("5","WEBASE007");
		WEB_HomePage.EnterChapterDesc("5","Testing in mtqa Normal Chapter");
		WEB_HomePage.AddChapterAbove("5.");
		WEB_HomePage.EnterChapterTitle("5","WEBASE007__AddChapterAbove");
		WEB_HomePage.EnterChapterDesc("5","Testing in mtqa Above");
		WEB_HomePage.ClickOnValidateButton();
		WEB_HomePage.AddChapterBelow("5.");
		WEB_HomePage.EnterChapterTitle("6","WEBASE007__AddChapterBelow");
		WEB_HomePage.EnterChapterDesc("6","Testing in mtqa Below");
		WEB_HomePage.ClickOnValidateButton();
		WEB_HomePage.DeleteChapter("6");
		WEB_HomePage.DeleteChapter("5");	
	}
	

	@Test(enabled=false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE010__AddChapterAttachment()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.AddNewChapter();
		WEB_HomePage.EnterChapterTitle("5","WEBASE010__AddChapterAttachment");
		WEB_HomePage.ClickAddChapterAttachment("5");
		WEB_HomePage.AddNewAttachmentToChapter(GlobalVaraibles.Input.get("Parameter1"));
		WEB_HomePage.ClickOnValidateButton();
	}
	@Test()
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE010__AddChapterTextinEditor()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.AddNewChapter();
		WEB_HomePage.EnterChapterTitle("5","Siddharth");
		WEB_HomePage.OpenChapterEditor("5");
		ChapterEditorPage.AddChapterRichtext(GlobalVaraibles.Input.get("Parameter1"));	
		ChapterEditorPage.ClickBackFromEditor();
		WEB_HomePage.ClickOnValidateButton();
		WEB_HomePage.DeleteChapter("5");
	}
	@Test(enabled=false)
	@SFCReportCategory(Author = "Siddharth",Category = "WEBASE,Regression")
	public static void WEBASE010__AddChapterImageinEditor()
	{
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginbySingleSignOn();
		Specifications.LoadSpecInEditor(GlobalVaraibles.Input.get("ID"));
		WEB_HomePage.AddNewChapter();
		WEB_HomePage.EnterChapterTitle("5","Siddharth");
		WEB_HomePage.OpenChapterEditor("5");
		ChapterEditorPage.AddImageRichText(GlobalVaraibles.Input.get("Parameter1"));	
		ChapterEditorPage.ClickBackFromEditor();
		WEB_HomePage.VerifyImageInChapterDescription();
		WEB_HomePage.DeleteChapter("5");
	}
	
}
