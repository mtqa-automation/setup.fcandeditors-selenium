package setupFC;


import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Test;

import com.gf.common.CommonScript;
import com.gf.common.GlobalVaraibles;
import com.gf.common.RandomValueGenerator;
import com.gf.common.TestRailConfiguration;
import com.gf.core.SleepUtils;
import com.gf.core.SleepUtils.TimeSlab;
import com.gf.setupFC.AdminPage;
import com.gf.setupFC.LoginPage;
import com.gf.setupFC.MenuTab;
import com.gf.common.*;



public class WorkFlowConfiguration {

	public static void main(String[] args) {
		
		//CreateSpec.SFC001__CreateConfig();
	}
	
	@BeforeMethod
	public void GenerateRandomValues() {
		
		RandomValueGenerator.AssignRandomValues();
		
	}
	
	@AfterMethod
	public void ResetBrowser() {
		
		MenuTab.CloseBrowser();
		TestRailConfiguration.GetInstance().AddTestResult(GlobalVaraibles.Input.get("Manual_TC_ID1"), GlobalVaraibles.TestResult, GlobalVaraibles.Input.get("Manual_TC_Desc")+" :-Tested by automation and "+GlobalVaraibles.TestResult);
		
	}
	
	@BeforeClass
	public void Presetup() {
		
		GlobalVaraibles.SuiteName="WorkFlowConfiguration";
		
	}
	
	@AfterClass
	public void ResetSuite() {
		
		CommonScript.SuiteReportEnd();
		
	}
	
	
	
	@Test(groups="WorkFlowConfiguration,smoke,admin",enabled=true)
	@SFCReportCategory(Author="")
	public  void SFC_WC001__CreateConfig() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Admin,SmokeTest", "Rashmi");
		
		// Login to Setup.FC
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		// LoginPage.LoginbySingleSignOn();
		
		// Create new config
		MenuTab.SelectAdminTab();
		AdminPage.NewConfig_AddConfigDetails(GlobalVaraibles.Input.get("GeneratedTitle"),GlobalVaraibles.Input.get("Class"),GlobalVaraibles.Input.get("SubClass"),GlobalVaraibles.Input.get("Department"));
		AdminPage.NewConfig_AddApprovalGrpCofig(GlobalVaraibles.Input.get("GroupConfigName"));
		AdminPage.NewConfig_Save();
	}
	
	
	@Test(groups="WorkFlowConfiguration,Regression,admin", enabled=true)
	public static void SFC_WC002__SerchAndVerifyConfigDetails() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Admin,RegressionTest", "Rashmi");
		
		// Login to Setup.FC
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		// LoginPage.LoginbySingleSignOn();
		
		// Search config
		MenuTab.SelectAdminTab();
		AdminPage.SearchConfigByName(GlobalVaraibles.Input.get("Title"));
		AdminPage.SearchConfigByDepartment(GlobalVaraibles.Input.get("Department"));
		AdminPage.SearchConfigByClass(GlobalVaraibles.Input.get("Class"));
		AdminPage.SearchConfigBySubClass(GlobalVaraibles.Input.get("Class"),GlobalVaraibles.Input.get("SubClass"));
		AdminPage.SearchConfigByID(GlobalVaraibles.Input.get("ID"));
		AdminPage.SelectConfigbyTitle(GlobalVaraibles.Input.get("Title"));
		
		// Verify config details
		AdminPage.VerifyWorkflowdetails(GlobalVaraibles.Input.get("ID"), GlobalVaraibles.Input.get("Title"), GlobalVaraibles.Input.get("Class"),GlobalVaraibles.Input.get("SubClass"), GlobalVaraibles.Input.get("Department"));
		
	}
	
	
	@Test(groups="WorkFlowConfiguration,Regression,admin", enabled=true)
	public static void SFC_WC003__UpdateConfig() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Admin,SmokeTest", "Siddharth");
		
		// Login to Setup.FC
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		// LoginPage.LoginbySingleSignOn();
		
		// Search config by ID
		MenuTab.SelectAdminTab();
		AdminPage.SearchConfigByID(GlobalVaraibles.Input.get("ID"));
		AdminPage.UpdateWorkConfig(GlobalVaraibles.Input.get("GeneratedTitle"), GlobalVaraibles.Input.get("GroupConfigName"));
		AdminPage.UpdatedConfig_Save();
		
	}

	@Test(groups="WorkFlowConfiguration,Regression,admin")
	public static void SFC_WC004__DeleteConfig() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Admin,RegressionTest", "Siddharth");
		
		// Login to Setup.FC
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		// LoginPage.LoginbySingleSignOn();
		
		// Search config
		MenuTab.SelectAdminTab();
		AdminPage.SearchConfigByID(GlobalVaraibles.Input.get("ID"));
		AdminPage.SelectConfigbyID(GlobalVaraibles.Input.get("ID"));
		
		// Delete config
		AdminPage.DeleteConfig(GlobalVaraibles.Input.get("SuccessMessage"));
		
	}
	
	
}
