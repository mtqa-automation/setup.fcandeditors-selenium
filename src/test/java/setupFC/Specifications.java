package setupFC;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.gf.CommonWebEditor.WE_MainPage;
import com.gf.WEBase.WEB_HomePage;
import com.gf.common.CommonScript;
import com.gf.common.GlobalVaraibles;
import com.gf.common.RandomValueGenerator;
import com.gf.setupFC.LoginPage;
import com.gf.setupFC.MenuTab;
import com.gf.setupFC.SpecificationsPage;
import com.gf.setupFC.TaskPage;


public class Specifications {
	
	@BeforeMethod
	public static void GenerateRandomValues() {
		
		RandomValueGenerator.AssignRandomValues();
	}
	
	@AfterMethod
	public static void ResetBrowser() {
		
		CommonScript.ReportEnd();
		MenuTab.CloseBrowser();
	}
	
	@BeforeClass
	public static void Presetup() {
		
		GlobalVaraibles.SuiteName="Specifications";
	}
	
	@AfterClass
	public static void ResetSuite() {
		
		CommonScript.SuiteReportEnd();
	}
	
	
	@Test(groups="Specifications,smoke", enabled=false, priority=1)
	public static void SFC_SP001__CreateSpecification() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Specifications,SmokeTest", "Rashmi");
		
		// Login to Setup.FC application
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
	    // LoginPage.LoginbySingleSignOn();
		
		// Create a new Specification
		MenuTab.SelectSpecificationTab();
		SpecificationsPage.CreateNewSpec_SelectNewOption();
		SpecificationsPage.CreateNewSpec_AddGeneralSettings(GlobalVaraibles.Input.get("GeneratedTitle"), GlobalVaraibles.Input.get("Class"), GlobalVaraibles.Input.get("SubClass"),  GlobalVaraibles.Input.get("Department"));
		SpecificationsPage.CreateNewSpec_AddWorkFlowConfig(GlobalVaraibles.Input.get("WorkflowID"));
		SpecificationsPage.CreateNewSpec_Confirmation(GlobalVaraibles.Input.get("SuccessMessage"));
		
	}
	
	@Test(groups="Specifications,smoke", priority=2)
	public static void SFC_SP002__CreateSpec_EditType_ContentChange() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Specifications,SmokeTest", "Rashmi");
		
		// Login to Setup.FC application
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
	    // LoginPage.LoginbySingleSignOn();
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		
		// Create a new specification
		MenuTab.SelectSpecificationTab();
		SpecificationsPage.CreateNewSpec_SelectNewOption();
		SpecificationsPage.CreateNewSpec_AddGeneralSettings(GlobalVaraibles.Input.get("GeneratedTitle"), GlobalVaraibles.Input.get("Class"), GlobalVaraibles.Input.get("SubClass"),  GlobalVaraibles.Input.get("Department"));
		SpecificationsPage.CreateNewSpec_AddWorkFlowConfig(GlobalVaraibles.Input.get("WorkflowID"));
		SpecificationsPage.CreateNewSpec_Confirmation(GlobalVaraibles.Input.get("SuccessMessage"));
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		
		// Lock Spec for edit and create task
		SpecificationsPage.CreateNewSpec_EditSpec(GlobalVaraibles.Input.get("Parameter1"), GlobalVaraibles.Input.get("ID"));
		SpecificationsPage.CreateNewSpec_VerifyEditType(GlobalVaraibles.Input.get("Parameter2"));
		SpecificationsPage.CreateNewSpec_LockForEditSpec(GlobalVaraibles.Input.get("Parameter3"));
		
		// Verify created task
		MenuTab.SelectTaskTab();
		TaskPage.VerifyTask(GlobalVaraibles.Input.get("Parameter4"), GlobalVaraibles.Input.get("Parameter5"), GlobalVaraibles.Input.get("Author"), GlobalVaraibles.Input.get("ID"),  GlobalVaraibles.Input.get("Title"), GlobalVaraibles.Input.get("Parameter6"));
		
	}
	
	@Test(groups="Specifications,smoke", enabled=false, priority=3)
	public static void SFC_SP005__ContentChange_LoadSpecInEditor() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Specifications,SmokeTest", "Rashmi");
		
		// Login to Setup.FC application
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
	    // LoginPage.LoginbySingleSignOn();
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		
		MenuTab.SelectTaskTab();
		TaskPage.VerifyTask(GlobalVaraibles.Input.get("Parameter4"), GlobalVaraibles.Input.get("Parameter5"), GlobalVaraibles.Input.get("Author"), GlobalVaraibles.Input.get("ID"),  GlobalVaraibles.Input.get("Title"), GlobalVaraibles.Input.get("Parameter6"));
		TaskPage.SelectConfirmBtn_ContentEditSpec();
		WE_MainPage.SwitchToEditorWindow();
		WEB_HomePage.VerifyHomeScreen();
		
	}
	
	
	@Test(groups="Specifications,smoke", priority=4)
	public static void SFC_SP003__CreateSpec_EditType_PropertyChange() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Specifications,SmokeTest", "Rashmi");
		
		// Login to Setup.FC application
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
	    // LoginPage.LoginbySingleSignOn();
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		
		// Create new Specification
		MenuTab.SelectSpecificationTab();
		SpecificationsPage.CreateNewSpec_SelectNewOption();
		SpecificationsPage.CreateNewSpec_AddGeneralSettings(GlobalVaraibles.Input.get("GeneratedTitle"), GlobalVaraibles.Input.get("Class"), GlobalVaraibles.Input.get("SubClass"),  GlobalVaraibles.Input.get("Department"));
		SpecificationsPage.CreateNewSpec_AddWorkFlowConfig(GlobalVaraibles.Input.get("WorkflowID"));
		SpecificationsPage.CreateNewSpec_Confirmation(GlobalVaraibles.Input.get("SuccessMessage"));
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		
		// Lock Spec for edit and create new task
		SpecificationsPage.CreateNewSpec_EditSpec(GlobalVaraibles.Input.get("Parameter1"), GlobalVaraibles.Input.get("ID"));
		SpecificationsPage.CreateNewSpec_VerifyEditType(GlobalVaraibles.Input.get("Parameter2"));
		SpecificationsPage.CreateNewSpec_LockForEditSpec(GlobalVaraibles.Input.get("Parameter3"));
		
		// Verify created task
		MenuTab.SelectTaskTab();
		System.out.println("1. " + GlobalVaraibles.Input.get("Parameter4") + " 2. " + GlobalVaraibles.Input.get("Parameter5") + " 3. " + GlobalVaraibles.Input.get("Author") + " 4. " + GlobalVaraibles.Input.get("ID") + " 5. " + GlobalVaraibles.Input.get("Title") + " 6. " + GlobalVaraibles.Input.get("Parameter6"));
		TaskPage.VerifyTask(GlobalVaraibles.Input.get("Parameter4"), GlobalVaraibles.Input.get("Parameter5"), GlobalVaraibles.Input.get("Author"), GlobalVaraibles.Input.get("ID"),  GlobalVaraibles.Input.get("Title"), GlobalVaraibles.Input.get("Parameter6"));
		
		
	}
	

	@Test(groups="Specifications,smoke", enabled=false, priority=5)
	public static void SFC_SP006__PropertyChange_EditPropertyChangeAndApprove() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Specifications,SmokeTest", "Rashmi");
		
		// Login to Setup.FC application
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
	    // LoginPage.LoginbySingleSignOn();
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		
		// Open task tab
		MenuTab.SelectTaskTab();
		TaskPage.VerifyTask(GlobalVaraibles.Input.get("Parameter4"), GlobalVaraibles.Input.get("Parameter5"), GlobalVaraibles.Input.get("Author"), GlobalVaraibles.Input.get("ID"),  GlobalVaraibles.Input.get("Title"), GlobalVaraibles.Input.get("Parameter6"));
		TaskPage.GoToBottomPage();
		TaskPage.PC_UpdateSpecOwner(GlobalVaraibles.Input.get("UserName2"));
		TaskPage.PC_AddChangeDescription(GlobalVaraibles.Input.get("Change-Desc"));
		TaskPage.PC_AddChangeReason(GlobalVaraibles.Input.get("Change-Reason"));
		TaskPage.PC_Confirm();
		MenuTab.Logout();
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName2"), GlobalVaraibles.Input.get("Password2"));
		MenuTab.SelectTaskTab();
		TaskPage.TaskFilterBySpecID(GlobalVaraibles.Input.get("ID"));
		TaskPage.ApproveTask_Confirm();
		TaskPage.ApprovalPage_AddComment(GlobalVaraibles.Input.get("Parameter7"));
		TaskPage.ApprovalPage_Approve();
		TaskPage.ApprovalPage_ApprovalConfirm();
		TaskPage.TaskSearch_NotExist(GlobalVaraibles.Input.get("ID"));
		
	}

	
	@Test(groups="Specifications,smoke", priority=6)
	public static void SFC_SP004__CreateSpec_EditType_Termination() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Specifications,SmokeTest", "Rashmi");
		
		// Login to Setup.FC application
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		//LoginPage.LoginbySingleSignOn();
		
		// Create new specification
		MenuTab.SelectSpecificationTab();
		SpecificationsPage.CreateNewSpec_SelectNewOption();
		SpecificationsPage.CreateNewSpec_AddGeneralSettings(GlobalVaraibles.Input.get("GeneratedTitle"), GlobalVaraibles.Input.get("Class"), GlobalVaraibles.Input.get("SubClass"),  GlobalVaraibles.Input.get("Department"));
		SpecificationsPage.CreateNewSpec_AddWorkFlowConfig(GlobalVaraibles.Input.get("WorkflowID"));
		SpecificationsPage.CreateNewSpec_Confirmation(GlobalVaraibles.Input.get("SuccessMessage"));
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		
		// Lock spec for edit and create new task
		SpecificationsPage.CreateNewSpec_EditSpec(GlobalVaraibles.Input.get("Parameter1"), GlobalVaraibles.Input.get("ID"));
		SpecificationsPage.CreateNewSpec_VerifyEditType(GlobalVaraibles.Input.get("Parameter2"));
		SpecificationsPage.CreateNewSpec_LockForEditSpec(GlobalVaraibles.Input.get("Parameter3"));
		
		// Verify the created task
		MenuTab.SelectTaskTab();
		TaskPage.VerifyTask(GlobalVaraibles.Input.get("Parameter4"), GlobalVaraibles.Input.get("Parameter5"), GlobalVaraibles.Input.get("Author"), GlobalVaraibles.Input.get("ID"),  GlobalVaraibles.Input.get("Title"), GlobalVaraibles.Input.get("Parameter6"));
	
	}
	
	
	@Test(groups="Specifications,smoke", enabled=false, priority=7)
	public static void SFC_SP007__TerminateSpecificationAndApprove() {
		
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.Input=CommonScript.Testdata(GlobalVaraibles.TestTitle);
		CommonScript.ReportStart(GlobalVaraibles.SuiteName, GlobalVaraibles.TestTitle, "Specifications,SmokeTest", "Rashmi");
		LoginPage.LaunchApplication(GlobalVaraibles.Input.get("ApplicationURL"));
	//	LoginPage.LoginbySingleSignOn();
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName1"), GlobalVaraibles.Input.get("Password1"));
		MenuTab.SelectTaskTab();
		TaskPage.VerifyTask(GlobalVaraibles.Input.get("Parameter4"), GlobalVaraibles.Input.get("Parameter5"), GlobalVaraibles.Input.get("Author"), GlobalVaraibles.Input.get("ID"),  GlobalVaraibles.Input.get("Title"), GlobalVaraibles.Input.get("Parameter6"));
		TaskPage.GoToBottomPage();
		TaskPage.PC_UpdateSpecOwner(GlobalVaraibles.Input.get("UserName2"));
		TaskPage.PC_AddChangeDescription(GlobalVaraibles.Input.get("Change-Desc"));
		TaskPage.PC_AddChangeReason(GlobalVaraibles.Input.get("Change-Reason"));
		TaskPage.PC_Confirm();
		MenuTab.Logout();
		LoginPage.LoginByUser(GlobalVaraibles.Input.get("UserName2"), GlobalVaraibles.Input.get("Password2"));
		MenuTab.SelectTaskTab();
		TaskPage.TaskFilterBySpecID(GlobalVaraibles.Input.get("ID"));
		TaskPage.ApproveTask_Confirm();
		TaskPage.ApprovalPage_AddComment(GlobalVaraibles.Input.get("Parameter7"));
		TaskPage.ApprovalPage_Approve();
		TaskPage.ApprovalPage_ApprovalConfirm();
		TaskPage.TaskSearch_NotExist(GlobalVaraibles.Input.get("ID"));
		
	}
	
	//===============================Pre-Setup and  and Post Setup Test Case for the WebEditor======================================
	public static void LoadSpecInEditor(String SpecID) {
		
		MenuTab.SelectTaskTab();
		TaskPage.SelectTaskByID(SpecID);
		TaskPage.SelectConfirmBtn_ContentEditSpec();
		WE_MainPage.SwitchToEditorWindow();
		WEB_HomePage.VerifyHomeScreen();
		
	}

	
	public static void CreateSpec_EditType_ContentChange(String ApplicationURL, String SpecName, String ClassName,
			String Subclass, String Department, String WorkflowID, String SuccessMsg) {
		
		SpecificationsPage.CreateNewSpec_SelectNewOption();
		SpecificationsPage.CreateNewSpec_AddGeneralSettings(SpecName,ClassName, Subclass, Department);
		SpecificationsPage.CreateNewSpec_AddWorkFlowConfig(WorkflowID);
		SpecificationsPage.CreateNewSpec_Confirmation(SuccessMsg);
		SpecificationsPage.CreateNewSpec_EditSpec();
		SpecificationsPage.CreateNewSpec_EditContentChange();
		
	}
}
