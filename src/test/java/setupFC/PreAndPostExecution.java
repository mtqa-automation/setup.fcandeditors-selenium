package setupFC;

import org.testng.annotations.BeforeSuite;

import com.gf.common.CommonScript;
import com.gf.common.GlobalVaraibles;
import com.gf.common.IniFile;
import com.gf.common.TestRailConfiguration;


public class PreAndPostExecution {
	@BeforeSuite(enabled=false)
	public void PreSetup1()
	{
		GlobalVaraibles.TestTitle=new Object(){}.getClass().getEnclosingMethod().getName();
		GlobalVaraibles.TestRailEnable=CommonScript.StringToBoolean(System.getProperty("TestRailEnable"));
		if(GlobalVaraibles.TestRailEnable) 
		{
		IniFile.GetInstance().WriteIniFile("Start_TestRail", "MileStoneName",System.getProperty("TestRail_MileStone"));
		IniFile.GetInstance().WriteIniFile("Start_TestRail", "TestPlanName",System.getProperty("TestRail_TestPlan"));
		IniFile.GetInstance().WriteIniFile("Start_TestRail", "TestRunName",System.getProperty("TestRail_TestRun"));
		}
		
	}

	
	@BeforeSuite(enabled=false)
	public void PreSetup2()
	{
		TestRailConfiguration.GetInstance().CreateMileStone();
		TestRailConfiguration.GetInstance().CreateTestPlan();
		TestRailConfiguration.GetInstance().CreateTestRun("C1");
	}

}
