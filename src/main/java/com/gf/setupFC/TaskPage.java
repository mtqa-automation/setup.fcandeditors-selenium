package com.gf.setupFC;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.gf.common.GenericWebObjects;
import com.gf.core.GeneralWebObjects;
import com.gf.core.SleepUtils;
import com.gf.core.SleepUtils.TimeSlab;

public class TaskPage {
	
	public static GeneralWebObjects uiDriver = GeneralWebObjects.getFunction();

	//===============================Task Page Locators details:Start====================================================================

	//Task filters
	
	private static By TaskFilter_SpecTitle=By.xpath("//input[@id='taskTable:specTitle_filter']");
	private static By TaskFilter_SpecID=By.xpath("//input[@id='taskTable:specId_filter']");
	
	
	// Tasks grid details
	private static By Task_Tasktype1=By.xpath("//td[@id='taskTable:0:j_id215']");
	private static By Task_TaskWK1=By.xpath("//td[@id='taskTable:0:j_id240']");
	private static By Task_Author1=By.xpath("//td[@id='taskTable:0:j_id243']");
	private static By Task_SpecID1=By.xpath("//td[@id='taskTable:0:j_id273']");
	private static By Task_SpecTitle=By.xpath("//td[@id='taskTable:0:j_id277']");
	private static By Task_SpecVersion=By.xpath("//td[@id='taskTable:0:j_id281']");
	
	//Refresh button
	private static By Task_Refresh_Btn=By.xpath("//input[@name='taskTable:j_id293']");
	
	
	//Task Details 
	private static By TaskDetails_SpecificationID=By.xpath("//span[@id='taskSpecId']");
	private static By TaskDetails_SpecificationName=By.xpath("//span[@class='specTitle']");
	private static By TaskDetails_SpecificationOwner=By.xpath("//span[@class='specOwner']");
	private static By TaskDetails_SpecificationVersion=By.xpath("//span[@id='taskSpecVersion']");
	private static By TaskDetails_SpecificationTaskType=By.xpath("//span[@id='taskWorkflowType']");
	
	private static By TaskDetails_ChangeReason=By.xpath("//table[@id='taskDetailsWrapper']/tbody/tr[5]/td[1]/pre");
	private static By TaskDetails_ChangeDescription=By.xpath("//table[@id='taskDetailsWrapper']/tbody/tr[5]/td[2]/pre");
	private static By TaskDetails_PCRB=By.xpath("//table[@id='taskDetailsWrapper']/tbody/tr[5]/td[3]");
	
	//Task Actions
//	private static By TaskDetails_TaskName=By.xpath("//span[@id='taskTitle']");
	private static By TaskDetails_ActionsSelectionTypes=By.xpath("//label[contains(@for,'actionSelector')]");
	private static By TaskDetails_EditSpec_ConfrmBtn=By.xpath("//a[contains(@id,'actionRepeat:0:editSpecContent')]");
	private static By TaskDetails_Apprve_TakeActn_ConfrmBtn=By.xpath("//a[@class='fakeButton']");
	private static By TaskDetails_common_ConfrmBtn=By.xpath("//*[contains(@id,'submit')]");
	private static By TaskDetails_Update_ChangeReason_Txtfld=By.xpath("//textarea[contains(@id,'changeReason')]");
	private static By TaskDetails_Update_ChangeDescription_Txtfld=By.xpath("//textarea[contains(@id,'changeDescription')]");
	
	//Property change task details
	
	private static By PC_Update_SpecTitle_Txtfld=By.xpath("//label[contains(text(),'Title')]/ancestor::td/following-sibling::td/input");
	private static By PC_Update_Subclass_Txtfld=By.xpath("//label[contains(text(),'Subclass *')]/ancestor::td/following-sibling::td//input[contains(@id,'subclassCombocomboboxField')]");
	private static By PC_Update_Department_Txtfld=By.xpath("//label[contains(text(),'Department')]/ancestor::td/following-sibling::td//input[contains(@id,'departmentCombocomboboxField')]");
	private static By PC_Update_Workflow_Txtfld=By.xpath("//a[contains(text(),'Workflow Config *')]/ancestor::td/following-sibling::td//input[contains(@id,'workflowConfigCombocomboboxField')]");
	private static By PC_Update_SpecOwner_Txtfld=By.xpath("//input[contains(@id,'userNameInputField')]");

	
	
	//Approval Task details
	private static By ApprovalPage_Comment_Txtfld=By.id("taskActions:submitComment");
	private static By ApprovalPage_Approve_Btn=By.id("taskActions:approveTask");
	private static By ApprovalPage_Reject_Btn=By.id("taskActions:rejectTask");
	
	//Approval Confirmation details
	private static By ApprovalPage_Confirm_OkBtn=By.xpath("//input[@id='_approveTaskForm:OkButton']");
	private static By ApprovalPage_Confirm_Hdr=By.xpath("//div[@id='confirmTaskApprovalPanelHeader']");
	
	
	//Terminations Task details
	
	
//===============================Task Page Locators details:Ends====================================================================	


//===============================Task Page Keywords/functions details:Start====================================================================	

	public static void GoToBottomPage()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ScrollToBottom("Scroll down to bottom of the page"));	
	}
	
	
	//-----------------------***************************Task Grid***************************--------------------------------------
	
	
	/**
	 * Select task by spec ID
	 **/
	public static void SelectTaskByID(String SpecID) {
		
		TaskFilterBySpecID(SpecID);
		Assert.assertTrue(uiDriver.click(Task_Tasktype1, "Click on First Row task"));
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
	
	/**
	 * Filter task by Spec ID
	 **/
	public static void TaskFilterBySpecID(String SpecID) {
		
		Assert.assertTrue(uiDriver.setValue(TaskFilter_SpecID, SpecID+Keys.ENTER, "Enter the Spec ID in Spec ID filter"));
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
	
	/**
	 * Verify the created task
	 **/
	public static void VerifyTask(String TaskType, String Workflow,String author,String SpecID,String SpecTitle,String SpecVersion) {
		
		SelectTaskByID(SpecID);
		Assert.assertTrue(uiDriver.validateText(Task_Tasktype1,TaskType, "Verified Task Type"),"Verified Task Type");
		Assert.assertTrue(uiDriver.validateText(Task_TaskWK1,Workflow, "Verified Task Type"),"Verified Task Type");
		Assert.assertTrue(uiDriver.validateText(Task_Author1,author, "Verified Spec author"),"Verified Spec author");
		Assert.assertTrue(uiDriver.validateText(Task_SpecID1,SpecID, "Verified Spec ID"),"Verified Spec ID");
		Assert.assertTrue(uiDriver.validateText(Task_SpecTitle,SpecTitle, "Verified Spec title"),"Verified Spec title");
		Assert.assertTrue(uiDriver.validateText(Task_SpecVersion,SpecVersion, "Verified Spec version"),"Verified Spec version");
	}
	
	public static void VerifyTaskDetails(String SpecID, String SpecName,String SpecOwner,String SpecVersion,String SpecTaskType,String ChangeReason
			,String ChangeDesc,String PCRB)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(TaskDetails_SpecificationID,SpecID, "Verified Specification ID"),"Verified Specification ID");
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(TaskDetails_SpecificationName,SpecName, "Verified Specification Name"),"Verified Specification Name");
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(TaskDetails_SpecificationOwner,SpecOwner, "Verified Specification Owner"),"Verified Specification Owner");
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(TaskDetails_SpecificationVersion,SpecVersion, "Verified Specification Version"),"Verified Specification Version");
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(TaskDetails_SpecificationTaskType,SpecTaskType, "Verified Specification Task type"),"Verified Specification Task type");
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(TaskDetails_ChangeReason,ChangeReason, "Verified Specification Change reason"),"Verified Specification Change reason");
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(TaskDetails_ChangeDescription,ChangeDesc, "Verified Specification change Description"),"Verified Specification change Description");
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(TaskDetails_PCRB,PCRB, "Verified Specification PCRB"),"Verified Specification PCRB");
	
	}
	
	public static void TaskSearch_NotExist(String SpecID)
	{
		
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Task_Refresh_Btn, "Click on Refresh button"),"Click on Refresh button");
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(TaskFilter_SpecID, SpecID, "Enter the Spec ID in Spec ID filter"),"Enter the Spec ID in Spec ID filter");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Task_Refresh_Btn, "Click on Refresh button"),"Click on Refresh button");
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(Task_Tasktype1, "Verified Task is not exist"),"Verified Task is not exist");
	}
	
	public static void TaskFilterBySpecTitle(String SpecTitle)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(TaskFilter_SpecTitle, SpecTitle+Keys.ENTER, "Enter the Spec ID in Spec ID filter"),"Enter the Spec ID in Spec ID filter");
		Assert.assertTrue(GenericWebObjects.GetInstance().MediumWait());
	}
	
	
	//----------------------**********Task Action-Content Change ***************************----------------------
	
	public static void SelectConfirmBtn_ContentEditSpec(String SelectAction)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(TaskDetails_ActionsSelectionTypes,SelectAction, "Select the Action"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(TaskDetails_EditSpec_ConfrmBtn, "Click on Confirm button from Edit specification section"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	public static void SelectConfirmBtn_ContentEditSpec()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(TaskDetails_EditSpec_ConfrmBtn, "Click on Confirm button from Edit specification section"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	
	//------------------**************Task Action--Property Change*********************---------------------
	
	public static void PC_UpdateSpecTitle(String SpecTitle)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(PC_Update_SpecTitle_Txtfld,SpecTitle, "Enter the Spec Title Name for property change"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		
	}
	
	public static void PC_UpdateSpecSubclass(String SubclassName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(PC_Update_Subclass_Txtfld,SubclassName, "Enter/Select the Subclass"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	
	public static void PC_UpdateSpecDepartment(String DepartName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(PC_Update_Department_Txtfld,DepartName, "Enter/Select the Department"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	public static void PC_UpdateWorkFlowConfig(String WorkflowConfigID)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(PC_Update_Workflow_Txtfld,WorkflowConfigID, "Enter/Select the Workflow ID"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	public static void PC_UpdateSpecOwner(String Owner)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(PC_Update_SpecOwner_Txtfld,Owner, "Enter/Select spec Owner"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MediumWait());
	}
	public static void PC_AddChangeReason(String Reason)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(TaskDetails_Update_ChangeReason_Txtfld,Reason, "Enter Change reason "));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	public static void PC_AddChangeDescription(String Description)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(TaskDetails_Update_ChangeDescription_Txtfld,Description, "Enter Change description "));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	public static void PC_Confirm()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(TaskDetails_common_ConfrmBtn, "Click on property change confirm button"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	//**********************Task Action --Termination details *********************************
	
	
	public static void Termination_AddChangeReason(String Reason)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(TaskDetails_Update_ChangeReason_Txtfld,Reason, "Enter Change reason "));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	public static void Termination_AddChangeDescription(String Description)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(TaskDetails_Update_ChangeDescription_Txtfld,Description, "Enter Change description "));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	public static void Termination_Confirm()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ScrollToBottom("Scroll down to bottom of the page"));
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(TaskDetails_common_ConfrmBtn, "Click on property change confirm button"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	
	//**********************Task Action --Approval details *********************************
	public static void ApproveTask_Confirm()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ScrollToBottom("Scroll down to bottom of the page"));
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(TaskDetails_Apprve_TakeActn_ConfrmBtn, "Click on confirm button for approval task"));
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
	}
	
	public static void ApprovalPage_AddComment(String ApproverComment)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(ApprovalPage_Comment_Txtfld,ApproverComment,"Enter the approval comment"));
		
	}
	
	public static void ApprovalPage_Approve()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(ApprovalPage_Approve_Btn, "Click on Approve button for approving spec"));
	}
	
	public static void ApprovalPage_Reject()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(ApprovalPage_Reject_Btn, "Click on Reject button for Rejecting spec"));
	}
	public static void ApprovalPage_ApprovalConfirm()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(ApprovalPage_Confirm_Hdr, "Approval Confirmation Header"));
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(ApprovalPage_Confirm_OkBtn, "Click on OK button from Approval confirmation window"));
	}
	
//===============================Task Page Keywords/functions details:End====================================================================	

}
