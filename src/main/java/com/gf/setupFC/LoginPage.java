package com.gf.setupFC;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.gf.common.GenericWebObjects;
import com.gf.core.GeneralWebObjects;

/**
 * 
 * */
public class LoginPage {
	
	public static GeneralWebObjects uiDriver = GeneralWebObjects.getFunction();

	
	// ******WebElements for LoginPage***************	
	
	private	static By SingleSignOn= By.xpath("//a[contains(text(),'Follow this link to automatically re-login (single')]");
	private static By UserName_Txt=By.id("loginForm:username");
	private static By Password_Txt=By.id("loginForm:password");
	private static By Login_Btn=By.id("loginForm:submit");
	
	// ****WebElements End for LoginPage*************
	
	public static void main(String[] args) {
		
		LoginPage.LaunchApplication("http://fc8stfcm01:8080/setupfc/pages/login.seam");
	}
	
	/**
	 * @param URL
	 */
	public static void LaunchApplication(String URL) {
			
		//Assert.assertTrue(uiDriver.launchApplication(URL, "Launch Application URL"));
		uiDriver.launchApplication(URL, "Launch Application URL");
		
	}
	
	public static void LoginbySingleSignOn() {
		
		Assert.assertTrue(uiDriver.click(SingleSignOn, "Click on Single sign in button"));
		HomePage.VerifyHomeScreen();
		
	}
	
	/**
	 * Login to the application
	 * 
	 * @param UserName
	 * @param Password
	 * 
	 **/
	public static void LoginByUser(String UserName,String Password) {
		
		uiDriver.setValue(UserName_Txt, UserName,"Enter Login UserName");
		uiDriver.setValue(Password_Txt, Password, "Enter Loging Password");
		uiDriver.highlightElement(Login_Btn);
		uiDriver.AddScreenshortInReport("Credentials are set and Login button is present");
		uiDriver.click(Login_Btn, "Click on Login button");
		HomePage.VerifyHomeScreen();
	}
	
	public static void closeBrowser() {
		
		GenericWebObjects.GetInstance().CloseTab();
	}
}
