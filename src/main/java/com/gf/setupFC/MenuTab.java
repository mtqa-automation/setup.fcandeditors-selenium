package com.gf.setupFC;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.gf.common.GenericWebObjects;
import com.gf.common.IniFile;
import com.gf.core.GeneralWebObjects;
import com.gf.core.SleepUtils;
import com.gf.core.SleepUtils.TimeSlab;

/**
 * @author smallika
 *
 *
 */
public class MenuTab {
	
	public static GeneralWebObjects uiDriver = GeneralWebObjects.getFunction();	
	
	//===============================Menu Tab Locators details:Start=====================================================================	
	private static By HomeTab= By.xpath("//div[@id='menu']//tr//*[contains(text(),'Home')]");
	private static By TasksTab=By.xpath("//div[@id='menu']//tr//*[contains(text(),'Tasks')]");
	private static By SpecificationsTab=By.xpath("//div[@id='menu']//tr//*[contains(text(),'Specifications')]");
	private static By ReportsTab=By.xpath("//div[@id='menu']//tr//*[contains(text(),'Reports')]");
	private static By AdminTab=By.xpath("//div[@id='menu']//tr//*[contains(text(),'Admin')]");


	private static By Logout_Btn=By.id("j_id22:menuLogoutId");
	private static By UserID=By.xpath("//span[@id='j_id22:menuWelcomeId']");

	//===============================Menu Tab Locators details:End=====================================================================


	
	//===============================Menu Tab Keywords/Functions details:Start=========================================================
	
	/**
	 * Select task tab
	 **/
	public static void SelectTaskTab() {
		
		Assert.assertTrue(uiDriver.click(TasksTab, "Select the Task tab"));
		SleepUtils.sleep(TimeSlab.LOW);
	}

	
	public static void SelectHomeTab() {
		
		Assert.assertTrue(uiDriver.click(HomeTab, "Select the Home tab"));
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
	
	/**
	 * Select specification tab
	 **/
	public static void SelectSpecificationTab() {
		
		Assert.assertTrue(uiDriver.click(SpecificationsTab, "Select the Specifications tab"));
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
	
	/**
	 * Select Admin tab
	 **/
	public static void SelectAdminTab() {
		
		Assert.assertTrue(uiDriver.click(AdminTab, "Select the Admin tab"));
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
	
	public static void SelectReportsTab() {
		
		Assert.assertTrue(uiDriver.click(ReportsTab, "Select Reports tab"));
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
	
	public static void Logout() {
		
		Assert.assertTrue(uiDriver.click(Logout_Btn, "click on Logout button"));
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
	
	public static void GetUserID() {
		
		String UserName = uiDriver.getValue(UserID,"Signed in User Name");
		UserName=UserName.split(":")[1].trim();
		
		IniFile.GetInstance().WriteIniFile(
				IniFile.GetInstance().ReadIniFile("START_GLOBAL", "ExecutionSection"), "EnvironmentLoggedUserID",UserName);	
	}
	
	
	public static void CloseBrowser() {
		
		Assert.assertTrue(uiDriver.CloseBrowser());
	}
	
	
//===============================Menu Tab Keywords/Functions details:End=========================================================
}
