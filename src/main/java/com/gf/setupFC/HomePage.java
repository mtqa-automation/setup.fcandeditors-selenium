package com.gf.setupFC;


import org.openqa.selenium.By;
import org.testng.Assert;

import com.gf.common.GenericWebObjects;
import com.gf.core.GeneralWebObjects;

/**
 * 
 * */
public class HomePage {
	
	public static GeneralWebObjects uiDriver = GeneralWebObjects.getFunction();
	
	static By HomeTitle=By.xpath("//span[contains(@title,'Detail: Welcome to setup.FC')]");
	
	/**
	 * @param by
	 * @param Text
	 */

	public static void VerifyHomeScreen() {
		
		if(uiDriver.IsElementPresentAndVisible(HomeTitle)) {
			
			uiDriver.highlightElement(HomeTitle);
			MenuTab.GetUserID();
		
		} else {
			uiDriver.AddScreenshortInReport("Home Title is not present");
		}
		
	}

	
}
