package com.gf.setupFC;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.gf.common.GenericWebObjects;
import com.gf.core.GeneralWebObjects;
import com.gf.core.SleepUtils;
import com.gf.core.SleepUtils.TimeSlab;



/**
 * 
 * @author smallika
 * 
 * @ModifiedBy rhalli
 *
 */
public class AdminPage {
	
	public static GeneralWebObjects uiDriver = GeneralWebObjects.getFunction();
	
//===============================Admin Page Locators details:Start==========================================//

	// Left Panel header locator
	private static By General=By.id("j_id78:j_id86");
	private static By Workflow=By.id("j_id78:j_id103");
	private static By System=By.id("j_id78:j_id114");
	
	//Workflow section locators
	private static By NewConfig_Optn=By.id("j_id78:j_id106");
	private static By SrchConfig_Optn=By.id("j_id78:j_id108");
	private static By UsrTskMaint_Optn=By.id("j_id78:j_id110");
	private static By Maintance_Optn=By.id("j_id78:j_id113");
	
	
	//WorkFlow New Config locators
	private static By NewConfig_Title_txtfld=By.xpath("//input[@name='newWorkflowsConfigForm:j_id332']");
	private static By NewConfig_Class_txtfld=By.xpath("//input[@id='newWorkflowsConfigForm:classCombocomboboxField']");
	private static By NewConfig_SubClass_txtfld=By.xpath("//input[@id='newWorkflowsConfigForm:subclassCombocomboboxField']");
	private static By NewConfig_Deprtmnt_txtfld=By.xpath("//input[@id='newWorkflowsConfigForm:j_id341comboboxField']");
	//private static By NewConfig_Subjct_txtfld=By.xpath("//input[@id='newWorkflowsConfigForm:j_id345comboboxField']");

	// WorkFlow Search Config locators
	private static By SrchConfig_ID_txtfld=By.id("searchIdInput");
	private static By SrchConfig_Title_txtfld=By.id("searchTitleInput");
	private static By SrchConfig_Class_txtfld=By.id("classCombocomboboxField");
	private static By SrchConfig_SubClass_txtfld=By.id("subclassCombocomboboxField");
	private static By SrchConfig_Deprtmnt_txtfld=By.id("departmentFilterCombo1comboboxField");
	
	private static By SearchConfig_Btn=By.id("searchButton");
	private static By ClearConfig_Btn=By.id("clearButton");

	// Workflow Searched details
	private static By SrchConfig1_ID=By.xpath("//td[@id='configTable:0:j_id161']");
	private static By SrchConfig1_Title=By.xpath("//td[@id='configTable:0:j_id164']");
	private static By SrchConfig1_Dept=By.xpath("//td[@id='configTable:0:j_id167']");
	private static By SrchConfig1_Class=By.xpath("//td[@id='configTable:0:j_id173']");
	private static By SrchConfig1_Subclass=By.xpath("//td[@id='configTable:0:j_id176']");
	private static By  SrchConfigLst_ID=By.xpath("//td[contains(@id,'j_id161')]");
	private static By  SrchConfigLst_Title=By.xpath("//td[contains(@id,'j_id164')]");
	private static By  SrchConfigLst_class=By.xpath("//td[contains(@id,'j_id173')]");
	
	//New Config Approval section locators
	private static By NewConfig_Nrmal_ApprvalTier1_Btn=By.id("newWorkflowsConfigForm:approvalTable:0:j_id380");
	private static By NewConfig_Nrmal_GrpConfig_txtfld= By.id("newWorkflowsConfigForm:approvalTable:0:j_id387:0:j_id390comboboxField");
	private static By NewConfig_SucsMsg=By.xpath("//div[@id='workflowConfigs:userActionMessagePanel']");
	private static By NewWrkConfigID=By.id("workflowConfigs:workflowConfigId");
	private static By NewWrkConfigTitle=By.id("workflowConfigs:workflowConfigTitle");
	
	
	//New Config Decision locators
	private static By SaveBtn=By.id("newWorkflowsConfigForm:j_id463");
	private static By ClearBtn=By.id("newWorkflowsConfigForm:j_id464");
	private static By CancelBtn=By.id("newWorkflowsConfigForm:j_id465");

	// WorkFlow Config Details
	private static By WrkConfigID=By.id("workflowConfigId");
	private static By WrkConfigTitle=By.id("workflowConfigTitle");
	private static By WrkConfigClsSubCls=By.id("workflowConfigClassSubClass");
	private static By WrkConfigDepart=By.id("workflowConfigDepartment");

	// Edit Workflow Config
	private static By EditConfig_Btn=By.id("j_id282");
	private static By EditConfig_Title_txtfld=By.xpath("//input[@name='j_id121:j_id130']");
	private static  By Nrml_Apprval1_AddBtn=By.id("j_id121:approvalTable:0:j_id184");
	private static  By Nrml_Apprval1_DltBtn=By.id("j_id121:approvalTable:0:j_id186");
	private static  By Nrml_GrpCnfg1_txtfld=By.id("j_id121:approvalTable:0:j_id191:0:j_id194comboboxField");
	private static  By Nrml_GrpCnfg1_DltBtn=By.id("j_id121:approvalTable:0:j_id191:0:j_id200");
	private static  By Nrml_GrpCnfg1_AddBtn=By.id("j_id121:approvalTable:0:j_id203");
	
	//Edit Config Decision locators
	private static By Edit_SaveBtn=By.id("j_id121:j_id267");
	private static By Edit_CancelBtn=By.id("j_id121:j_id268");
	
	//Delete Config locators
	private static By Config_Dlt_Optn=By.id("deleteConfig");
	private static By Config_Dlt_Hdr= By.id("deleteConfigPanelHeader");
	private static By Config_Dlt_CnfrmBtn= By.id("_deleteConfigForm:OkButton");
	private static By Config_Dlt_Cancl= By.id("_deleteConfigForm:CancelButton");
	private static By Config_Dltd_SucsMsg= By.xpath("//span[contains(@title,'Detail:')]");
	
	
	private static By progress_loader=By.xpath("//div[@id='statusContainer']//td[text()='Ready']");
	
	
	
//===============================Admin Page Locators details:End====================================================================



	/**
	 * Create new workflow config
	 **/
	public static void NewConfig_AddConfigDetails(String Title, String ClassID, String SubClassName, String Department) {
		
		Assert.assertTrue(uiDriver.click(Workflow, "Click on Workflow Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(NewConfig_Optn, "Click on New Config Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.setValue(NewConfig_Title_txtfld, Title, "Enter New Config Title Name"));
		Assert.assertTrue(uiDriver.setValue(NewConfig_Class_txtfld, ClassID + Keys.ENTER, "Enter the Class ID"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.setValue(NewConfig_SubClass_txtfld, SubClassName + Keys.ENTER, "Enter the SubClass Name"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.setValue(NewConfig_Deprtmnt_txtfld, Department + Keys.ENTER, "Enter the Department Name"));
		SleepUtils.sleep(TimeSlab.LOW);

	}

	public static void NewConfig_AddApprovalGrpCofig(String GroupConfigName) {
		
		Assert.assertTrue(uiDriver.click(NewConfig_Nrmal_ApprvalTier1_Btn, "Click on Add approval tier 1 button"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.setValue(NewConfig_Nrmal_GrpConfig_txtfld, GroupConfigName + Keys.ENTER, "Enter the Group Config Name"));

	}

	
	/**
	 * Save the new config
	 **/
	public static void NewConfig_Save() {

		Assert.assertTrue(uiDriver.clickUsingJS(SaveBtn, "Click on Save button for New Config"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertNotNull(uiDriver.getValue(NewConfig_SucsMsg, "New workflow Created success message"));
		Assert.assertNotNull(uiDriver.getValue(NewWrkConfigID, "EnvironmentConfigID", "Newly Created Workflow ID "));
		Assert.assertNotNull(uiDriver.getValue(NewWrkConfigTitle, "EnvironmentConfigName", "Newly Created Workflow Title "));
	}

	
	/**
	 * Search config by ID
	 **/
	public static void SearchConfigByID(String ConfigId) {
		
		Assert.assertTrue(uiDriver.click(Workflow, "Click on Workflow Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(SrchConfig_Optn, "Click on Search Config Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(ClearConfig_Btn, "Click on Clear button from search config"));
		SleepUtils.sleep(TimeSlab.LOW);
		uiDriver.highlightElement(SrchConfig_ID_txtfld);
		Assert.assertTrue(uiDriver.setValue(SrchConfig_ID_txtfld, ConfigId, "Enter Workflow Config ID"));
		Assert.assertTrue(uiDriver.click(SearchConfig_Btn, "Click on Search button"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertNotNull(uiDriver.validateText(SrchConfig1_ID, ConfigId, "First Row Workflow ID "));
	
	}

	
	/**
	 * Search config by Name
	 **/
	public static void SearchConfigByName(String ConfigName) {
		
		uiDriver.IsElementPresentAndVisible(progress_loader);
		Assert.assertTrue(uiDriver.click(Workflow, "Click on Workflow Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(SrchConfig_Optn, "Click on Search Config Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(ClearConfig_Btn, "Click on Clear button from search config"));
		SleepUtils.sleep(TimeSlab.LOW);
		uiDriver.highlightElement(SrchConfig_Title_txtfld);
		Assert.assertTrue(uiDriver.setValue(SrchConfig_Title_txtfld, ConfigName, "Enter Workflow Config Title Name"));
		Assert.assertTrue(uiDriver.click(SearchConfig_Btn, "Click on Search button"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertNotNull(uiDriver.validateText(SrchConfig1_Title, ConfigName, "Compare First Row Workflow Title "));
	}

	
	/**
	 * Search config by Class
	 **/
	public static void SearchConfigByClass(String ConfigClass) {
		
		Assert.assertTrue(uiDriver.click(Workflow, "Click on Workflow Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(SrchConfig_Optn, "Click on Search Config Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(ClearConfig_Btn, "Click on Clear button from search config"));
		SleepUtils.sleep(TimeSlab.LOW);
		uiDriver.highlightElement(SrchConfig_Class_txtfld);
		Assert.assertTrue(uiDriver.setValue(SrchConfig_Class_txtfld, ConfigClass+ Keys.ENTER, "Enter Workflow Config Class"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(SearchConfig_Btn, "Click on Search button"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertNotNull(uiDriver.validateText(SrchConfig1_Class, ConfigClass, "First Row Workflow Class ID "));
	
	}


	/**
	 * Search config by Sub-Class
	 **/
	public static void SearchConfigBySubClass(String ConfigClass,String ConfigSubClass) {
		
		Assert.assertTrue(uiDriver.click(Workflow, "Click on Workflow Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(SrchConfig_Optn, "Click on Search Config Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(ClearConfig_Btn, "Click on Clear button from search config"));
		SleepUtils.sleep(TimeSlab.LOW);
		uiDriver.highlightElement(SrchConfig_Class_txtfld);
		Assert.assertTrue(uiDriver.setValue(SrchConfig_Class_txtfld, ConfigClass+ Keys.ENTER, "Enter Workflow Config Class"));
		SleepUtils.sleep(TimeSlab.LOW);
		uiDriver.highlightElement(SrchConfig_SubClass_txtfld);
		Assert.assertTrue(uiDriver.setValue(SrchConfig_SubClass_txtfld, ConfigSubClass+ Keys.ENTER, "Enter Workflow Config SubClass"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(SearchConfig_Btn, "Click on Search button"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertNotNull(uiDriver.validateText(SrchConfig1_Subclass, ConfigSubClass, "First Row Workflow SubClass "));
	
	}

	
	/**
	 * Search config by department
	 **/
	public static void SearchConfigByDepartment(String Department) {
		
		Assert.assertTrue(uiDriver.click(Workflow, "Click on Workflow Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(SrchConfig_Optn, "Click on Search Config Option"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(ClearConfig_Btn, "Click on Clear button from search config"));
		SleepUtils.sleep(TimeSlab.LOW);
		uiDriver.highlightElement(SrchConfig_Deprtmnt_txtfld);
		Assert.assertTrue(uiDriver.setValue(SrchConfig_Deprtmnt_txtfld, Department+ Keys.ENTER, "Enter Workflow Config Department"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(SearchConfig_Btn, "Click on Search button"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertNotNull(uiDriver.validateText(SrchConfig1_Dept, Department, "First Row Workflow Department "));
	
	}

	
	/**
	 * Verify the Workflow details
	 **/
	public static void VerifyWorkflowdetails(String ConfigID,String ConfigTitle,String Class,String Subclass,String Department) {
	
		Assert.assertNotNull(uiDriver.validateText(WrkConfigID, ConfigID, "Verify Config Id"));
		Assert.assertNotNull(uiDriver.validateText(WrkConfigTitle, ConfigTitle, "Verify Config Title"));
		Assert.assertNotNull(uiDriver.validateText(WrkConfigClsSubCls, Class+" / "+Subclass, "Verify Classs and Subclasscd v Name"));
		Assert.assertNotNull(uiDriver.validateText(WrkConfigDepart, Department, "Verify Department Name"));
	}

	
	/**
	 * Update the workflow Config
	 **/
	public static void UpdateWorkConfig(String TitleName,String GroupConfigName) {

		Assert.assertTrue(uiDriver.click(SrchConfig1_ID, "Click on the Workflow config "));
		SleepUtils.sleep(TimeSlab.LOW);
		uiDriver.scrollintoView(EditConfig_Btn);
		Assert.assertTrue(uiDriver.clickUsingJS(EditConfig_Btn, "Click on the Workflow config edit button"));
		Assert.assertTrue(uiDriver.setValue(EditConfig_Title_txtfld, TitleName, "Edit the Workflow Title"));
		Assert.assertTrue(uiDriver.click(Nrml_GrpCnfg1_DltBtn, "Remove the Normal approval Group Config 1"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.elementDoesNotExist(Nrml_GrpCnfg1_txtfld, "Validate Group config drop down is removed"));
		Assert.assertTrue(uiDriver.click(Nrml_Apprval1_DltBtn, "Remove the Normal approval1"));
		Assert.assertTrue(uiDriver.elementDoesNotExist(Nrml_GrpCnfg1_AddBtn, "Validate Group config add option is removed"));
		Assert.assertTrue(uiDriver.click(Nrml_Apprval1_AddBtn, "Add the Normal approval1"));
		Assert.assertTrue(uiDriver.setValue(Nrml_GrpCnfg1_txtfld, TitleName+Keys.ENTER, "Select/Add Group config1"));
	
	}

	/**
	 * Save the updated Config
	 **/
	public static void UpdatedConfig_Save() {
		
		uiDriver.IsElementPresentAndVisible(progress_loader);
		uiDriver.scrollintoView(Edit_SaveBtn);
		Assert.assertTrue(uiDriver.clickUsingJS(Edit_SaveBtn, "Click on Save button for Updated Config"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertNotNull(uiDriver.getValue(NewConfig_SucsMsg, "Updated workflow success message"));
		Assert.assertNotNull(uiDriver.getValue(NewWrkConfigID, "EnvironmentConfigID","Updated Workflow ID "));
		Assert.assertNotNull(uiDriver.getValue(NewWrkConfigTitle, "EnvironmentConfigName","updated Workflow Title "));
	}

	public static void DeleteConfig(String SuccessMsg) {

		uiDriver.scrollintoView(Config_Dlt_Optn);
		Assert.assertTrue(uiDriver.clickUsingJS(Config_Dlt_Optn, "Click on Delete button for workflow Config"));
		SleepUtils.sleep(TimeSlab.YIELD);
		Assert.assertNotNull(uiDriver.getValue(Config_Dlt_Hdr,"Delete Header Title:"));
		Assert.assertTrue(uiDriver.click(Config_Dlt_Cancl, "Click on Cancel button from Delete Config window"));
		SleepUtils.sleep(TimeSlab.YIELD);
		Assert.assertNotNull(uiDriver.getValue(WrkConfigID, "Validate Config is not deleted"));
		Assert.assertTrue(uiDriver.clickUsingJS(Config_Dlt_Optn, "Click on Delete button for workflow Config"));
		SleepUtils.sleep(TimeSlab.YIELD);
		Assert.assertTrue(uiDriver.click(Config_Dlt_CnfrmBtn, "Click on Ok button from Delete Config window"));
		SleepUtils.sleep(TimeSlab.LOW);		
		Assert.assertNotNull(uiDriver.ValidatePartialText(Config_Dltd_SucsMsg, SuccessMsg,"Validate Deleted Config success Message"));
	
	}

	/**
	 * Select the Config by ID 
	 **/
	public static void SelectConfigbyID(String WorkflowID) {
		
		Assert.assertTrue(uiDriver.validateText(SrchConfigLst_ID, WorkflowID, "Validate the Config ID"));
		uiDriver.click(SrchConfigLst_ID, "Click on the Config ID");
		SleepUtils.sleep(TimeSlab.LOW);
	
	}

	/**
	 * Select Config by Title 
	 **/
	public static void SelectConfigbyTitle(String WorkflowTitle) {
	
		Assert.assertTrue(uiDriver.validateText(SrchConfigLst_Title, WorkflowTitle,"Validate the Config Title"));
		uiDriver.click(SrchConfigLst_Title, "Click on the Config Title");
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
public static void SelectConfigbyClass(String WorkflowClass)
{
	Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(SrchConfigLst_class, WorkflowClass,"Click on first Workflow Config from class "));
	
}


}
