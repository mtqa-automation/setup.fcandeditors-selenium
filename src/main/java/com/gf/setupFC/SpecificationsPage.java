package com.gf.setupFC;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.gf.common.GenericWebObjects;
import com.gf.common.IniFile;
import com.gf.core.GeneralWebObjects;
import com.gf.core.SleepUtils;
import com.gf.core.SleepUtils.TimeSlab;

public class SpecificationsPage {
	
	public static GeneralWebObjects uiDriver = GeneralWebObjects.getFunction();

//	static GenericWebObjects GenericWebObjects.GetInstance()=new GenericWebObjects();

//===========================Locator Details Start=====================================================================

	// Specification left panel locator
	private static By Spec_newOptn = By.id("j_id79:j_id86");
	private static By Spec_SearchOptn = By.id("j_id79:j_id88");
	private static By Spec_MyOwnOptn = By.id("j_id79:j_id92");

// Header label locator
	private static By NewSpecHeader_Msg = By.xpath("//div[@id='newSpecForm:specificationNewPanel_header']/label");
	private static By SpecSearchHeader_Msg = By.xpath("//div[@id='specContent:criteriaPanel_header']");

// New Specification locator
	private static By NewSpecTitle = By.id("newSpecForm:title");
	private static By Class_Txtfld = By.id("newSpecForm:classCombocomboboxField");
	private static By SubClass_Txtfld = By.id("newSpecForm:subclassCombocomboboxField");
	private static By Department_Txtfld = By.id("newSpecForm:departmentCombocomboboxField");
	private static By OwnerName_Txtfld = By.xpath("//input[@id='newSpecForm:metadataSpecOwner:userNameInputField']");

	private static By NextBtn = By.xpath("//input[contains(@id,'newSpecForm') and @value='Next >']");
	private static By CreateBtn = By.xpath("//input[contains(@id,'newSpecForm') and @value='Create']");
	private static By WrkFlowRadioBtn = By.xpath("//input[contains(@id,'newSpecForm:workflowConfigTable:workflowConfigRadios')]");
	private static By WrokflowID_siblingElement = By.xpath("//tr//td//*[contains(text(),'Id:')]//parent::td//following-sibling::td");
	private static By NewCreatdSpcID_Msg = By.xpath("//div[@id='newSpecForm:specificationNewPanel_body']//tr//td[2]");
	private static By NewSpcCreationMsg = By.xpath("//div[@id='newSpecForm:specificationNewPanel_body']//p//label");
	// Editing New Spec
	private static By NewSpec_LockForEdit_Btn = By.id("newSpecForm:editNewSpec");
	private static By NewSpec_EditSpecHdr_Msg = By.id("editNewSpecPanelHeader");
	private static By NewSpec_EditTypes_Msg = By
			.xpath("//label[contains(@for,'editNewSpecPanelForm:createdSpecWorkflowTypeItems')]");
	private static By NewSpec_EditSpec_OkBtn = By.id("editNewSpecPanelForm:OkButton");
// Search specification locators
	private static By Spec_srchID_txtFld = By.id("specContent:searchIdInput");
	private static By Spec_srchTitle_txtFld = By.id("specContent:searchTitleInput");
	private static By Spec_srchChngeDesc_txtFld = By.id("specContent:searchChangeDescInput");
	private static By Spec_srchChangeRsn_txtFld = By.id("specContent:searchChangeReasonInput");

	private static By Spec_srchBtn = By.id("specContent:searchButton");
	private static By Searchd_SpecIDs = By.xpath("//tbody[@id='specContent:specTable:tb']/tr[1]/td[4]/label");

	// Editing Searchd spec
	private static By SearchdSpec_LockForEdit_Btn = By.id("specContent:editSpec");
	private static By SearchdSpec_EditSpecHdr_Msg = By.id("editSpecPanelHeader");
	private static By SearchdSpec_EditTypes_Msg = By
			.xpath("//label[contains(@for,'editSpecPanelForm:editSpecSelectionRadio')]");
	private static By SearchdSpec_EditSpec_OkBtn = By.id("editSpecPanelForm:OkButton");

//===========================Locator Details End=====================================================================

//===========================Group of KeyWords/functions Starts================================================================

	
	/**
	 * Select new option to create new specification
	 **/
	public static void CreateNewSpec_SelectNewOption() {
		
		Assert.assertTrue(uiDriver.click(Spec_newOptn, "Click on specification New Button"));
		SleepUtils.sleep(TimeSlab.LOW);
	}
	
	
	/**
	 * Select search option
	 **/
	public static void SearchSpec_SelectSearchOption() {
		
		Assert.assertTrue(uiDriver.click(Spec_SearchOptn, "Click on specification Search option"));
		SleepUtils.sleep(TimeSlab.LOW);

	}

	
	/**
	 * Add all the details required for new spec creation
	 **/
	public static void CreateNewSpec_AddGeneralSettings(String TitleName, String ClassID, String SubclassID,
			String Department) {

		Assert.assertNotNull(uiDriver.getValue(NewSpecHeader_Msg, "Get the General Setting Header Title"));
		Assert.assertTrue(uiDriver.setValue(NewSpecTitle, TitleName, "Enter the spec Name"));
		Assert.assertTrue(uiDriver.setValue(Class_Txtfld, ClassID + Keys.ENTER, "Enter Sub Class name"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.setValue(SubClass_Txtfld, SubclassID + Keys.ENTER, "Enter Sub Class name"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.setValue(Department_Txtfld, Department + Keys.ENTER, "Enter the Department Name"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.click(NextBtn, "Click on Next button from General settings"));
		SleepUtils.sleep(TimeSlab.LOW);

	}

	/**
	 * Add workflow configuration to new spec
	 **/
	public static void CreateNewSpec_AddWorkFlowConfig(String WrkFlowID) {

		Assert.assertNotNull(uiDriver.getValue(NewSpecHeader_Msg, "Get the Workflow config Header Title"));
		Assert.assertTrue(uiDriver.clickElementTextByAncestor(WrkFlowRadioBtn, WrokflowID_siblingElement,
						WrkFlowID, "Select the Workflow Config radio Button"));
		Assert.assertTrue(uiDriver.clickUsingJS(NextBtn, "Click on Next button from Workflow Config"));
		SleepUtils.sleep(TimeSlab.LOW);
	}

	
	/**
	 * Confirm the creation of spec
	 **/
	public static void CreateNewSpec_Confirmation(String SuccessMsg) {

		Assert.assertNotNull(uiDriver.getValue(NewSpecHeader_Msg, "Get the Confirmation Header Title"));
		uiDriver.scrollintoView(CreateBtn);
		Assert.assertTrue(uiDriver.clickUsingJS(CreateBtn, "Click on Create button from Confirmation"));
		uiDriver.checkElementPresent(NewSpcCreationMsg, 50);
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.ValidatePartialText(NewSpcCreationMsg, SuccessMsg, "Verify Spec creation success message"));
		Assert.assertNotNull(uiDriver.getValue(NewCreatdSpcID_Msg, "EnvironmentSpecID", "Get the Newly Created Spec ID and store"));
		String SectionName = IniFile.GetInstance().ReadIniFile("START_GLOBAL", "ExecutionSection");
		IniFile.GetInstance().WriteIniFile(SectionName, "EnvironmentSpecName",
				IniFile.GetInstance().ReadIniFile(SectionName, "EnvironmentRandomSpecName"));
	}

	/**
	 * Edit spec
	 **/
	public static void CreateNewSpec_EditSpec(String EditSpecHeaderMsg, String EditSpecID) {
		
		Assert.assertTrue(uiDriver.click(NewSpec_LockForEdit_Btn, "Click on Lock for Edit button from New spec spec Result page"));
		SleepUtils.sleep(TimeSlab.LOW);
		Assert.assertTrue(uiDriver.validateText(NewSpec_EditSpecHdr_Msg, EditSpecHeaderMsg + " " + EditSpecID, "Verified New Spec Edit Type Header message"));
	}
	
	public static void CreateNewSpec_EditSpec() {
		Assert.assertTrue(
				GenericWebObjects.GetInstance().Click(NewSpec_LockForEdit_Btn,
						"Click on Lock for Edit button from New spec spec Result page"),
				"Click on Lock for Edit button from New spec spec Result page");
	}
	
	
	
	/**
	 * Verify edit type
	 **/
	public static void CreateNewSpec_VerifyEditType(String EditTypes) {
		
		Assert.assertTrue(uiDriver.verifyListofTextFromElements(NewSpec_EditTypes_Msg, EditTypes, "Verified New specification Edit types"));

	}

	
	/**
	 * Lock the spec for edit
	 **/
	public static void CreateNewSpec_LockForEditSpec(String EditTypes) {
		
		Assert.assertTrue(uiDriver.clickElementText(NewSpec_EditTypes_Msg, EditTypes, "Selected Editing type for new spec"));
		Assert.assertTrue(uiDriver.click(NewSpec_EditSpec_OkBtn, "Click on Ok button from Edit specification window"));
		SleepUtils.sleep(TimeSlab.LOW);
	}

	public static void CreateNewSpec_EditContentChange(String EditTypes) {
		CreateNewSpec_LockForEditSpec(EditTypes);
	}
	public static void CreateNewSpec_EditContentChange() {
		CreateNewSpec_LockForEditSpec("Content Change");
	}

	public static void CreateNewSpec_EditPropertyChange(String EditTypes) {
		CreateNewSpec_LockForEditSpec(EditTypes);
	}
	public static void CreateNewSpec_EditPropertyChange() {
		CreateNewSpec_LockForEditSpec("Property Change");
	}

	public static void CreateNewSpec_EditTermination(String EditTypes) {
		CreateNewSpec_LockForEditSpec(EditTypes);
	}
	public static void CreateNewSpec_EditTermination() {
		CreateNewSpec_LockForEditSpec("Termination");
	}
	public static void SearchSpecification(String specificationID) {
		Assert.assertNotNull(
				GenericWebObjects.GetInstance().GetText(SpecSearchHeader_Msg, "Get the Search criteria Header Title"),
				"Get the Search criteria Header Title");
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(Spec_srchID_txtFld, specificationID,
				"Enter the spec Id in search field"), "Enter the spec Id in search field");
		Assert.assertTrue(
				GenericWebObjects.GetInstance().Click(Spec_srchBtn, "Click on Search button from specificatio search"),
				"Click on Search button from specificatio search");
		Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(Searchd_SpecIDs, specificationID,
				"Verified Searched Spec ID"), "Verified Searched Spec ID");

	}

	public static void SearchedSpec_EditSpec(String EditSpecHeaderMsg, String EditSpecID) {
		
		Assert.assertTrue(
				GenericWebObjects.GetInstance().Click(SearchdSpec_LockForEdit_Btn,
						"Click on Lock for Edit button from searched spec spec Result page"),
				"Click on Lock for Edit button from searched spec spec Result page");
		Assert.assertTrue(
				GenericWebObjects.GetInstance().ValidateText(SearchdSpec_EditSpecHdr_Msg,
						EditSpecHeaderMsg + " " + EditSpecID, "Verified searched Spec Edit Type Header message"),
				"Verified searched Spec Edit Type Header message");
	}

	public static void SearchedSpec_EditContentChange(String EditTypes) {
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(SearchdSpec_EditTypes_Msg, EditTypes,
				"Selected New Spec Edit type"), "Selected New Spec Edit type");
		Assert.assertTrue(
				GenericWebObjects.GetInstance().clickUsingJS(SearchdSpec_EditSpec_OkBtn,
						"Click on Ok button from Edit specification window"),
				"Click on Ok button from Edit specification window");
	}

	public static void SearchedSpec_EditPropertyChange(String EditTypes) {
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(SearchdSpec_EditTypes_Msg, EditTypes,
				"Selected New Spec Edit type"), "Selected New Spec Edit type");
		Assert.assertTrue(
				GenericWebObjects.GetInstance().clickUsingJS(SearchdSpec_EditSpec_OkBtn,
						"Click on Ok button from Edit specification window"),
				"Click on Ok button from Edit specification window");
	}

	public static void SearchedSpec_EditTermination(String EditTypes) {
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(SearchdSpec_EditTypes_Msg, EditTypes,
				"Selected New Spec Edit type"), "Selected New Spec Edit type");
		Assert.assertTrue(
				GenericWebObjects.GetInstance().clickUsingJS(SearchdSpec_EditSpec_OkBtn,
						"Click on Ok button from Edit specification window"),
				"Click on Ok button from Edit specification window");
	}
//===========================Group of KeyWords/functions Ends================================================================	

}
