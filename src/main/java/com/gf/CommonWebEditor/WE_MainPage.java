package com.gf.CommonWebEditor;

import org.testng.Assert;

import com.gf.common.GenericWebObjects;

public class WE_MainPage {

	
	//==========================================HomePage-Keywords/Functions details Start==========================================
	
		public static void SwitchToEditorWindow()
		{
			
			Assert.assertTrue(GenericWebObjects.GetInstance().SwitchtoNewWindow(),"Switch to Web editor");
			Assert.assertTrue(GenericWebObjects.GetInstance().MaxWait(),"Wait For Loading to new window");
		}
		
		public static void CloseCurrentWindow()
		{
			Assert.assertTrue(GenericWebObjects.GetInstance().CloseTab(),"Current tab is closed");
			Assert.assertTrue(GenericWebObjects.GetInstance().SwitchtoParentWindow(),"Current tab is closed");
		}
		
		public static void GetCurrentWindowTitle()
		{
			Assert.assertNotNull(GenericWebObjects.GetInstance().GetWindowTitle(),"Get Current Window title");
		}

		public static void SwitchToNewWindow()
		{
			
			Assert.assertTrue(GenericWebObjects.GetInstance().SwitchtoNewWindow(),"Switch to New Window");
			Assert.assertTrue(GenericWebObjects.GetInstance().MaxWait(),"Wait For Loading to new window");
		}
		public static void ScrollToBottom()
		{
			Assert.assertTrue(GenericWebObjects.GetInstance().ScrollToBottom("Scroll down to bottom of the screen"),"Scroll down to bottom of the screen");
		}
		
		//==========================================HomePage-Keywords/Functions details Start==========================================
}
