package com.gf.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * 
 * This class is used for sleep utilities.
 * 
 * @author rhalli
 * 
 **/
public class SleepUtils {
	
	private static List<Integer> timerSlabs = new ArrayList<Integer>();

	/**
	 *
	 * Sleep with level of TimeSlab and returns boolean value depending on level value
	 * 
	 * @param level
	 * 		object of TimeSlab enum
	 * @return boolean result
	 * 
	 **/
	public static boolean sleep(TimeSlab level) {
		
		boolean sleep = false;
		int timer = interval(level.getLevel());
		if(timer > 0) {
			try {
				Thread.sleep(timer);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return sleep;
		
	}
	
	
	/**
	 * Enum to define different sleep levels
	 *   
	 **/
	public enum TimeSlab {
		YIELD("YIELD", 0), LOW("LOW", 1), MEDIUM("MEDIUM", 2), HIGH("HIGH", 3);
		
		private TimeSlab(String n, int lvl) {
			name = n;
			level = lvl;
		}
		
		/**
		 * Retrieves level of TimeSlab
		 * 
		 * @return TimeSlab level
		 * 
		 **/
		public int getLevel() {
			return level;
		}
		
		/**
		 * Overridden toString() method of object class to return name for specified level
		 *  
		 **/
		public String toString() {
			return name;
		}
		
		public final String name;
		public final int level;
		
	}
	
	
	/**
	 *
	 * Defines interval for sleep
	 * 
	 * @param lvl
	 * 		level value for TimeSlab
	 * @return interval value
	 * 
	 **/
	public static int interval(int lvl) {
		
		int timer = 0;
		List<Integer> timerSlabs = getTimerSlabs();
		if(lvl >= 0) {
			if(lvl <= timerSlabs.size())
				timer = timerSlabs.get(lvl);
			else
				timer = timerSlabs.get(timerSlabs.size() - 1) * (lvl - timerSlabs.size() + 1);
		}
		
		if(timer < 0) {
			timer = 0;
		}
		
		return timer;
	}
	
	
	/**
	 *
	 * Retrives TimeSlabs levels and returns List
	 * 
	 * @return List of levels
	 * 
	 **/
	public static List<Integer> getTimerSlabs() {
		
		timerSlabs = Arrays.asList(new Integer[] {1000, 3000, 10000, 20000});
		
		return timerSlabs;
	}
	
	
	/**
	 * 
	 * Inserts wait
	 * 
	 *  @param secs
	 *  		Time to sleep(In Seconds)
	 *  
	 **/
	public static void sleep(int secs) {
		try {
			Thread.sleep(secs * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
