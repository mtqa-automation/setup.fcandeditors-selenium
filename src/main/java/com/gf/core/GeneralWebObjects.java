package com.gf.core;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gf.common.GlobalVaraibles;
import com.gf.common.IniFile;
import com.gf.common.SetupFCException;
import com.gf.common.SetupFCTestReport;
import com.gf.core.SleepUtils.TimeSlab;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * Handles all the common functionalities for web controls like setting the textbox,
 * Selecting an option in dropdown, etc
 * 
 * @author rhalli
 *
 * 
 **/
public class GeneralWebObjects {

	private static GeneralWebObjects gwo = null;
	private static Logger logger = Logger.getLogger(GeneralWebObjects.class);
	public static WebDriver driver;
	private static String ParentWindow;
	
	
	public static GeneralWebObjects getFunction() {
		
		if(gwo == null) {
			gwo = new GeneralWebObjects();
		}
		return gwo;
	}
	
	public static void main(String[] args) {

	}

	public void AfterSuite() {

		SetupFCTestReport.Endtest();
		SetupFCTestReport.RemoveInstance();
		CloseBrowser();
	}
	
	private enum Browser {
		CHROME, FIREFOX, IE, SAFARI,HEADLESS,HEADLESSCHROME;
	}
	
	
	public String GetParentWindow() {

		try {
			logger.debug("  Entered into GetParentWindow method");
			ParentWindow = driver.getWindowHandle();
			logger.info("Parent Window ID : "+ ParentWindow);
			logger.debug("  Left from GetParentWindow method");
			return ParentWindow;
		} catch (Exception e) {
			logger.error("error occured while getting Parent Window: " + e);
			AddScreenshortInReport("error occured while getting Parent Window: " + e);
			return " Error Message:" + e.getMessage();
		}
	}
	
	
	/**
	 * Adding Screenshot in the test report with error details
	 * @param errorDetails
	 */
	public void AddScreenshortInReport(String errorDetails) {
		try {
			logger.error(errorDetails);
			String ScrnshtPath = CaptureScreenShot(GlobalVaraibles.TestScriptID);
			String scrnshot = "<img class='r-img' onerror='this.style.display=\"none\"" + "' data-featherlight='"
					+ ScrnshtPath + "' src='" + ScrnshtPath + "' data-src='" + ScrnshtPath + "'>";

			String msg = "<details>" + "<summary>" + "<b>" + "<font color =" + "red>" + "Error Occured: Click to view"
					+ "</font>" + "</b>" + "</summary>" + "<b style=\"background-color:powderblue;color:black;\">"
					+ errorDetails.replaceAll(",", "<br>") + "</b>" + "<br>" + scrnshot + "</details>" + "\n";
			SetupFCTestReport.GetInstance().fail(msg);
			// AfterSuite();

		} catch (Exception e) {
			throw new SetupFCException("Error occured while capturing the screenshot:- ", e);
		}
	}
	
	
	/**
	 * Method used to capture the screenshot of the current screen
	 * @param TestCaseID
	 * @param Action
	 * 
	 */
	public String CaptureScreenShot(String TestCaseID) {

		String _PathofScrnshot = "";
		try {

			logger.debug("  Entered into CaptureScreenShot method");
			TakesScreenshot scrShot = ((TakesScreenshot) driver);
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			DateFormat dft = new SimpleDateFormat("yyyyMMdd_hhmmss");
			String date = df.format(new Date());
			String datetime = dft.format(new Date());
			_PathofScrnshot = GlobalVaraibles.ScreenShotFolderPath + "\\" + date
					+ "\\" +datetime  + "_" + TestCaseID + ".png";
			File DestFile = new File(_PathofScrnshot);
			FileUtils.copyFile(SrcFile, DestFile);
			logger.info("Screenshot is captured and store in : .//" + _PathofScrnshot);
			logger.debug("  Left from CaptureScreenShot method");
			System.out.println(System.getProperty("user.dir"));
			return System.getProperty("user.dir") + "\\"+_PathofScrnshot;
		} catch (Exception e) {

			throw new SetupFCException("Error occured while capturing the screenshot:- " + e);
		}

	}
	
	
	/**
	 * Create webdriver for given browser
	 */
	public WebDriver Driver(String BrowserName) {

		try {

			logger.debug("  Entered into Driver method");
			Browser browser;
			browser = Browser.valueOf(BrowserName.replace(" ", "").toUpperCase().trim());
			switch (browser) {
			case CHROME:
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				break;
			case FIREFOX:
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
				break;

			case IE:
				WebDriverManager.iedriver().setup();
				driver = new InternetExplorerDriver();
				break;
			case HEADLESS:
			case HEADLESSCHROME:
				WebDriverManager.chromedriver().setup();
				ChromeOptions chromeOptions = new ChromeOptions();
	            chromeOptions.addArguments("--no-sandbox");
	            chromeOptions.addArguments("--headless");
	            chromeOptions.addArguments("disable-gpu");

				driver = new ChromeDriver(chromeOptions);
				break;
				
			default:
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				break;
			}
			SetupFCTestReport.GetInstance().pass(BrowserName + " Browser is Launched successfully!");
			logger.info(BrowserName + " Browser is Launched successfully!");
			driver.manage().window().maximize();
			GetParentWindow();
			logger.debug("  Left from Driver method");
		} catch (Exception e) {
			throw new SetupFCException("error occured while getting webdriver: " + e);
		}
		return driver;
	}
	
	
	/**
	 * 
	 * Checks if body of the page is loaded or not
	 * 
	 * @return Boolean result
	 * 
	 **/
	public boolean isBrowserLoaded() {
		
		boolean isBrowserLoaded = false;
		try {
			
			logger.debug("\nEntered isBrowserLoaded method...");
			
			long timeOut = 5000;
			long end = System.currentTimeMillis() + timeOut;
			
			while(System.currentTimeMillis() < end) {
				if(String.valueOf(((JavascriptExecutor) driver).executeScript("return document.ready")).equals("complete")) {
					isBrowserLoaded = true;
					logger.debug("Browser is loaded");
					break;
				}
			}
		} catch(Exception e) {
			logger.error("Error occured in browser loading");
			String msg = e.getMessage();
			AddScreenshortInReport("Error occured in browser loading" + msg);
		}
		
		return isBrowserLoaded;
	}

	
	/**
	 * 
	 * Checks whether the page is loaded or not
	 * 
	 * @param maxTimeInSec Time to wait (In Seconds)
	 * 
	 * @result boolean result
	 * 
	 * */
	public boolean waitForBrowserStability(int maxTimeInSec) {
		
		boolean result = false;
		
		int secsWaited = 0;
		try {
			
			logger.debug("\nEntered waitForBrowserStability method");
			
			do {
				Thread.sleep(100);
				secsWaited++;
				if(isBrowserLoaded()) {
					result = true;
					logger.info("Page loaded");
					break;
				}
				
			} while(secsWaited < (maxTimeInSec * 10));
		} catch(Exception e) {
			logger.error("Error while waiting for the page to load", e);
		}
		
		return result;
	}
	
	
	/**
	 * Launches the Application in the browser
	 * 
	 * @param url
	 * 				URL of the page to be opened
	 * 
	 **/
	public boolean launchApplication(String URL, String Description) {
		
		boolean status = false;
		
		try {
			driver=	Driver(IniFile.GetInstance().ReadIniFile(GlobalVaraibles.ExecutionSection, "EnvironmentBrowser"));
			logger.debug("\nEntered into launchApplication method");
			driver.get(URL);
			
			SetupFCTestReport.GetInstance().pass(Description + "=" + URL);
			logger.info(Description+ "=" + URL );
			status = true;
			logger.debug("Left from the LaunchApplication method");
			
		} catch(Exception e) {
			logger.error(" :Error occured while launching the URL :- " + e);
			SetupFCTestReport.GetInstance().error(":Error occured while launching the URL :- " + e);
		}
		
		return status;
	}
	
	
	/**
	 * 
	 * Checks whether page title matched or not
	 * 
	 * @param pageTitle
	 * 				   title of the page opened
	 * 
	 * @return true or false result
	 * 
	 **/
	public boolean checkPage(String pageTitle) {
		
		SleepUtils.sleep(TimeSlab.LOW);
		
		logger.debug("\nChecking the page title...");
		try {
			if(pageTitle.equals(driver.getTitle())) {
				logger.info("Page title matches");
				return true;
			}
		} catch(Exception e) {
			logger.error("Error in checking the page title: " + pageTitle, e);
			AddScreenshortInReport("Error occured in checking the page title");
			
		}
		return false;
	}
	
	
	
	/**
	 * 
	 * Identifies the locator as needed by the webDriver object
	 * 
	 * @param by
	 * 			
	 * @return Identified locator as web Element
	 * 
	 * */
	public WebElement CreateWebelement(By by) {
		
		WebElement wb =null;
		logger.debug("\nEntered into CreateWebelement method...");
		
		try {
			wb = driver.findElement(by);
			logger.info("Welement is created for the:"+ by.toString());
		} 
		
		catch (Exception e) {
			throw new SetupFCException("Error occured while creating a web element due to:"+e);
			}
		logger.debug("Exited from CreateWebelement method");
		return wb;
	}
 
	
	
	public List<WebElement> CreateWebElements(By by) {
		
		logger.debug("\nEntered into CreateWebElements method...");
		List<WebElement> wb=null;
		try {
			//WaitForPresenceAndVisibilityOfElements(by);
			wb = driver.findElements(by);
			logger.info("Welement is created for the:"+ by.toString());	
		} 
		
		catch (Exception e) {
			throw new SetupFCException("Error occuring while creating the webelements:- " ,e);
		}
		logger.debug("Exited from CreateWebElements method");
		return wb;
	}
	
	
	/**
 	 *  @implNote both Locators should be in form of xpath only
	 * @param by
	 * @param SiblingElement
	 * @param ElementText
	 * @return boolean
	 **/
	private WebElement getWebElementFromAncestor(By by, By Sibling, String ElementText) {
		
		WebElement _wb = null;
		
		try {
			
			logger.debug(" Entered into click method::GetWebElementFromAncestor");
			String SiblingElement = Sibling.toString().replaceAll("By.xpath:", "").trim();
			
			if (waitForElementsPresentAndVisible(by)) {
				List<WebElement> elements = CreateWebElements(by);
				ListIterator<WebElement> ElementsIterator = elements.listIterator();
				
				if (SiblingElement.startsWith("//"))
				SiblingElement = SiblingElement.replaceFirst("//", "");
				
				By SiblingElelemnt = By.xpath(by.toString().replaceAll("By.xpath:", "") + "/ancestor::" + SiblingElement);
				
				System.out.println(SiblingElelemnt);
				
				ListIterator<WebElement> SibLngElementsIterator = CreateWebElements(SiblingElelemnt).listIterator();
				SKIP:
			
					while (ElementsIterator.hasNext()) {
						WebElement wb = ElementsIterator.next();
						while (SibLngElementsIterator.hasNext()) {
							WebElement SibLngWB = SibLngElementsIterator.next();
							if (SibLngWB.getText().contains(ElementText)) {
								logger.info(wb+":: WebElement Siblings/Ancestor::"+SibLngWB+":: And Element Text: "+ElementText);
								return wb;
							}
							continue SKIP;
						}

					}
			}
			logger.info("No WebElement found for the Locator:: "+by+":: with Sibling ::"+Sibling+":: for the element text"+ElementText);
			SetupFCTestReport.GetInstance().error("No WebElement found for the Locator:: "+by+":: with Sibling ::"+Sibling+":: for the element text"+ElementText);
		} catch (Exception e) {
			throw new SetupFCException("Error occured while Geeting Webelement for the Locator:: "+by+":: with Sibling ::"+Sibling+":: for the element text"+ElementText+","+e);		
		}
		return _wb;
	}

	
	
	/**
	 * 
	 * Checks that the element is not present
	 * 
	 * @param by
	 * @param description
	 * 
	 **/
	public boolean elementDoesNotExist(By by, String description) {
		
		boolean status = false;
		
		try {
			
			logger.debug("  Entered into NotExist method");
			WebDriverWait wait = new WebDriverWait(driver, 200);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			List<WebElement> wb = CreateWebElements(by);
			
			if (wb.size() ==0) {
				logger.info(description);
				SetupFCTestReport.GetInstance().pass(description);
				status = true;
			}
			else {
				AddScreenshortInReport(description+ ": found in locator: "+ by);

			}
			
			logger.debug("  Left from NotExist method");
		} catch (Exception e) {
			AddScreenshortInReport("Error occured while verifying ["+description +" ]"+ e);

		}
		return status;
	}

	
	/**
	 * 
	 * Checks that the element is not present
	 * 
	 * @param by
	 * @param description
	 * 
	 **/
	public boolean elementExist(By by, String Description) {
		
		boolean status = false;
		try {
			logger.debug("  Entered into Exist method");

			if (waitForElementsPresentAndVisible(by)) {
				SetupFCTestReport.GetInstance().pass(Description);
				logger.info(Description);
				status = true;
			} else {
				AddScreenshortInReport(Description + ": is not found for locator: " + by);

			}
			logger.debug("  Left from Exist method");
		} catch (Exception e) {
			AddScreenshortInReport("Error occured while verifying  for["+Description +" ]" + e);

		}
		return status;
	}

	
	
	/**
	 * Scroll into view of the element
	 *  
	 **/
	public boolean scrollintoView(By by) {
		
		boolean status = false;
		WebElement ele = CreateWebelement(by);
		
		try {			
			logger.debug("\nScroll into view of the element started...");
			
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({block: 'center', inline: 'nearest'})", ele);
			logger.info("Scroll in to view of element success");
			status = true;
			
		} catch(Exception e) {
			
			logger.error("Error in scrolling in to the view.");
			AddScreenshortInReport(":Error occured while Scrolling to view element :- " + e);
			
		}
		
		return status;
	}
	
	
	/**
	 * 
	 * Add red border to element for verification
	 * 
	 * @param by
	 *
	 **/
	public void highlightElement(By by) {
		
		WebElement ele = CreateWebelement(by);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', arguments[1])", ele, 
				"color: red; border: 2px solid red;");
	}
	
	
	/**
	 * 
	 * Remove added border to element after verification
	 * 
	 * @param by
	 *
	 **/
	public void dehighlightElement(By by) {
		
		WebElement ele = CreateWebelement(by);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', arguments[1])", ele, "");
	}
	
	
	
	/**
	 * Gets the count of web elements present in the webpage
	 * 
	 * @param by
	 * @return Integer
	 * 
	 **/
	public Integer getCountOfWebElementPresent(By by) {
		
		try {
			return driver.findElements(by).size();
		} catch(Exception e) {
			logger.error("Failed to getCountOfWebElementPresent");
		}
		return 0;
	}
	
	
	
	/**
	 * 
	 * Checks the presence of element till timeout
	 * 
	 * @param by
	 * 			locator of the element
	 * 
	 * @param timeInSec
	 * 			Time limit
	 * 
	 * @return boolean result
	 * 
	 **/
	public boolean checkElementPresent(final By by, int timeInSec) {
		
		boolean result = false;
		
		try {
			
			ExpectedCondition<Boolean> exception = new ExpectedCondition<Boolean>() {
				
				public Boolean apply(WebDriver webDriver) {
					WebElement wb = CreateWebelement(by);
					if(wb != null) {
						return true;
					} else {
						return false;
					}
				}
			};
			
			Wait<WebDriver> wait = new WebDriverWait(driver, timeInSec);
			result = wait.until(exception);
		
		} catch(Exception e) {
			logger.error("Error caused while checkElementPresent \" " + by + " \"", e);
		}
		
		return result;
	}
	
	
	public boolean waitForElementsPresentAndVisible(By by) {

		boolean status = false;
		try {
			logger.debug("  Entered into WaitForPresenceAndVisibilityOfElement method");
			WebDriverWait wait = new WebDriverWait(driver, 200);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
			status=true;
			
			logger.debug("  Left from WaitForPresenceAndVisibilityOfElement method");
		}  catch (Exception e) {
			AddScreenshortInReport("Unable to locate the web elements in the page for:" + by);
			throw new SetupFCException("Error occured while waiting for PresenceAndVisibilityOfElement:-"+by+","+e);
		}
		return status;
	}
	
	
	/**
	 * 
	 * Checks whether the element is present and visible on the page
	 * 
	 *  @return boolean
	 *  
	 **/
	public boolean IsElementPresentAndVisible(By by) {

		boolean status = false;
		try {
			logger.debug("\nEntered into CreateWebelement method");
			
			List<WebElement> wb = driver.findElements(by);
			if (wb.size() > 0) {
				
				Iterator<WebElement> it = wb.iterator();
				while(it.hasNext())
					if(it.next().isDisplayed())	
						status = true;
			}
			logger.debug("  Left from CreateWebelement method");

		} catch (Exception e) {
			throw new SetupFCException("  Error occuring while creating the webelement :- " ,e);
		}
		return status;
	}
	
	
	
	/**
	 * 
	 * Waits for the element to be clickable
	 * 
	 * @param by locator of the element
	 * @param description 
	 * 
	 * @return boolean status
	 * 
	 **/
	public boolean waitForElementToBeClickable(By by) {
		
		boolean status = false;
		
		try {
			logger.debug("  Entered into waitForElementToBeClickable method");
			WebDriverWait wait = new WebDriverWait(driver, 150);
			wait.until(ExpectedConditions.elementToBeClickable(by));
			status = true;
		}
		catch (StaleElementReferenceException elementHasDisappeared) {
			logger.warn(elementHasDisappeared.getMessage());
			waitForElementToBeClickable(by);
		} 
		
		catch (Exception e) {
			AddScreenshortInReport("Error occured while Getting the Text :- " + e);
			throw new SetupFCException("Error occuring while waiting for web element visibility :- " + e);
		}
		
		
		return status;
	}
	
	
	/**
	 * 
	 * Switch to frame using frame locator
	 * 
	 **/
	public boolean switchToFrame(By by) {
		
		boolean _status = false;
		
		try {
			logger.debug("\nSwitch to frame method started...");
			
			WebElement ele = CreateWebelement(by);
			driver.switchTo().frame(ele);
			
			logger.trace("Successfully switched to frame");
			
			_status = true;
		} catch(Exception e) {
			logger.error("Error in switching frame using locator '" + by + "'");
			AddScreenshortInReport("Error ocurred while switching to frame");
		}
				
		return _status;
	}
	
	
	/**
	 * 
	 * Switch to frame using frame int
	 * 
	 **/
	public boolean switchToFrame(int frameInt) {
		
		boolean status = false;
		
		try {
			
			logger.debug("\nSwitch to frame method started...");
			
			driver.switchTo().frame(frameInt);
			
			logger.trace("Successfully switched to frame");
			
			status = true;
		} catch(Exception e) {
			logger.error("Error in switching frame using frame number");
			AddScreenshortInReport("Error ocurred while switching to frame");
		}
		
		return status;
	}
	
	
	
	/**
	 * 
	 * Check the presence of alert
	 * 
	 * @return boolean
	 * 
	 **/
	public boolean isAlertPresent() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 240);
			wait.until(ExpectedConditions.alertIsPresent());
			return true;
		} 
		catch (NoAlertPresentException Ex) {
			return false;
		}
	}
	
	
	/**
	 * 
	 * Switch to Alert
	 *
	 **/
	private Alert switchToAlert() {

		try {
			logger.debug(": entered to SwitchtoAlert method");
			if (isAlertPresent()) {
				return driver.switchTo().alert();

			}
			logger.debug(": Left the SwitchtoAlert method");

		} catch (Exception e) {
			logger.error("  Error occuring while Switching to alert window :- " + e);
		}
		return null;
	}
	
	
	
	/**
	 * Accepted Alert commands---Done,Accept,Ok,Cancel,Close,No
	 * @param AlertCmnd
	 * @param Description
	 * @return boolean value
	 * 
	 */
	public boolean CloseAlertWindow(String AlertCmnd, String Description) {
		
		boolean _status = false;
		
		try {
			
			logger.debug("\nClose the Alert method started...");
			
			Alert alert = switchToAlert();
			switch (AlertCmnd.toUpperCase()) {
			case "DONE":
			case "ACCEPT":
			case "OK":
				alert.accept();
				break;
			case "CANCEL":
			case "CLOSE":
			case "NO":
				alert.dismiss();
				break;
			default:
				alert.dismiss();
				break;

			}
			 driver.switchTo().activeElement();
			SetupFCTestReport.GetInstance().pass(Description + "=" + AlertCmnd);
			_status = true;
		} catch (Exception e) {
			logger.error("Error occured while Closing the alrt window: " + e);

		}
		return _status;
	}
	
	
	/**
	 * Switch to default web page
	 * 
	 */
	public  boolean switchToDefaultWebPage() {
		
		boolean status=false;
		logger.debug("\nEntered switch to default web page method...");
		
		try {
			 driver.switchTo().defaultContent();
			 logger.info("Web driver switched to Default web page");
			 status=true;
		} catch (Exception e) {
			logger.error("Error occuring while Swithcing to default web page :- " + e);
		}
		
		return status;
	}
	
	
	/**
	 * 
	 * Get window handle
	 * 
	 **/
	public String getCurrentWindowHandle() {
		
		try {
			return driver.getWindowHandle();
		} catch(Exception e) {
			logger.error("No window handle is available to return :- " + e);
			AddScreenshortInReport("No Window handle is available to return");
			return " Error Message:" + e.getMessage();
		}
		
	}
	
	/**
	 * 
	 * */
	
	
	/**
	 * 
	 * Get the attribute value
	 * 
	 * @return attribute value
	 * 
	 **/
	public String getAttributeValue(By by, String attribute, String Description) {
		
		String Value = "";
		try {
			logger.debug("\nEntered into GetAttributeValue method");
				WebElement wb = CreateWebelement(by);
				Value = wb.getAttribute(attribute);
				SetupFCTestReport.GetInstance().pass(Description + " =" + Value);
				logger.info(Description + " =" + Value);
			
			
		} catch (Exception e) {
			logger.error(":Error occured while GetAttributeValue :- " + e);
			AddScreenshortInReport(":Error occured while GetAttributeValue :- for["+Description +" ]" + e);
		}
		logger.debug(":Left from the GetAttributeValue method");
		return Value;
		
	}
	
	
	/**
	 * 
	 * Get the value of the web element
	 * 
	 * @param by
	 * @param description
	 * @return String
	 */
	public String getValue(By by, String description) {
		
		String value = null;
		WebElement ele = null;
		
		if(!checkElementPresent(by, 200)) {
			logger.error("\nUnable to find the element " + by);
		}
		
		ele = CreateWebelement(by);
		
		try {
			
			logger.debug("\nEntered into getValue method");
			String type = ele.getAttribute("type");
			
			if(type == null || type.equals("text") || type.equals("password") || type.equals("")
					|| type.equals("textarea") || type.equals("hidden")) {
				
				if(ele.getText().equals("")) {
					return getAttributeValue(by, "value", description);
				}
				return ele.getText();
			}
			
			if(type.equals("select-one")) {
				
				Select selectOption = new Select(ele);
				return selectOption.getFirstSelectedOption().getText();
			}
			
			if(type.equals("checkbox") || type.equals("radio")) {
				
				if(ele.isSelected() || ele.isEnabled()) {
					return "Yes";
				} else {
					return "No";
				}
			}
			
			logger.info(description);
			
		}catch (StaleElementReferenceException elementHasDisappeared) {
			logger.warn(elementHasDisappeared.getMessage());
			value =	getValue(by, description);
		} 
		catch (Exception e) {
			logger.error(":Error occured while GetText :- " + e);
			AddScreenshortInReport("Error occured while Getting the Text [" + description + " ]" + e);

		}
		logger.debug(">>>Exited from  GetText method");
		return value;
	}
	
	
	/**
	 * Get the value of the web element and store the value
	 * 
	 * @param by locator of the webelement
	 * @param StoreKey name of the store key where the value needs to be stored
	 * @param description  
	 *
	 * @return String
	 * 
	 **/
	public String getValue(By by, String StoreKey, String description) {
		
		String value = getValue(by, description);
		if (StoreKey != null && !StoreKey.isEmpty()) {
			
			IniFile.GetInstance().WriteIniFile(GlobalVaraibles.ExecutionSection, StoreKey, value);
			SetupFCTestReport.GetInstance()
					.pass("[" + StoreKey + "] value is updated with [" + value + "] in the section "
							+ GlobalVaraibles.ExecutionSection);
			logger.info(description);
		}
		
		return value;
	}
	
	
	
	private String[] getArrayofTextFrmElement(By by, String Description) {
		
		logger.debug("  Entered into GetArrayofTextFrmElement method");
		
		try {
		
				List<WebElement> ListofElements = CreateWebElements(by);
				int temp = ListofElements.size(), temp1 = 0;
				String[] ArrayofElementTxt = new String[temp];
				ListIterator<WebElement> webelementsIterator = ListofElements.listIterator();
				while (webelementsIterator.hasNext()) {

					ArrayofElementTxt[temp1] = webelementsIterator.next().getText();
					temp1++;
				}
				return ArrayofElementTxt;
			
		} catch (Exception e) {
			AddScreenshortInReport("Error occured while verifying the element: " + e);
		} 
		
		logger.debug("  Exiting from GetArrayofTextFrmElement method");
		return null;
	}
	
	
	/**
	 * Verify list of text from the element
	 * 
	 * @param by Locator of the element
	 * @param values Values to be validated
	 * @param Description
	 * 
	 **/
	public boolean verifyListofTextFromElements(By by, String Values, String Description) {
		
		boolean status = false;
		
		try {
			String[] ArrayofTexts = getArrayofTextFrmElement(by, Description);
			String[] arrayofValues = Values.split("~#~");
			Arrays.sort(ArrayofTexts);
			Arrays.sort(arrayofValues);
			
			if (Arrays.deepEquals(ArrayofTexts, arrayofValues)) {
				SetupFCTestReport.GetInstance().pass(Description + "=" + Values);
				status = true;
			} else
				AddScreenshortInReport("Mismatch in array of values and expected values: " + Values);
		} catch (Exception e) {
			AddScreenshortInReport("Error occured while verifying for[" + Description + " ]" + e);
		}
		return status;
	}
	
	
	/**
	 * Clears the value of the specified element
	 * 
	 * @param by locator of the webelement to be cleared
	 * 
	 * @return boolean
	 * 
	 **/
	public boolean clearText(By by, String Description) {
		
		boolean status = false;
		try {
			
			logger.debug("\nclearText method started...");
			CreateWebelement(by).clear();
			SetupFCTestReport.GetInstance().pass(Description);
			logger.info(Description);
			status = true;
		}
		
		catch(StaleElementReferenceException eleNotPresent) {
			logger.warn(eleNotPresent.getMessage());
			status = clearText(by, Description);
		}
		
		catch(Exception e) {
			logger.error(Description+" :Error occured while Clearing the Text :- " + e);
			AddScreenshortInReport(Description+" :Error occured while Clearing the Text :- " + e);
		}
		
		return status;
	}
	
	
	
	/**
	 * 
	 * Sets the value to the specified web element
	 * 
	 * @param by
	 * 			locator
	 * @param value 
	 * 			value to be set on element
	 * @param description
	 * 
	 * @return boolean status
	 * 			
	 **/
	public boolean setValue(By by, String value, String description) {
		
		boolean status = false;
		WebElement ele = null;
		
		waitForElementsPresentAndVisible(by);
		waitForElementToBeClickable(by);
		
		if(!checkElementPresent(by, 150)) {
			logger.error("\nUnable to find the element - " + by);
		}
		
		ele = CreateWebelement(by);
		try {
			logger.debug("\nEntered setValue method");
			
			String type = ele.getAttribute("type");
			
			if(type == null) {
				ele.click();
				ele.clear();
				ele.sendKeys(value);
				SetupFCTestReport.GetInstance().pass(description + " = " + value);
			}
			
			if(type.equalsIgnoreCase("Text") || type.equals("password") || type.equals("textarea") || type.equals("email") 
					|| type.equals("search")) {
				
				ele.click();
				ele.clear();
				ele.sendKeys(value);
				
				if(by.toString().toLowerCase().contains("password") || by.toString().toLowerCase().contains("pwd") 
						|| description.toLowerCase().contains("password")) {
					logger.info(description + " = " + "*********");	
					SetupFCTestReport.GetInstance().pass(description + " = " + "*********");
				}
				else {
					logger.info(description + " = " + value);
					SetupFCTestReport.GetInstance().pass(description + " = " + value);
				
				}
			}
			
			if(type.equals("select-one")) {
				Select selectOption = new Select(ele);
				selectOption.selectByVisibleText(value);
				logger.info(description + " = " + value);
				SetupFCTestReport.GetInstance().pass(description + " = " + value);
			}
			
			if(type.equalsIgnoreCase("calendar")) {
				
			}
			
			status = true;
			logger.debug("Value is set to the element successfully");
		} 
		
		catch (StaleElementReferenceException elementHasDisappeared) {
			logger.warn(elementHasDisappeared);
			status=setValue(by, value, description);
		} 
		catch(Exception e) {
			logger.error("Error while setting value ' " + value + " on element " + by, e);
			AddScreenshortInReport("Error while setting value ' " + value + " on element " + by);
		}
		
		return status;
	}
	
	
	/**
	 * 
	 * Validates if the two text matches
	 * 
	 * @param by
	 * @param text Text to be compared
	 * @param description
	 * 
	 * @return boolean status
	 * 
	 **/
	public boolean validateText(By by, String text, String description) {
		
		boolean status = false;
		String extractedText = getValue(by, description);
		
		try {
			
			logger.debug("\nEntered the validateText method");
			if(extractedText.equalsIgnoreCase(text)) {
				SetupFCTestReport.GetInstance().pass(description + ":- Expected value [ " + text + " ] and Actual Value [" + extractedText + " ]");
				logger.info(description + ":- Expected value [ " + text + " ] and Actual Value [" + extractedText + " ]");
				status = true;
			}
		} catch(Exception e) {
			logger.error("Error while comparing text");
			AddScreenshortInReport("Error while comparing text");
		}
		
		return status;
	}
	
	
	/**
	 * @param by
	 * @param Text
	 * @param Description
	 * @return
	 */
	public boolean ValidatePartialText(By by, String Text, String Description) {
		
		boolean status = false;
		String text = getValue(by, Description);
		
		if (text.contains(Text)) {
			SetupFCTestReport.GetInstance()
					.pass(Description + ":- Expected value [ " + Text + " ] and Actual Value [" + text + " ]");
			logger.info(Description  + ":- Expected value [ " + Text + " ] and Actual Value [" + text + " ]");
			status = true;
		} else {
			AddScreenshortInReport(Description + ":-Expected value [" + Text + " ] But Actual Value [ " + text + " ]");
			logger.error(Description + "[Expected value] " + Text + "[But Actual Value] " + text);
		}
		return status;

	}
	
	
	
	/**
	 * Click on the element
	 * 
	 * @param by
	 * 			locator of the element
	 * @param description
	 * 			
	 **/
	public boolean click(By by, String description) {
		
		boolean status = false;
		WebElement wb = null;
		
		try {
			
			logger.debug("\nEntered into click method");
			wb = CreateWebelement(by);
			waitForElementsPresentAndVisible(by);
			waitForElementToBeClickable(by);
			wb.click();
			SetupFCTestReport.GetInstance().pass(description);
			logger.info(description);
			
			status = true;
		} 
		
		catch(StaleElementReferenceException staleEle) {
			logger.warn("Retry the click method due to :- " + staleEle);
			click(by, description);
		}
	    catch(Exception e) {
	    	logger.error(description + " : Error occured while clicking :- " + e);
			AddScreenshortInReport(description + " : Error occured while clicking :- " + e);
		}
		
		return status;
	}
	
	/**
	 * 
	 * Click on web elements using actions
	 * 
	 * @param by locator of the element
	 * @param description
	 * 
	 * @return boolean status
	 * 
	 **/
	public boolean clickUsingActions(By by, String description) {
		
		boolean status = false;
		WebElement wb = null;
		
		try {
			logger.debug("\nEntered clickUsingActions method...");
			
			wb = CreateWebelement(by);
			Actions ac = new Actions(driver);
			ac.moveToElement(wb).click().build().perform();
			SetupFCTestReport.GetInstance().pass(description);
			logger.info(description);
			
			status = true;
		}
		
		catch(StaleElementReferenceException staleEle) {
			logger.warn("Retry clicking due to :- " + staleEle);
			clickUsingActions(by, description);
		}
		
		catch(Exception e) {
			logger.error("Error occured while clicking element using Actions :- " + e);
			AddScreenshortInReport(description + " Error occured :- " + e);
		}
		
		return status;
		
	}
	
	
	/**
	 * 
	 * Click on web elements using javascript
	 * 
	 * @param by locator of the element
	 * @param description
	 * 
	 * @return boolean status
	 * 
	 **/
	public boolean clickUsingJS(By by, String description) {
		
		boolean status = false;
		WebElement wb = null;
		
		try {
			logger.debug("\nEntered clickUsingJS method...");
			
			wb = CreateWebelement(by);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", CreateWebelement(by));
			SetupFCTestReport.GetInstance().pass(description);
			logger.info(description);
			
			status = true;
		}
		
		catch(StaleElementReferenceException staleEle) {
			logger.warn("Retry clicking due to :- " + staleEle);
			clickUsingJS(by, description);
		}
		
		catch(Exception e) {
			logger.error("Error occured while clicking element using JS :- " + e);
			AddScreenshortInReport(description + " Error occured :- " + e);
		}
		
		return status;
		
	}
	
	
	/**
	 * @param by
	 * @param ElementText
	 * @param Description
	 * @return boolean
	 */
	public boolean clickElementText(By by, String ElementText, String Description) {
		
		boolean status = false;
		WebElement wb=null;
		
		try {
			logger.debug("~~~~~Entering to ClickElementText method for "+Description+"~~~~~~~~");
				wb = getWebElementFromText(CreateWebElements(by), ElementText);
				wb.click();
				SetupFCTestReport.GetInstance().pass(Description + "=" + ElementText);
				logger.info(Description + "=" + ElementText);
				status = true;
			
		}catch (StaleElementReferenceException elementHasDisappeared) {
			status=clickElementText(by,ElementText,Description);
		} 
		catch (Exception e) {

			logger.error(Description+":Error occured while clicking :- " + e);
			AddScreenshortInReport(Description+" :Error occured while Clicking for ElementText :- " +ElementText+ e);
		}
		logger.debug("~~~~~Exiting from ClickElementText method for "+Description+"~~~~~~~~");
		return status;
	}
	
	
	
	/**
	 * @implNote both Locators should be in form of xpath only
	 * @param by
	 * @param SiblingElement
	 * @param ElementText
	 * @param Description
	 * @return boolean
	 */
	public boolean clickElementTextByAncestor(By by, By Sibling, String ElementText, String Description) {
		boolean _status = false;
		try {
			logger.debug("~~~~~Entering to ClickElementTextByAncestor method for "+Description+"~~~~~~~~");
			WebElement wb = getWebElementFromAncestor(by,Sibling,ElementText);
				wb.click();
				SetupFCTestReport.GetInstance().pass(Description + "=" + ElementText);
				logger.info(Description + "=" + ElementText);
				_status=true;
				
		}
		catch (StaleElementReferenceException elementHasDisappeared) {
			logger.debug(Description+"Retyring due to:: "+elementHasDisappeared.getMessage());
			clickElementTextByAncestor(by,Sibling,ElementText,Description);
		} 
		catch (Exception e) {

			logger.error(Description+":Error occured while clicking :- " + e);
			AddScreenshortInReport(Description+" :Error occured while Clicking for ElementText :- " + e);
		}
		logger.debug("~~~~~Exiting from ClickElementTextByAncestor method for "+Description+"~~~~~~~~");
		return _status;
	}


	
	
	/**
	 * @param WebElements
	 * @param Text
	 * @return WebElement
	 */
	private WebElement getWebElementFromText(List<WebElement> WebElements, String Text) {
		WebElement _wb = null;

		List<WebElement> elements = WebElements;
		Iterator<WebElement> element_iterator = elements.iterator();
		while (element_iterator.hasNext()) {
			_wb = element_iterator.next();
			if (_wb.getText().equalsIgnoreCase(Text))
				break;
		}
		return _wb;
	}
	
	
	/**
	 * Performs drag and drop operation
	 * 
	 * @param fromLocator contains the locator of source element
	 * @param toLocator contains the locator of the destination element
	 * 
	 **/
	public boolean dragAndDrop(By fromLocator, By toLocator, String description) {
		
		boolean status = false;
		WebElement from = null;
		WebElement to = null;
		
		try {
			
			logger.debug("\ndragAndDrop method started...");
			
			from = CreateWebelement(fromLocator);
			to = CreateWebelement(toLocator);
			
			Actions ac = new Actions(driver);
			ac.clickAndHold(from).moveToElement(to).release().build().perform();
			
			SetupFCTestReport.GetInstance().pass(description);
			logger.info(description);
			
			status = true;
		
		} catch(Exception e) {
			logger.error("Error occured while clicking element using JS :- " + e);
			AddScreenshortInReport(description + " Error occured :- " + e);
		}
		
		return status;
	}
	
	
	
	/**
	 * Closing the entire browser
	 * @return
	 */
	public boolean CloseBrowser() {
		try {
			if (((RemoteWebDriver) driver).getSessionId() == null)
				return true;
			driver.quit();
			logger.info("Closing the browser");
			SetupFCTestReport.GetInstance().pass("Browser is closed!");
		} catch (SessionNotCreatedException e) {
			logger.info("Browser is already closed " + e);
			SetupFCTestReport.GetInstance().fail("Browser is already closed " + e);

		} catch (Exception e) {
			logger.info("Browser is already closed " + e);
			SetupFCTestReport.GetInstance().fail("Browser is already closed " + e);
		}

		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
}
