package com.gf.WEBase;



import org.openqa.selenium.By;
import org.testng.Assert;
import com.gf.common.GenericWebObjects;


public class WEB_HomePage {
	
	
	//==========================================HomePage-Locaters details Start==========================================
	
	//Editor Header Details
	private static By HomePage_Title=By.xpath("//span[@class='navbar-title']");
	private static By HomePage_User=By.xpath("//a[@id='account-menu']//span/span");
	
	//Editor Menu Details
	private static By Menu_SpecificationDrpDown=By.xpath("//a[@id='menu-spec']");
	private static By Menu_AddDrpDown=By.xpath("//a[@id='menu-add']");
	
	//Specification Metainformation
	private static By MetaInfo_SpecIDWithVrsn=By.xpath("//table[@id='MetaInformation']//tbody//tr//th[1]");
	private static By MetaInfo_SpecStatus=By.xpath("//table[@id='MetaInformation']//tbody//tr//th[@colspan='2']");
	private static By MetaInfo_SpecName=By.xpath("//table[@id='MetaInformation']//tbody//tr//td[@colspan='3']");
	private static By MetaInfo_Author=By.xpath("//span[contains(text(),'Author:')]/following-sibling::span");
	private static By MetaInfo_Department=By.xpath("//span[contains(text(),'Department:')]/following-sibling::span");
	private static By MetaInfo_Owner=By.xpath("//span[contains(text(),'Owner:')]/following-sibling::span");
	private static By MetaInfo_WorkflowConfiguration=By.xpath("//span[contains(text(),'WorkflowConfiguration:')]/following-sibling::span");
	private static By MetaInfo_SubClass=By.xpath("//span[contains(text(),'SubClass:')]/following-sibling::span");
	private static By MetaInfo_GroupAccessLevel=By.xpath("//span[contains(text(),'GroupAccessLevel:')]/following-sibling::span");
	private static By MetaInfo_Template=By.xpath("//span[contains(text(),'Template:')]/following-sibling::span");
	private static By MetaInfo_UseCase=By.xpath("//span[contains(text(),'UseCase:')]/following-sibling::span");
	
	
	
	//Change Main view details
	private static By PCRB_Txtfld=By.id("field_pcrb");
	private static By ChngDesc_Txtfld=By.id("field_changeDescription");
	private static By ChngRsn_Txtfld=By.id("field_changeReason");
	private static By Purpose_Txtfld=By.xpath("//*[@id='edit-purpose']//editor/div");
	private static By Scope_Txtfld=By.xpath("//*[@id='edit-scope']//editor/div");
	private static By Definitions_Txtfld=By.xpath("//gf-text-editor[@id='edit-definitions']//editor");

	//Mandate fields With Error text fields
	private static By ChngDesc_ErrorTxtfld=By.xpath("//*[ @id='field_changeReason' and contains(@class,'invalid')]");
	private static By ChngRsn_ErrorTxtfld=By.xpath("//*[ @id='field_changeReason' and contains(@class,'invalid')]");
	private static By Purpose_ErrorTxtfld=By.xpath("//*[@id='edit-purpose']//editor[contains(@class,'invalid')]");
	private static By Scope_ErrorTxtfld=By.xpath("//*[@id='edit-scope']//editor[contains(@class,'invalid')]");
	
	//Mandate fields Error message
	private static By ChngDesc_ErrorMsg=By.xpath("//*[@id='field_changeDescription']/following-sibling::div//small");
	private static By ChngRsn_ErrorMsg=By.xpath("//*[@id='field_changeReason']/following-sibling::div//small");
	private static By Purpose_ErrorMsg=By.xpath("//*[@id='edit-purpose']//div//small");
	private static By Scope_ErrorMsg=By.xpath("//*[@id='edit-scope']//div//small");
	
	
	
	//Specifications and Add drop down list details
	private static By SpecificationDrpDwn_List=By.xpath("//a[@id='menu-spec']/following-sibling::p-menubarsub//span[@class='ui-menuitem-text']");
	private static By AddDrpDwn_List=By.xpath("//a[@id='menu-add']/following-sibling::p-menubarsub//span[@class='ui-menuitem-text']");
	
	//************Add the details to Spec*****************
	
	//File upload details
	private static By AttChmnt_FileUploadHdr=By.className("ui-dialog-title");
	private static By AttChmnt_ChooseFileBtn=By.xpath("//span[contains(@class,'ui-fileupload-choose ui-button')]");
	private static By AttChmnt_UploadFile=By.xpath("//input[@type='file']");
	private static By AttChmnt_UploadFileBtn=By.xpath("//span[@class='ui-button-text ui-clickable'][contains(text(),'Upload')]");
	
	//Add Reference details
	private static By Reference_AddRefer_HdrTitle= AttChmnt_FileUploadHdr;
	private static By Reference_AddRefer_SpecID_txtfld=By.id("inputSpecId");
	private static By Reference_AddRefer_Search_Btn=By.id("start-search");
	private static By Reference_AddRefer_ResultGrid=By.className("ui-table-thead");
	private static By Reference_AddRefer_Result_FrstRow=By.xpath("//table[@class='ui-table-scrollable-body-table']//tbody[@class='ui-table-tbody']/tr[1]");
	private static By Reference_AddRefer_Ok_Btn=By.id("add-ref-ok");
	private static By Reference_AddRefer_Cancel_Btn=By.xpath("//div[@class='modal-footer']//button//span[text()='Cancel']");

	//Add PLM Reference details
	private static By PLMReference_AddRefer_HdrTitle=Reference_AddRefer_HdrTitle;
	private static By PLMReference_AddRefer_SpecID_txtfld=Reference_AddRefer_SpecID_txtfld;
	private static By PLMReference_AddRefer_Ok_Btn=By.id("confirm-add-plm-reference");;
	private static By PLMReference_AddRefer_Cancel_Btn=Reference_AddRefer_Cancel_Btn;

	//Add Keyword details
	private static By KeyWord_AddKeyword_HdrTitle=Reference_AddRefer_HdrTitle;
	private static By KeyWord_AddKeyword_EqupmntTxtFild=By.xpath("//div[@class='ui-listbox-filter-container ng-star-inserted']/input");
	private static By KeyWord_AddKeyword_SelectFltrdEquipmntID=By.xpath("//ul[@class='ui-listbox-list']//li[@style='display: block;'][1]");
	private static By KeyWord_AddKeyword_Ok_Btn=By.id("confirm-keyword-add");
	
	//Add Additional Chapter
	private static By Chapter_NewChapter_Btn=By.xpath("//button[@id='add-additional-chapter']");
	private static By Chapter_Chapter5Title_txtfld=By.xpath("//span[@id='chapter-number' and text()='5.']/ancestor::div[@class='input-group']/input");
	private static By Chapter_InvalidChapter5Title=By.xpath("//span[@id='chapter-number' and text()='5.']/ancestor::div[@class='input-group']//input[contains(@class,'ng-invalid')]");
	private static By Chapter_validChapter5Title=By.xpath("//span[@id='chapter-number' and text()='5.']/ancestor::div[@class='input-group']//input[contains(@class,'ng-valid')]");
	private static By Chapter_Chapter5MoreOption=By.xpath("//span[@id='chapter-number' and text()='5.']/ancestor::div[@class='input-group']//button");
	private static By Chapter_Chapter5Description=By.xpath("//span[@id='chapter-number' and text()='5.']/ancestor::div[@class='ng-star-inserted']//gf-text-editor[@id='edit-chapter-text']//editor//p");
	
	//----List of Additional Chapter
	private static By ListofChaptersNmbr=By.xpath("//div//span[@id='chapter-number']");
	private static By ListofChapterTitle_Txtfld=By.xpath("//div[@class='input-group']/input");
	private static By ListofChapterDescription_Txtfld=By.xpath("//div[@class='ng-star-inserted']//gf-text-editor[@id='edit-chapter-text']//editor//div");
	private static By ListofChpaterMoreOption=By.xpath("//div[@class='input-group']//button");
	
	//---Additional Chapter by Input Locator
	private static By Chapter_ChapterTitle_Byinput=By.xpath("//span[@id='chapter-number' and text()='$ChartNumber$']/ancestor::div[@class='input-group']/input");
	
	//Chapter More Option detail
	private static By Chapter_MoreOption_DeleteChapter=By.id("menu-chapter-delete");
	private static By Chapter_MoreOption_AddChapterAbove=By.id("menu-chapter-add-above");
	private static By Chapter_MoreOption_AddChapterBelow=By.id("menu-chapter-add-below");
	private static By Chapter_MoreOption_AddSubChapter=By.id("menu-chapter-add-sub");
	private static By Chapter_MoreOption_AddAttachment=By.id("menu-chapter-add-attachment");
	private static By Chapter_MoreOption_OpenEditor=By.id("menu-chapter-open-editor");
	
	//Chapter -Add Attachment Window
	private static By Chapter_AddAttchmentHdr=AttChmnt_FileUploadHdr;
	private static By Chapter_AddAttchmnt_UploadNewBtn=By.id("ui-tabpanel-1-label");
	private static By Chapter_AddAttchmnt_ChooseFileBtn=AttChmnt_ChooseFileBtn;
	private static By Chapter_AddAttchmnt_UploadFile=AttChmnt_UploadFile;
	private static By Chapter_AddAttchmnt_UploadFileBtn=AttChmnt_UploadFileBtn;
	
	
	//**********Added Details in Spec*******************
	//Added Attachment List details
	private static By AttChmnt_SpecAttachmentSection=By.xpath("//div[@id='attachments-container']");
	private static By AttChmnt_ListofAddedAttachmentsToSpec=By.xpath("//div[@id='attachments-container']//fa-icon/parent::div");
	private static By AttChmnt_ViewBtn=By.xpath("//div[@id='attachments-container']//a[@rel='noopener']");
	private static By AttChmnt_RemoveBtn=By.xpath("//div[@id='attachments-container']//div//button[@class='btn btn-danger btn-sm ng-star-inserted']");
	private static By AttChmnt_Remove_HdrTtle=AttChmnt_FileUploadHdr;
	private static By AttChmnt_Remove_WarngMsg=By.xpath("//div[@class='ui-dialog-content ui-widget-content']//p");;
	private static By AttChmnt_Remove_Confirm=By.id("gf-confirm-delete-attachment");
	private static By AttChmnt_Remove_Cancel=By.xpath("//button[@class='btn btn-secondary']");
	
	
	
	//Added References List details
	private static By Reference_SpecReferenceSection=By.xpath("//div[@id='spec-references-container']");
	private static By Reference_ListofAddedReferenceToSpec=By.xpath("//div[@id='spec-references-container']//fa-icon/parent::div");
	private static By Reference_ViewBtn=By.xpath("//div[@id='spec-references-container']//a[@rel='noopener']");
	private static By Reference_RemoveBtn=By.xpath("//div[@id='spec-references-container']//div//button[@class='btn btn-danger btn-sm ng-star-inserted']");
	private static By Reference_Remove_HdrTtle=AttChmnt_Remove_HdrTtle;
	private static By Reference_Remove_WarngMsg=AttChmnt_Remove_WarngMsg;
	private static By Reference_Remove_Confirm=AttChmnt_Remove_Confirm;
	private static By Reference_Remove_Cancel=AttChmnt_Remove_Cancel;
	
	
	
	//Added PLMReferences List details
	private static By PLMReference_SpecPLMReferenceSection=By.xpath("//div[@id='plm-references-container']");
	private static By PLMReference_ListofAddedPLMReferenceToSpec=By.xpath("//div[@id='plm-references-container']//fa-icon/parent::div");
	private static By PLMReference_ViewBtn=By.xpath("//div[@id='plm-references-container']//a[@rel='noopener']");
	private static By PLMReference_RemoveBtn=By.xpath("//div[@id='plm-references-container']//div//button[@class='btn btn-danger btn-sm ng-star-inserted']");
	private static By PLMReference_Remove_HdrTtle=AttChmnt_Remove_HdrTtle;
	private static By PLMReference_Remove_WarngMsg=AttChmnt_Remove_WarngMsg;
	private static By PLMReference_Remove_Confirm=By.id("gf-confirm-delete-plm-ref");
	private static By PLMReference_Remove_Cancel=AttChmnt_Remove_Cancel;
	
	
	//Added Keyword List details
	private static By KeyWord_AddedKeywordSection=By.xpath("//div[@id='keyword-container']");
	private static By KeyWord_ListofAddedKeywordtoSpec=By.xpath("//div[@id='keyword-container']//fa-icon/parent::div");
	private static By KeyWord_RemoveBtn=By.xpath("//div[@id='keyword-container']//div//button[@class='btn btn-danger btn-sm ng-star-inserted']");
	private static By KeyWord_Remove_HdrTtle=AttChmnt_Remove_HdrTtle;
	private static By KeyWord_Remove_WarngMsg=AttChmnt_Remove_WarngMsg;
	private static By KeyWord_Remove_Confirm=By.id("confirm-remove-keyword");
	private static By KeyWord_Remove_Cancel=AttChmnt_Remove_Cancel;
	
	//Added Chapter details
	private static By ListofChapterDescription_AddedImages=By.xpath("//div[@class='ng-star-inserted']//gf-text-editor[@id='edit-chapter-text']//editor//div//img[1]");
	
	
	
	
	
	//==========================================HomePage-Locaters details End==========================================

	//==========================================HomePage-Keywords/Functions details Start==========================================
	
	//Home screen
	public static void VerifyHomeScreen()
	{
		
		Assert.assertTrue(GenericWebObjects.GetInstance().IsElementPresentAndVisible(HomePage_Title),"Web Editor Menu Page is displayed");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(HomePage_User,"Logged in User"),"Logged in User");	
	}
	
	

	public static void VerifySpecMetaInformation()
	{
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_SpecIDWithVrsn, " Verify Specification ID with version"),"Verify Specification ID with version");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_SpecStatus, " Verify Specification Status"),"Verify Specification Status");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_SpecName, " Verify Specification Name "),"Verify Specification Name");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_Author, " Verify Specification Author"),"Verify Specification Author");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_Department, " Verify Specification Department"),"Verify Specification Department");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_Owner, " Verify Specification Owner"),"Verify Specification Owner");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_WorkflowConfiguration, " Verify Specification WorkflowConfiguration"),"Verify Specification WorkflowConfiguration");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_SubClass, " Verify Specification SubClass"),"Verify Specification SubClass");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_GroupAccessLevel, " Verify Specification GroupAccessLevel"),"Verify Specification GroupAccessLevel");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_Template, " Verify Specification Template"),"Verify Specification Template");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(MetaInfo_UseCase, " Verify Specification UseCase"),"Verify Specification UseCase");
		
	
	
	}
	
	//Specification Drop down
	public static void ClickOnSpecificationDropDownValue(String DropDownValue)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Menu_SpecificationDrpDown, "Click on Menu specification drop down"),"Click on Menu specification drop down");
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(SpecificationDrpDwn_List, DropDownValue, "Click on"+ DropDownValue+" button from specification"), "Click on "+ DropDownValue+" button from specification");		
	}
	public static void ClickOnValidateButton()
	{
		ClickOnSpecificationDropDownValue("Validate");	
	}
	
	public static void ClickOnSaveToSetupFCButton()
	{	
		ClickOnSpecificationDropDownValue("Save to setup.FC");	
	}
	public static void ClickOnSubmitButton()
	{	
		ClickOnSpecificationDropDownValue("Submit");	
	}
	public static void ClickOnReloadFromSetupFCButton()
	{	
		ClickOnSpecificationDropDownValue("Reload from setup.FC");	
	}
	
	//Main View Mandate Text fields details
	
	public static void ValidateChangeReason_ErrorMsg()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(ChngRsn_ErrorTxtfld, " Mandate Change Reason field is highlighted"),"Mandate Change Reason field is highlighted");
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(ChngRsn_ErrorMsg, " Change Reason Error message is displaying"),"Change Reason Error message is displaying");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(ChngRsn_ErrorMsg, " Change Reason Error message"),"Change Reason Error message ");
	}
	
	public static void ValidateChangeDescription_ErrorMsg()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(ChngDesc_ErrorTxtfld, " Mandate Change Description field is highlighted"),"Mandate Change Description field is highlighted");
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(ChngDesc_ErrorMsg, " Change Description Error message is displaying"),"Change Description Error message is displaying");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(ChngDesc_ErrorMsg, " Change Description Error message"),"Change Description Error message ");
	}
	
	public static void ValidateScope_ErrorMsg()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(Scope_ErrorTxtfld, " Mandate Scope field is highlighted"),"Mandate Scope field is highlighted");
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(Scope_ErrorMsg, " Scope Error message is displaying"),"Scope Error message is displaying");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(Scope_ErrorMsg, " Scope Error message"),"Scope Error message ");
	}
	

	public static void ValidatePurpose_ErrorMsg()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(Purpose_ErrorTxtfld, " Mandate Purpose field is highlighted"),"Mandate Purpose field is highlighted");
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(Purpose_ErrorMsg, " Purpose Error message is  displaying"),"Purpose Error message is displaying");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(Purpose_ErrorMsg, " Scope Error message"),"Purpose Error message ");
	}
	
	
	public static void ValidatePurpose()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(Purpose_ErrorTxtfld, " Mandate Purpose field is not highlighted"),"Mandate Purpose field is not highlighted");
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(Purpose_ErrorMsg, " Purpose Error message is not displaying"),"Purpose Error message is not displaying");
		}
	public static void ValidateChangeReason()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(ChngRsn_ErrorTxtfld, " Mandate Change Reason field is not highlighted"),"Mandate Change Reason field is not highlighted");
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(ChngRsn_ErrorMsg, " Change Reason Error message is not displaying"),"Change Reason Error message is not displaying");
		
	}
	
	public static void ValidateChangeDescription()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(ChngDesc_ErrorTxtfld, " Mandate Change Description field is not highlighted"),"Mandate Change Description field is not highlighted");
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(ChngDesc_ErrorMsg, " Change Description Error message is not displaying"),"Change Description Error message is not displaying");
		
	}
	
	public static void ValidateScope()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(Scope_ErrorTxtfld, " Mandate Scope field is not highlighted"),"Mandate Scope field is not highlighted");
		Assert.assertTrue(GenericWebObjects.GetInstance().NotExist(Scope_ErrorMsg, " Scope Error message is not displaying"),"Scope Error message is not displaying");
	}
	
	
	public static void EnterPCRBValue(String PCRB)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(PCRB_Txtfld, PCRB," Enter the PCRB Value ")," Enter the PCRB Value ");

	}
	
	public static void EnterChangeDescriptionValue(String ChangeDescription)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(ChngDesc_Txtfld, ChangeDescription," Enter the ChangeDescription Value ")," Enter the ChangeDescription Value ");

	}
	public static void EnterChangeReasonValue(String ChangeReason)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(ChngRsn_Txtfld, ChangeReason," Enter the ChangeReason Value ")," Enter the ChangeReason Value ");

	}
	public static void EnterPurposeValue(String Purpose)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Purpose_Txtfld," Enter the Purpose Value ")," Enter the Purpose Value ");
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(Purpose_Txtfld, Purpose," Enter the Purpose Value ")," Enter the Purpose Value ");

	}
	public static void EnterScopeValue(String Scope)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Scope_Txtfld," Enter the Scope Value ")," Enter the Scope Value ");
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(Scope_Txtfld, Scope," Enter the Scope Value ")," Enter the Scope Value ");

	}
	public static void EnterDefinitionValue(String Definitions)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Definitions_Txtfld," Enter the Definitions Value ")," Enter the Definitions Value ");
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(Definitions_Txtfld, Definitions," Enter the Definitions Value ")," Enter the Definitions Value ");	
	}
	
	
	//Add Drop down Details
	
	public static void ClickOnAddDropDownValue(String DropDownValue)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Menu_AddDrpDown, "Click on Menu ADD drop down"),"Click on Menu Add drop down");
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementText(AddDrpDwn_List, DropDownValue, "Click on ["+ DropDownValue+" ]Option from Add dropdown"), "Click on ["+ DropDownValue+" ]Option from Add dropdown");		
	}
	public static void ClickOnAttachmentButton()
	{
		ClickOnAddDropDownValue("Attachment");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(AttChmnt_FileUploadHdr,"Upload Attachment Header "),"Upload attachment header");
	}
	
	public static void ClickOnReferenceButton()
	{	
		ClickOnAddDropDownValue("Reference");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(Reference_AddRefer_HdrTitle,"Add Reference Header Title"),"Add Reference Header Title");
	}
	public static void ClickOnPLMReferenceButton()
	{	
		ClickOnAddDropDownValue("PLM Reference");	
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(PLMReference_AddRefer_HdrTitle,"Add PLM  Reference Header Title"),"Add PLM Reference Header Title");
	}
	public static void ClickOnKeyWordButton()
	{	
		ClickOnAddDropDownValue("Keyword");
		Assert.assertTrue(GenericWebObjects.GetInstance().MediumWait());
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(KeyWord_AddKeyword_HdrTitle,"Add Keyword Header Title"),"Add Keyword Header Title");
	}
	
	//Add attachment details
	public static void UploadFile(String FileName)
	{
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(AttChmnt_FileUploadHdr, "Upload File Header Title"),"Verified Upload File Header Title");
	//	GenericWebObjects.GetInstance().CreateWebelement(AttChmnt_ChooseFileBtn).findElement(AttChmnt_UploadFile).sendKeys(FileName);
		Assert.assertTrue(GenericWebObjects.GetInstance().UploadFile(FileName, "Upload the File",AttChmnt_ChooseFileBtn,AttChmnt_UploadFile),"Upload the File");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(AttChmnt_UploadFileBtn, "Click UploadFile Button"),"Clicked on Upload Button");
	
	}
	
	//Add Reference Details
	public static void AddReference_SearchSpecID(String SpceID)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(Reference_AddRefer_SpecID_txtfld, SpceID," Enter the Spec ID in Spec text field ")," Enter the Spec ID in Spec text field ");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Reference_AddRefer_Search_Btn, "Click on Search button in Add reference"),"Click on Search button in Add reference");
	}
	
	/**
	 * @param Rowdetails Add Cell details in(ClmnHeaderName=Value)
	 */
	public static void AddReference_VerifyFirstRecord(String... Rowdetails) {
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(Reference_AddRefer_ResultGrid, "Verified Result grid is present"),"Verified Result grid is present");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Reference_AddRefer_Result_FrstRow,"Click on first record of reference result"), "Click on first record of reference result");
		for (String RsltClmn : Rowdetails) {
			By FirstRowDetails = null;
			int temp = 0;
			String[] ClmnHdrAndValue = RsltClmn.split("=", 2);
			switch (ClmnHdrAndValue[0].toUpperCase().trim()) {
			case "ID":
				temp = 1;
				break;
			case "TITLE":
				temp = 2;
				break;
			case "DEPARTMENT":
				temp = 3;
				break;
			case "AUTHOR":
				temp = 4;
				break;
				default:
					temp = 1;
					break;	
			}
			FirstRowDetails = By.xpath(
					Reference_AddRefer_Result_FrstRow.toString().replaceAll("By.xpath:", "") + "//td[" + temp + "]");
			Assert.assertTrue(
					GenericWebObjects.GetInstance().ValidateText(FirstRowDetails, ClmnHdrAndValue[1],
							"Validate [" + ClmnHdrAndValue[0] + " ]value in Add reference search result"),
					"Validate [" + ClmnHdrAndValue[0] + " ]value in Add reference search result");
		}

	}

	
	public static void AddReference_ClickonOkButton()
	{

		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Reference_AddRefer_Ok_Btn, "Click on OK button to Add the reference into the spec"),"Click on OK button to Add the reference into the spec");
	}
	
	//Add PLM Reference Details
	public static void AddPLMReference(String SpceID)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(PLMReference_AddRefer_SpecID_txtfld, SpceID," Enter the Spec ID in Spec text field ")," Enter the Spec ID in Spec text field ");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(PLMReference_AddRefer_Ok_Btn, "Click on OK button to Add the PLM reference into the spec"),"Click on OK button to Add the PLM reference into the spec");
	}
	
	//Add KeyWords details
	public static void AddKeyWords(String EquipmentIDs)
	{
		for(String LstofEquipmnt:EquipmentIDs.split(","))
		{
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(KeyWord_AddKeyword_EqupmntTxtFild, LstofEquipmnt," Enter the Equipment ID in Spec text field ")," Enter the Equipment ID in the text field ");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(KeyWord_AddKeyword_SelectFltrdEquipmntID, "Select the first EquipmentID from filter"),"Select the first EquipmentID from filter");
		}
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(KeyWord_AddKeyword_Ok_Btn, "Click on OK button to Add the Keyword into the spec"),"Click on OK button to Add Keyword into the spec");
	}
	
	
	
	
	//*******************Added Spec Details***************************
	//Added Attachment details
	public static void VerifyAddedSpecAttachmentName(String AttachmentName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyTextfromElements(AttChmnt_ListofAddedAttachmentsToSpec, AttachmentName,"Verified Added Attachments"),"Verified Added Attachments");
	}
	public static void VerifyAttachmentViewOption(String AttachmentName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(AttChmnt_ViewBtn,AttChmnt_ListofAddedAttachmentsToSpec, AttachmentName,"Click on view button for Attachment"),"Click on View button for the Attachment");
	}
	
	public static void AttachmentRemoveAndConfirm(String AttachmentName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(AttChmnt_RemoveBtn,AttChmnt_ListofAddedAttachmentsToSpec, AttachmentName,"Click on Remove button for Attachment"),"Click on Remove button for the Attachment");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(AttChmnt_Remove_HdrTtle, "Remove AttChmnt Header Title is "),"Remove Attachment Header Title is ");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(AttChmnt_Remove_WarngMsg, "Remove AttChmnt Warning message is "),"Remove Attachment Warning message ");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(AttChmnt_Remove_Confirm, "Click on Delete/Remove button from Remove Attachment window"),"Click on Delete/Remove button Remove Attachment window");
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyNoTextfromElements(AttChmnt_ListofAddedAttachmentsToSpec, AttachmentName,"successfully removed the AttChmnt"),"successfully removed the AttChmnt");
	}
	public static void AttachmentRemoveAndCancel(String AttachmentName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(AttChmnt_RemoveBtn,AttChmnt_ListofAddedAttachmentsToSpec, AttachmentName,"Click on Remove button for Attachment"),"Click on Remove button for the Attachment");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(AttChmnt_Remove_Cancel, "Click on Cancel button from Remove Attachment window"),"Click on Delete/Remove button Remove Attachment window");
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyTextfromElements(AttChmnt_ListofAddedAttachmentsToSpec, AttachmentName,"Verify Attachment is not removed"),"Attachment is not removed");
	}
	
	
	
	
	//Added Reference details
	public static void VerifyAddedSpecReferenceName(String ReferenceName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyTextfromElements(Reference_ListofAddedReferenceToSpec, ReferenceName,"Verified Added Reference"),"Verified Added Reference");
	}
	public static void VerifyReferenceViewOption(String ReferenceName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(Reference_ViewBtn,Reference_ListofAddedReferenceToSpec, ReferenceName,"Click on view button for reference"),"Click on View button for the reference");
	}
	
	public static void ReferenceRemoveAndConfirm(String ReferenceName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(Reference_RemoveBtn,Reference_ListofAddedReferenceToSpec, ReferenceName,"Click on Remove button for reference"),"Click on Remove button for the reference");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(Reference_Remove_HdrTtle, "Remove Reference Header Title is "),"Remove Reference Header Title is ");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(Reference_Remove_WarngMsg, "Remove Reference Warning message is "),"Remove Reference Warning message ");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Reference_Remove_Confirm, "Click on Delete/Remove button from Remove Reference window"),"Click on Delete/Remove button Remove Reference window");
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyNoTextfromElements(Reference_ListofAddedReferenceToSpec, ReferenceName,"successfully removed the reference"),"successfully removed the reference");
	}
	public static void ReferenceRemoveAndCancel(String ReferenceName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(Reference_RemoveBtn,Reference_ListofAddedReferenceToSpec, ReferenceName,"Click on Remove button for reference"),"Click on Remove button for the reference");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(Reference_Remove_Cancel, "Click on Cancel button from Remove Reference window"),"Click on Delete/Remove button Remove Reference window");
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyTextfromElements(Reference_ListofAddedReferenceToSpec, ReferenceName,"Verified Added Reference"),"Verified Added Reference");
	}
	
	//Added PLM Reference details
	public static void VerifyAddedSpecPLMReferenceName(String PLMReferenceName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyTextfromElements(PLMReference_ListofAddedPLMReferenceToSpec, PLMReferenceName,"Verified Added PLM Reference"),"Verified Added PLM Reference");
	}
	public static void VerifyPLMReferenceViewOption(String PLMReferenceName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(PLMReference_ViewBtn,PLMReference_ListofAddedPLMReferenceToSpec, PLMReferenceName,"Click on view button for PLMReference"),"Click on View button for the PLMReference");
	}
	
	public static void PLMReferenceRemoveAndConfirm(String PLMReferenceName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(PLMReference_RemoveBtn,PLMReference_ListofAddedPLMReferenceToSpec, PLMReferenceName,"Click on Remove button for PLMReference"),"Click on Remove button for the PLMReference");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(PLMReference_Remove_HdrTtle, "Remove PLMReference Header Title is "),"Remove PLMReference Header Title is ");
		Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(PLMReference_Remove_WarngMsg, "Remove PLMReference Warning message is "),"Remove PLMReference Warning message ");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(PLMReference_Remove_Confirm, "Click on Delete/Remove button from Remove PLMReference window"),"Click on Delete/Remove button Remove PLMReference window");
		Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyNoTextfromElements(PLMReference_ListofAddedPLMReferenceToSpec, PLMReferenceName,"successfully removed the PLMReference"),"successfully removed the PLMReference");
	}
	public static void PLMReferenceRemoveAndCancel(String PLMReferenceName)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(PLMReference_RemoveBtn,PLMReference_ListofAddedPLMReferenceToSpec, PLMReferenceName,"Click on Remove button for PLMReference"),"Click on Remove button for the PLMReference");
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(PLMReference_Remove_Cancel, "Click on Cancel button from Remove PLMReference window"),"Click on Delete/Remove button Remove PLMReference window");
		Assert.assertTrue(GenericWebObjects.GetInstance().VerifyTextfromElements(PLMReference_ListofAddedPLMReferenceToSpec, PLMReferenceName,"PLMReference is not removed and displaying"),"PLMReference is not removed and displaying");
	}
	
	
	
	
	//Added KeyWord details
		public static void VerifyAddedSpecKeywordName(String KeywordName)
		{
			for(String Keyword:KeywordName.split(","))
			{
			Assert.assertTrue(GenericWebObjects.GetInstance().VerifyTextfromElements(KeyWord_ListofAddedKeywordtoSpec, Keyword,"Verified Added Keyword"),"Verified Added Keyword");
			}
		}
		public static void KeywordRemoveAndConfirm(String KeywordName)
		{
			for(String Keyword:KeywordName.split(","))
			{	
			Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(KeyWord_RemoveBtn,KeyWord_ListofAddedKeywordtoSpec, Keyword,"Click on Remove button for Keyword"),"Click on Remove button for the Keyword");
			Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(KeyWord_AddKeyword_HdrTitle, "Remove Keyword Header Title is "),"Remove Keyword Header Title is ");
			Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(KeyWord_Remove_WarngMsg, "Remove Keyword Warning message is "),"Remove Keyword Warning message ");
			Assert.assertTrue(GenericWebObjects.GetInstance().Click(KeyWord_Remove_Confirm, "Click on Delete/Remove button from Remove Keyword window"),"Click on Delete/Remove button Remove Keyword window");
			Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
			Assert.assertTrue(GenericWebObjects.GetInstance().VerifyNoTextfromElements(KeyWord_ListofAddedKeywordtoSpec, Keyword,"successfully removed the Keyword"),"successfully removed the Keyword");
			}
			}
		public static void KeywordRemoveAndCancel(String KeywordName)
		{
			for(String Keyword:KeywordName.split(","))
			{
			Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor(KeyWord_RemoveBtn,KeyWord_ListofAddedKeywordtoSpec, Keyword,"Click on Remove button for Keyword"),"Click on Remove button for the Keyword");
			Assert.assertTrue(GenericWebObjects.GetInstance().Click(KeyWord_Remove_Cancel, "Click on Cancel button from Remove Keyword window"),"Click on Cancel button Remove Keyword window");
			Assert.assertTrue(GenericWebObjects.GetInstance().VerifyTextfromElements(KeyWord_ListofAddedKeywordtoSpec, Keyword,"Keyword is not removed and displaying"),"Keyword is not removed and displaying");
			}
		}
		
		// Add New chapters
		public static void AddNewChapter()
		{
			//Assert.assertTrue(GenericWebObjects.GetInstance().ScrollToBottom("Scroll down to bottom of the screen"),"Scroll down to bottom of the screen");
			if(GenericWebObjects.GetInstance().IsElementPresentAndVisible(Chapter_NewChapter_Btn))
			{	
				Assert.assertTrue(GenericWebObjects.GetInstance().Click(Chapter_NewChapter_Btn, "Click on Add Chapter button to add chapter 5"),"Click on Added Chapter button to add chapter 5");
			
			}
			else
				Assert.assertTrue(GenericWebObjects.GetInstance().Exist(ListofChapterTitle_Txtfld, "Chapter is already Exist"),"Chapter is already Exist");
		}
		
		public static void ValidateInvalidChapterTitle()
		{
			Assert.assertTrue(GenericWebObjects.GetInstance().ScrollToBottom("Scroll down to bottom of the screen"),"Scroll down to bottom of the screen");
			Assert.assertTrue(GenericWebObjects.GetInstance().Exist(Chapter_InvalidChapter5Title, "Chapter 5 Title is highlighted with Red"),"Chapter 5 Title is highlighted with Red");
					
		}
		
		public static void ValidatValidChapterTitle()
		{	
			Assert.assertTrue(GenericWebObjects.GetInstance().ScrollToBottom("Scroll down to bottom of the screen"),"Scroll down to bottom of the screen");
			Assert.assertTrue(GenericWebObjects.GetInstance().Exist(Chapter_validChapter5Title, "Chapter 5 Title is highlighted with Green"),"Chapter 5 Title is highlighted with Green");					
		}
			
		
		
		public static void EnterChapterTitle(String ChapterNmbr,String ChapterTitle)
		{
			if(!ChapterNmbr.endsWith("."))
				ChapterNmbr=ChapterNmbr+".";
			Assert.assertTrue(GenericWebObjects.GetInstance().SendKeysForElementTextFromAncestor( ListofChapterTitle_Txtfld,ListofChaptersNmbr,ChapterNmbr,ChapterTitle,"Enter Chapter Title"),"Enter Chapter Title");
			
		}
		public static void EnterChapterDesc(String ChapterNmbr,String ChpaterDesc)
		{
			if(!ChapterNmbr.endsWith("."))
			ChapterNmbr=ChapterNmbr+".";
			Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor( ListofChapterDescription_Txtfld,ListofChaptersNmbr,ChapterNmbr,"Cick on chapter"+ChapterNmbr+" Description text field"),"Cick on chapter"+ChapterNmbr+" Description text field");
			Assert.assertTrue(GenericWebObjects.GetInstance().SendKeysForElementTextFromAncestor( ListofChapterDescription_Txtfld,ListofChaptersNmbr,ChapterNmbr,ChpaterDesc,"Enter Chapter description"),"Enter Chapter description");
			Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		}
		public static void DeleteChapter(String ChapterNmbr)
		{
			if(!ChapterNmbr.endsWith("."))
			ChapterNmbr=ChapterNmbr+".";
		//	Assert.assertTrue(GenericWebObjects.GetInstance().ScrollToBottom("Scroll down to bottom of the screen"),"Scroll down to bottom of the screen");
			Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor( ListofChpaterMoreOption,ListofChaptersNmbr,ChapterNmbr,"Cick on chapter"+ChapterNmbr+" more option"),"Cick on chapter"+ChapterNmbr+" more option");
			Assert.assertTrue(GenericWebObjects.GetInstance().Click(Chapter_MoreOption_DeleteChapter, "Successfully Deleted the Chapter"),"Successfully Deleted the Chapter");			
		}
		public static void AddChapterAbove(String ChapterNmbr)
		{
			if(!ChapterNmbr.endsWith("."))
			ChapterNmbr=ChapterNmbr+".";
			Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor( ListofChpaterMoreOption,ListofChaptersNmbr,ChapterNmbr,"Cick on more option from chapter "+ChapterNmbr),"Cick on more option from chapter "+ChapterNmbr);
			Assert.assertTrue(GenericWebObjects.GetInstance().Click(Chapter_MoreOption_AddChapterAbove,"Click on Add chapter Above"),"Click on Add chapter Above");			
			Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		}
		public static void AddChapterBelow(String ChapterNmbr)
		{
			if(!ChapterNmbr.endsWith("."))
			ChapterNmbr=ChapterNmbr+".";
			Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor( ListofChpaterMoreOption,ListofChaptersNmbr,ChapterNmbr,"Cick on chapter"+ChapterNmbr+" more option"),"Cick on chapter"+ChapterNmbr+" more option");
			Assert.assertTrue(GenericWebObjects.GetInstance().Click(Chapter_MoreOption_AddChapterBelow,"Click on Add chapter Below"),"Click on Add chapter Below");			
			Assert.assertTrue(GenericWebObjects.GetInstance().MinWait());
		}
		public static void ClickAddChapterAttachment(String ChapterNmbr)
		{
			if(!ChapterNmbr.endsWith("."))
				ChapterNmbr=ChapterNmbr+".";	
				Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor( ListofChpaterMoreOption,ListofChaptersNmbr,ChapterNmbr,"Cick on chapter"+ChapterNmbr+" more option"),"Cick on chapter"+ChapterNmbr+" more option");
				Assert.assertTrue(GenericWebObjects.GetInstance().Click(Chapter_MoreOption_AddAttachment,"Click on Add Attachment option"),"Click on Add Attachment option");			
				Assert.assertNotNull(GenericWebObjects.GetInstance().GetText(Chapter_AddAttchmentHdr,"Upload Attachment Header "),"Upload attachment header");
		}
		
		public static void AddNewAttachmentToChapter(String AttachmentName)
		{
			Assert.assertTrue(GenericWebObjects.GetInstance().Click(Chapter_AddAttchmnt_UploadNewBtn, "Click Upload New tab Button"),"Click Upload New tab Button");
			UploadFile(AttachmentName);
		}
		
		public static void OpenChapterEditor(String ChapterNmbr)
		{
			if(!ChapterNmbr.endsWith("."))
				ChapterNmbr=ChapterNmbr+".";
				Assert.assertTrue(GenericWebObjects.GetInstance().ClickElementTextByAncestor( ListofChpaterMoreOption,ListofChaptersNmbr,ChapterNmbr,"Cick on chapter"+ChapterNmbr+" more option"),"Cick on chapter"+ChapterNmbr+" more option");
				Assert.assertTrue(GenericWebObjects.GetInstance().Click(Chapter_MoreOption_OpenEditor,"Click on Open Editor option"),"Click on Open Editor option");
				ChapterEditorPage.ChapterEditorScreen();
		}
		
		public static void VerifyImageInChapterDescription()
		{
			Assert.assertTrue(GenericWebObjects.GetInstance().Exist(ListofChapterDescription_AddedImages, "Added Image is displaying in Description field"));
		}
		
		public static void GetChapterID(String ChapterNmbr)
		{
			if(!ChapterNmbr.endsWith("."))
				ChapterNmbr=ChapterNmbr+".";
			By by=GenericWebObjects.GetInstance().UpdateLocatorMechanism(Chapter_ChapterTitle_Byinput, "$ChartNumber$", ChapterNmbr);
			String ChartID=GenericWebObjects.GetInstance().GetAttributeValue(by, "id", "Get chapter ID from the id");
			ChartID = ChartID.replaceAll("[^\\d]", "");
			
			
			
		}
	//==========================================HomePage-Keywords/Functions details End==========================================
}
