package com.gf.WEBase;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.gf.common.GenericWebObjects;

public class PreviewPage {
	//==========================================Preview-Locaters details Start==========================================
	
		private static final By Metainfo_SpecID=By.xpath("//table[@id='MetaInformation']//tbody//tr[1]//th[1]");
		private static final By Metainfo_Status=By.xpath("//table[@id='MetaInformation']//tbody//tr[1]//th[2]");
		private static final By Metainfo_SpecName=By.xpath("//table[@id='MetaInformation']//tbody//tr[2]//th[1]");
		private static final By Metainfo_SpecDetails=By.xpath("//table[@id='MetaInformation']//tbody//tr//td//span[2]");
		
	//==========================================Preview-Locaters details End==========================================	

	//==========================================Keyword-Function Start==========================================
		
		public static void VerifyMetaInfo(String SpecID,String Status,String SpecName,String MetaSpecDetails )
		{
			Assert.assertTrue(GenericWebObjects.GetInstance().ValidatePartialText(Metainfo_SpecID, SpecID, "Verified the Spec ID"),"Verified the Spec ID");
			Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(Metainfo_Status, Status, "Verified the Spec status"),"Verified the Spec status");
			Assert.assertTrue(GenericWebObjects.GetInstance().ValidateText(Metainfo_SpecName, SpecName, "Verified the Spec Name"),"Verified the Spec name");
			Assert.assertTrue(GenericWebObjects.GetInstance().VerifyListofTextFromElements(Metainfo_SpecDetails, MetaSpecDetails, "Verified the Meta Spec details "),"Verified the Meta spec details");
		}
		
}
