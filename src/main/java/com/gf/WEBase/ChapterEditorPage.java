package com.gf.WEBase;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.gf.common.GenericWebObjects;

public class ChapterEditorPage {
	
	//==========================================Chapter Editor-Locaters details Start==========================================
	
	private static By ChapterEditor_BackBtn=By.id("cancel-save");
	private static By ChapterEditor_Textfld=By.xpath("//body[@id='tinymce']");
	private static By ChapterEditor_AddImageBtn=By.xpath("//button[@title='Insert image']");
	private static By ChapterEditor_MoreOption=By.xpath("//button[@title='More...']");
	private static By ChapterEditr_EditorScreen=By.className("tox-toolbar-overlord");
	
	
	
	//==========================================Chapter Editor-Locaters details End==========================================
	
	
	
	//==========================================Chapter Editor-Keywords/Functions details Start==========================================
	public static void ChapterEditorScreen()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Exist(ChapterEditr_EditorScreen," Chapter editor screen is displaying")," Chapter editor screen is displaying");
	}
	public static void ClickBackFromEditor()
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(ChapterEditor_BackBtn, " Clck on Back button from Chapter Editor screen"),"Clck on Back button from Chapter Editor screen");
	}
	
	public static void AddChapterRichtext(String Text)
	{
		Assert.assertTrue(GenericWebObjects.GetInstance().SwitchToFrame(0),"Switing to frame");
		Assert.assertTrue(GenericWebObjects.GetInstance().SendKeys(ChapterEditor_Textfld, Text," Enter Chapter text in editor"),"Enter Chapter text in editor");
		Assert.assertTrue(GenericWebObjects.GetInstance().SwitchToDefaultWebPage(),"Switing to default page");
	}
	public static void AddImageRichText(String ImageName)
	{
		if(GenericWebObjects.GetInstance().IsElementPresentAndVisible(ChapterEditor_MoreOption))
			Assert.assertTrue(GenericWebObjects.GetInstance().Click(ChapterEditor_MoreOption,"Click on more Icon"));
		Assert.assertTrue(GenericWebObjects.GetInstance().Click(ChapterEditor_AddImageBtn,"Click on Image icon"));
		Assert.assertTrue(GenericWebObjects.GetInstance().uploadWindowFile(ImageName));
		
	}
	//==========================================Chapter Editor-Keywords/Functions details End==========================================

}
