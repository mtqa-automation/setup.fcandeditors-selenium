package com.gf.common;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface SFCReportCategory {
	String Author() default "Admin";
	String Suite() default "";
	String TestTitle() default "";
	String Category() default "";
	

}
