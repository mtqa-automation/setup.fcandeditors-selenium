package com.gf.common;

import java.util.Map;

import org.openqa.selenium.WebDriver;
/**
 * @author smallika
 *
 */
public class GlobalVaraibles {
	public static WebDriver driver1;
	public static WebDriver driver2;
	public static int Default_ExtMinWait=1000;
	public static int Default_MinWait=2000;
	public static int Default_MedWait=5000;
	public static int Default_MaxWait=10000;
	public static int Default_ExtMaxWait=15000;
	
	public static int AutoG_Number1;
	public static long AutoG_LongNumber1;
	public static double AutoG_DoubleNumber1;
	
	public static String RandomAutoSpec= "QA_Automation_";
	public static String RandomAutoConfig="AutoTestConfig_";
	public static String RandomValue1 = "AutoTestValue1_";
    public static String RandomValue2 = "AutoTestValue2_";
    public static String RandomValue3 = "09";
    public static String RandomValue4 = "07";
    public static String SuiteName="";
	public static String TestScriptID;
	public static String TestTitle;
	public static Map<String, String> Input;

	public static boolean TestRailEnable=false;
	public static String TestResult="";
	
	//Config File Common Varaibles
	public static final String  ExecutionSection=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "ExecutionSection");
	public static final String  ExtMinimumWait=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "ExtMinimumWait");
	public static final String  MinimumWait=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "MinimumWait");
	public static final String  MediumWait=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "MediumWait");
	public static final String  MaximumWait=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "MaximumWait");
	public static final String  LongWait=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "LongWait");
	public static final String  TestDataFilePath=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "TestDataFilePath");
	public static final String  TestDataFileName=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "TestDataFileName");
	public static final String  ScreenShotFolderPath=IniFile.GetInstance().ReadIniFile("START_GLOBAL", "ScreenShotFolderPath");
	
}
