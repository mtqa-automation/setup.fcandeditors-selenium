package com.gf.common;

import org.apache.log4j.Logger;

import testrail.api.functions.TestRail;

public class TestRailConfiguration {

	private static Logger logger = Logger.getLogger(TestRailConfiguration.class);
	 TestRail testRail;
	private static TestRailConfiguration config;
	private static String ProjectName=IniFile.GetInstance().ReadIniFile("Start_TestRail", "ProjectName");
	private static String MileStoneName=IniFile.GetInstance().ReadIniFile("Start_TestRail", "MileStoneName");
	private static String TestPlanName=IniFile.GetInstance().ReadIniFile("Start_TestRail", "TestPlanName");
	private static String TestRunName=IniFile.GetInstance().ReadIniFile("Start_TestRail", "TestRunName");
	

	private TestRailConfiguration() {
		if (GlobalVaraibles.TestRailEnable) {
				String URL = IniFile.GetInstance().ReadIniFile("Start_TestRail", "TestRail_URL");
				String UserName = IniFile.GetInstance().ReadIniFile("Start_TestRail", "TestRail_UserName");
				String Password = IniFile.GetInstance().ReadIniFile("Start_TestRail", "TestRail_Password");
				testRail = TestRail.GetInstance(URL, UserName, Password);
			//	 testRail=TestRail.GetInstance("https://mvalentina3.testrail.io","mvalentina3.svis@emvil.com","Siddu,1994");
		}
	}

	public static TestRailConfiguration GetInstance()
	{
		if(config==null)
			config=new TestRailConfiguration();
		return config;
	}
	
	/**
	 * @param ProjectName
	 * @param MileStoneName
	 * @param MileStoneDescription
	 * @return boolean
	 */
	public boolean CreateMileStone()
	{
		try {
		if(GlobalVaraibles.TestRailEnable)
		{
			testRail.AddMileStone(ProjectName, MileStoneName, "Added by Automation Scripts");
			return true;
		}
		else
		{
			logger.info("Test Rail Enabled: "+GlobalVaraibles.TestRailEnable);
			return true;
		}
		}
		catch(Exception e)
		{
			logger.error("Error while Creating MileStone : "+e);
		}
		return false;
	}

	/**
	 * @param ProjectName
	 * @param MilestoneName
	 * @param TestPlanName
	 * @param PlanDescription
	 * @return boolean
	 */
	public boolean CreateTestPlan()
	{
		try {
		if(GlobalVaraibles.TestRailEnable)
		{
			testRail.AddTestPlan(ProjectName, MileStoneName, TestPlanName, "Added by Automation Script");
			return true;
		}
		else
		{
			logger.info("Test Rail Enabled: "+GlobalVaraibles.TestRailEnable);
			return true;
		}
		}
		catch(Exception e)
		{
			logger.error("Error while Creating Test plan : "+e);
		}
		return false;
	}

	/**
	 * @param ProjectName
	 * @param TestPlanName
	 * @param TestRunName
	 * @param TestCaseIDs
	 * @return boolean
	 */
	public boolean CreateTestRun(String TestCaseIDs)
	{
		try {
		if(GlobalVaraibles.TestRailEnable)
		{
			testRail.AddNewPlanEntry(ProjectName, TestPlanName, TestRunName, TestCaseIDs);
			return true;
		}
		else
		{
			logger.info("Test Rail Enabled: "+GlobalVaraibles.TestRailEnable);
			return true;
		}
		}
		catch(Exception e)
		{
			logger.error("Error while Creating Test Run : "+e);
		}
		return false;
	}

	/**
	 * @param ProjectName
	 * @param PlanName
	 * @param RunName
	 * @param TestCases
	 * @param TestStatus
	 * @param Comment
	 * @return boolean
	 */
	public boolean AddTestResult(String TestCases, String TestStatus, String Comment)
	{
		try {
			if(GlobalVaraibles.TestRailEnable)
			{
				testRail.AddTestResultWithCase(ProjectName, TestPlanName, TestRunName, TestCases, TestStatus, Comment);
				return true;
			}
			else
			{
				logger.info("Test Rail Enabled: "+GlobalVaraibles.TestRailEnable);
				return true;
			}
			}
			catch(Exception e)
			{
				logger.error("Error while Adding Test Result : "+e);
			}
			return false;
	}
	
}
