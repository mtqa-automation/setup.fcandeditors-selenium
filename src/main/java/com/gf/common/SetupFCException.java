package com.gf.common;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class SetupFCException extends RuntimeException {
	private static Logger logger = Logger.getLogger(GenericWebObjects.class.getName());
	public SetupFCException(String message)
	{
		super(message);
		logger.error(message);
		SetupFCTestReport.GetInstance().error(message);
	}

	public SetupFCException(String message,Throwable e)
	{
		super(message,e);
		logger.error(message+ ": "+e);
		SetupFCTestReport.GetInstance().error(message+": "+e.getMessage());
	}

}
