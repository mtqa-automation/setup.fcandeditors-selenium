package com.gf.common;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
import io.github.bonigarcia.wdm.WebDriverManager;


/**
 * @author smallika
 *
 */
public class GenericWebObjects implements webdriverfunctions {
	private static WebDriver driver;
	private static String ParentWindow;
	private static GenericWebObjects gwo = null;
	private static Logger logger = Logger.getLogger(GenericWebObjects.class);

	/**
	 * 
	 * @author smallika
	 * @param args
	 */

	public static GenericWebObjects GetInstance() {
		if (gwo == null)
			gwo = new GenericWebObjects();
		return gwo;
	}

	public static void main(String[] args) {

	}

	public void AfterSuite() {

		SetupFCTestReport.Endtest();
		SetupFCTestReport.RemoveInstance();
		CloseBrowser();
	}

	/**
	 * Create webdriver for given brower
	 */
	@Override
	public WebDriver Driver(String BrowserName) {

		try

		{

			logger.debug("  Entered into Driver method");
			Browser browser;
			browser = Browser.valueOf(BrowserName.replace(" ", "").toUpperCase().trim());
			switch (browser) {
			case CHROME:
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				break;
			case FIREFOX:
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
				break;

			case IE:
				WebDriverManager.iedriver().setup();
				driver = new InternetExplorerDriver();
				break;
			case HEADLESS:
			case HEADLESSCHROME:
				WebDriverManager.chromedriver().setup();
				ChromeOptions chromeOptions = new ChromeOptions();
	            chromeOptions.addArguments("--no-sandbox");
	            chromeOptions.addArguments("--headless");
	            chromeOptions.addArguments("disable-gpu");

				driver = new ChromeDriver(chromeOptions);
				break;
				
			default:
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				break;
			}
			SetupFCTestReport.GetInstance().pass(BrowserName + " Browser is Launched successfully!");
			logger.info(BrowserName + " Browser is Launched successfully!");
			driver.manage().window().maximize();
			GetParentWindow();
			logger.debug("  Left from Driver method");
		} catch (Exception e) {
			throw new SetupFCException("error occured while getting webdriver: " + e);
		}
		return driver;
	}

	/**
	 *
	 */
	@Override
	public String GetParentWindow() {

		try {
			logger.debug("  Entered into GetParentWindow method");
			ParentWindow = driver.getWindowHandle();
			logger.info("Parent Window ID : "+ ParentWindow);
			logger.debug("  Left from GetParentWindow method");
			return ParentWindow;
		} catch (Exception e) {
			logger.error("error occured while getting Parent Window: " + e);
			AddScreenshortInReport("error occured while getting Parent Window: " + e);
			return " Error Message:" + e.getMessage();
		}
	}

	/**
	 * switching browser to parent window
	 */
	@Override
	public boolean SwitchtoParentWindow() {

		try {
			logger.debug("  Entered into SwitchParentWindow method");
			driver.switchTo().window(ParentWindow);
			SetupFCTestReport.GetInstance().pass("Switched to parent window: [" + ParentWindow+ "] and Window Title:[ "+driver.getTitle()+"]");
			logger.info("Switched to parent window: [" + ParentWindow+ "] and Window Title:[ "+driver.getTitle()+"]");
			logger.debug("  Left from SwitchParentWindow method");
			return true;
		} catch (Exception e) {
			logger.error("error occured while switching to Parent Window: " + e);
			AddScreenshortInReport("error occured while switching to Parent Window: " + e);
		}
		return false;
	}

	/**
	 * Switching to new window
	 */
	@Override
	public boolean SwitchtoNewWindow() {

		try {
			logger.debug("  Entered into SwitchtoNewWindow method");
			for (String windows : GetActiveWindows()) {
				driver.switchTo().window(windows);
				if(!ParentWindow.equalsIgnoreCase(windows))
				{	
				SetupFCTestReport.GetInstance().pass("Switched to child window [" +driver.getTitle()+"] and window ID "+ windows);
				logger.info("Switched to child window [" +driver.getTitle()+"] and window ID >>"+ windows);
				}
			}
			logger.debug("  Left from SwitchtoNewWindow method");
			return true;
		} catch (Exception e) {
			logger.error("error occured while switching to new Window: " + e);
			AddScreenshortInReport("error occured while switching to new Window: " + e);
		}
		return false;
	}

	/**
	 * List of active windows
	 * 
	 * @return String[]
	 */
	private String[] GetActiveWindows() {
		try {
			logger.debug(" Entered into GetActiveWindows method");
			int temp = 0;
			Set<String> windows = driver.getWindowHandles();
			GetParentWindow();
			String[] Wndws = new String[windows.size()];
			Iterator<String> Window_Iterator = windows.iterator();
			while (Window_Iterator.hasNext()) {
				Wndws[temp] = Window_Iterator.next();
				temp++;
			}
			logger.debug("  Left from GetActiveWindows method");
			return Wndws;
		} catch (Exception e) {
			logger.error("error occured while getting list of active Windows: " + e);
			AddScreenshortInReport("error occured while getting list of active Windows: " + e);
		}
		return null;
	}

	/**
	 * @param WaitSeconds
	 * @return
	 */
	public boolean WaitInSeconds(String WaitSeconds) {
		boolean _status = false;
		try {
			logger.debug("  Entered into MaxWait method");
			int Min = Integer.parseInt(WaitSeconds);
			logger.info("Wait in Seconds: " + Min / 1000);
			SetupFCTestReport.GetInstance().pass("Wait in Seconds: " + Min / 1000);
			Thread.sleep(Min);
			_status = true;
			logger.debug("  Left from MaxWait method");
		} catch (Exception e) {
			throw new SetupFCException("error occured while waiting: " + e);
		}
		return _status;
	}

	@Override
	public boolean MinWait() {
		return WaitInSeconds(GlobalVaraibles.MinimumWait);
	}

	public boolean ExtMinWait() {
		return WaitInSeconds(GlobalVaraibles.ExtMinimumWait);
	}

	@Override
	public boolean MediumWait() {
		return WaitInSeconds(GlobalVaraibles.MediumWait);

	}

	@Override
	public boolean MaxWait() {
		return WaitInSeconds(GlobalVaraibles.MaximumWait);
	}

	/**
	 * Method used to capture the screenshot of the current screen
	 * @param TestCaseID
	 * @param Action
	 */
	@Override
	public String CaptureScreenShot(String TestCaseID) {

		String _PathofScrnshot = "";
		try {

			logger.debug("  Entered into CaptureScreenShot method");
			TakesScreenshot scrShot = ((TakesScreenshot) driver);
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			DateFormat dft = new SimpleDateFormat("yyyyMMdd_hhmmss");
			String date = df.format(new Date());
			String datetime = dft.format(new Date());
			_PathofScrnshot = GlobalVaraibles.ScreenShotFolderPath + "\\" + date
					+ "\\" +datetime  + "_" + TestCaseID + ".png";
			File DestFile = new File(_PathofScrnshot);
			FileUtils.copyFile(SrcFile, DestFile);
			logger.info("Screenshot is captured and store in : .//" + _PathofScrnshot);
			logger.debug("  Left from CaptureScreenShot method");
			System.out.println(System.getProperty("user.dir"));
			return System.getProperty("user.dir") + "\\"+_PathofScrnshot;
		} catch (Exception e) {

			throw new SetupFCException("Error occured while capturing the screenshot:- " + e);
		}

	}

	/**
	 * Addding Screenshot in the test report with error details
	 * @param errorDetails
	 */
	public void AddScreenshortInReport(String errorDetails) {
		try {
			logger.error(errorDetails);
			String ScrnshtPath = CaptureScreenShot(GlobalVaraibles.TestScriptID);
			String scrnshot = "<img class='r-img' onerror='this.style.display=\"none\"" + "' data-featherlight='"
					+ ScrnshtPath + "' src='" + ScrnshtPath + "' data-src='" + ScrnshtPath + "'>";

			String msg = "<details>" + "<summary>" + "<b>" + "<font color =" + "red>" + "Error Occured: Click to view"
					+ "</font>" + "</b>" + "</summary>" + "<b style=\"background-color:powderblue;color:black;\">"
					+ errorDetails.replaceAll(",", "<br>") + "</b>" + "<br>" + scrnshot + "</details>" + "\n";
			SetupFCTestReport.GetInstance().fail(msg);
			// AfterSuite();

		} catch (Exception e) {
			throw new SetupFCException("Error occured while capturing the screenshot:- ", e);
		}
	}

	
	public WebElement CreateWebelement(By by) {
		WebElement wb =null;
		logger.debug(" Entered into CreateWebelement method>>>>");
		try {
			
			WaitForPresenceAndVisibilityOfElements(by);
			 wb = driver.findElement(by);
			logger.info("Welement is created for the:"+ by.toString());
		} 
		
		catch (Exception e) {
			throw new SetupFCException("Error occured while creating a web element due to:"+e);
			}
		logger.debug("  Left from CreateWebelement method");
		return wb;
	}
 
	
	@Override
	public List<WebElement> CreateWebElements(By by) {
		logger.debug("  Entered into CreateWebElements method");
		List<WebElement> wb=null;
		try {
			//WaitForPresenceAndVisibilityOfElements(by);
			wb = driver.findElements(by);
			logger.info("Welement is created for the:"+ by.toString());	
		} 
		
		catch (Exception e) {
			throw new SetupFCException("Error occuring while creating the webelements:- " ,e);
		}
		logger.debug("  Left from CreateWebElements method");
		return wb;
	}
	/**
	 * @param WebElements
	 * @param Text
	 * @return WebElement
	 */
	private WebElement GetWebElementFromText(List<WebElement> WebElements, String Text) {
		WebElement _wb = null;

		List<WebElement> elements = WebElements;
		Iterator<WebElement> element_iterator = elements.iterator();
		while (element_iterator.hasNext()) {
			_wb = element_iterator.next();
			if (_wb.getText().equalsIgnoreCase(Text))
				break;
		}
		return _wb;
	}
/**
 	 *  @implNote both Locators should be in form of xpath only
	 * @param by
	 * @param SiblingElement
	 * @param ElementText
	 * @return boolean
 */
private WebElement GetWebElementFromAncestor(By by, By Sibling, String ElementText)
{
	WebElement _wb = null;
	try {
		System.out.println("test");
		logger.debug(" Entered into click method::GetWebElementFromAncestor");
		String SiblingElement = Sibling.toString().replaceAll("By.xpath:", "").trim();
		if (WaitForPresenceAndVisibilityOfElements(by)) {
			List<WebElement> elements = CreateWebElements(by);
			ListIterator<WebElement> ElementsIterator = elements.listIterator();
			if (SiblingElement.startsWith("//"))
				SiblingElement = SiblingElement.replaceFirst("//", "");
			By SiblingElelemnt = By.xpath(by.toString().replaceAll("By.xpath:", "") + "/ancestor::" + SiblingElement);
			ListIterator<WebElement> SibLngElementsIterator = CreateWebElements(SiblingElelemnt).listIterator();
			SKIP:
			while (ElementsIterator.hasNext()) {
				WebElement wb = ElementsIterator.next();
				while (SibLngElementsIterator.hasNext()) {
					WebElement SibLngWB = SibLngElementsIterator.next();
					if (SibLngWB.getText().contains(ElementText))
					{
						logger.info(wb+":: WebElement Siblings/Ancestor::"+SibLngWB+":: And Element Text: "+ElementText);
						return wb;
					}
					continue SKIP;
				}

			}

		}
		logger.info("No WebElement found for the Locator:: "+by+":: with Sibling ::"+Sibling+":: for the element text"+ElementText);
		SetupFCTestReport.GetInstance().error("No WebElement found for the Locator:: "+by+":: with Sibling ::"+Sibling+":: for the element text"+ElementText);
	} catch (Exception e) {
		throw new SetupFCException("Error occured while Geeting Webelement for the Locator:: "+by+":: with Sibling ::"+Sibling+":: for the element text"+ElementText+","+e);		
	}
	return _wb;
}
	/**
	 * Waiting for the Element visibility
	 */
	
	/**
	 *Waiting for the Elements visibility
	 */
	@Override
	public boolean WaitForPresenceAndVisibilityOfElements(By by) {

		boolean _status = false;
		try {
			logger.debug("  Entered into WaitForPresenceAndVisibilityOfElement method");
			WebDriverWait wait = new WebDriverWait(driver, GlobalVaraibles.Default_ExtMaxWait/1000);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
			ScrollToViewElement(by);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
			_status=true;
			
			logger.debug("  Left from WaitForPresenceAndVisibilityOfElement method");
		}  catch (Exception e) {
			AddScreenshortInReport("Unable to locate the web elements in the page for:" + by);
			throw new SetupFCException("Error occured while waiting for PresenceAndVisibilityOfElement:-"+by+","+e);
		}
		return _status;
	}
	
	/**
	 * @param by
	 * @return boolean
	 */
	public boolean WaitForAbsenceAndInvisibilityOfElements(By by) {

		boolean _status = false;
		try {
			logger.debug("  Entered into WaitForAbsenceAndInvisibilityOfElements method");
			WebDriverWait wait = new WebDriverWait(driver, GlobalVaraibles.Default_ExtMaxWait/1000);
			if (wait.until(ExpectedConditions.invisibilityOfElementLocated(by)))
			{
				logger.debug(by+ ": Element is Absent or not visible in the page ");
				_status = true;
			}				
			logger.debug(" Left from WaitForAbsenceAndInvisibilityOfElements method");
		}
		
		catch (Exception e) {
			throw new SetupFCException("Error occured while waiting for WaitForAbsenceAndInvisibilityOfElements:-"+by+" :"+ e);
		//	logger.debug("  Error occured while waiting for PresenceAndVisibilityOfElement:-" + e);
		}
		return _status;
	}

	/**
	 *
	 */
	@Override
	public boolean IsElementPresentAndVisible(By by) {

		boolean _status = false;
		try {
			logger.debug("  Entered into CreateWebelement method");
			List<WebElement> WE=driver.findElements(by);
			if (WE.size()>0)
			{
				Iterator<WebElement> WE_iterator = WE.iterator();
				while(WE_iterator.hasNext())
					if(WE_iterator.next().isDisplayed())	
						_status = true;
			}
			logger.debug("  Left from CreateWebelement method");

		} catch (Exception e) {
			throw new SetupFCException("  Error occuring while creating the webelement :- " ,e);
			//logger.error("  Error occuring while creating the webelement :- " + e);
		}
		return _status;
	}

	/**
	 *
	 */
	@Override
	public boolean WaitForElementToBeClickable(By by) {
		boolean _status = false;
		try {
			logger.debug("  Entered into WaitForElementToBeClickable method");
			WebDriverWait wait = new WebDriverWait(driver, GlobalVaraibles.Default_ExtMaxWait/1000);
			wait.until(ExpectedConditions.elementToBeClickable(by));
			logger.debug("  Left from WaitForElementToBeClickable method");
			_status=true;
		} catch (StaleElementReferenceException elementHasDisappeared) {
			WaitForElementToBeClickable(by);
		} catch (Exception e) {
			throw new SetupFCException("  Error occuring while waiting for web element visibility :- " + e);
		}
		return _status;
	}

	/**
	 *
	 */
	@Override
	public boolean IsElementEnabled(By by) {

		logger.debug(":Entered into IsElementEnabled method");
		boolean _status = false;
		if (CreateWebelement(by).isEnabled()) {
			_status = true;

		} else {
			AddScreenshortInReport(by + " element is not enabled in the web page");
			logger.error(by + "element is not enabled in the web page");
		}
		logger.debug(":Left from the IsElementEnabled method");
		return _status;
	}

	/**
	 *
	 */
	@Override
	public boolean CloseTab() {
		boolean _status = false;
		try {
			driver.close();
			SetupFCTestReport.GetInstance().pass("Close the current Tab");
			logger.info("Close the current Tab");
			_status = true;
		} catch (Exception e) {
			logger.error("  Error occuring while Closing window :- " + e);
			SetupFCTestReport.GetInstance().pass("Current Tab is closed aleady or Error occuring while Closing the window :- " +e);
		}

		return _status;
	}

	
	public String GetWindowTitle() {

		try {
			SetupFCTestReport.GetInstance().pass("Current Window Title is: [" + driver.getTitle() + "]");
			logger.info("Current Window Title is: [" + driver.getTitle() + "]");
			return driver.getTitle();
		} catch (Exception e) {
			logger.error("  Error occuring while Closing window :- " + e);
			SetupFCTestReport.GetInstance().fail("Error occuring while the getting the window Title :- " + e);
			return e.getMessage();
		}
	}
	
	/**
	 * Closing the entire browser
	 * @return
	 */
	public boolean CloseBrowser() {
		try {
			if (((RemoteWebDriver) driver).getSessionId() == null)
				return true;
			driver.quit();
			logger.info("Closing the browser");
			SetupFCTestReport.GetInstance().pass("Browser is closed!");
		} catch (SessionNotCreatedException e) {
			logger.info("Browser is already closed " + e);
			SetupFCTestReport.GetInstance().fail("Browser is already closed " + e);

		} catch (Exception e) {
			logger.info("Browser is already closed " + e);
			SetupFCTestReport.GetInstance().fail("Browser is already closed " + e);
		}

		return true;
	}

	/**
	 * @return Alert
	 */
	private Alert SwitchtoAlert() {

		try {
			logger.debug(": entered to SwitchtoAlert method");
			if (isAlertPresent()) {
				return driver.switchTo().alert();

			}
			logger.debug(": Left the SwitchtoAlert method");

		} catch (Exception e) {
			logger.error("  Error occuring while Switching to alert window :- " + e);
		}
		return null;
	}
	
	/**
	 * @param FrameNmbr
	 * @return
	 */
	public  boolean SwitchToFrame(int FrameNmbr) {
		boolean _status=false;
		logger.debug("Entered to Switch to Frames method");
		try {
			 driver.switchTo().frame(FrameNmbr);
			 logger.info("Web driver switched to frame:"+FrameNmbr);
			 _status=true;
		} catch (Exception e) {
			logger.error(" Error occuring while Switching to frame :- " + e);
		}
		logger.debug(": Left the Switch to Frames method");
		return _status;
	}
	/**
	 * @param FrameNmbr
	 * @return
	 */
	public  boolean SwitchToDefaultWebPage() {
		boolean _status=false;
		logger.debug("Entered to Switch to Frames method");
		try {
			 driver.switchTo().defaultContent();
			 logger.info("Web driver switched to Default web page");
			 _status=true;
		} catch (Exception e) {
			logger.error(" Error occuring while Swithcing to default web page :- " + e);
		}
		logger.debug(": Left the Switch to Frames method");
		return _status;
	}
	/**
	 * Accepted Alert commands---Done,Accept,Ok,Cancel,Close,No
	 * @param AlertCmnd
	 * @param Description
	 * @return boolean value
	 */
	public boolean CloseAlertWindow(String AlertCmnd, String Description) {
		boolean _status = false;
		try {
			Alert alert = SwitchtoAlert();
			switch (AlertCmnd.toUpperCase()) {
			case "DONE":
			case "ACCEPT":
			case "OK":
				alert.accept();
				break;
			case "CANCEL":
			case "CLSOE":
			case "NO":
				alert.dismiss();
				break;
			default:
				alert.dismiss();
				break;

			}
			 driver.switchTo().activeElement();
			SetupFCTestReport.GetInstance().pass(Description + "=" + AlertCmnd);
			_status = true;
		} catch (Exception e) {
			logger.error("Error occured while Closing the alrt window: " + e);

		}
		return _status;
	}
	/**
	 * @param AlertCmnd
	 * @param Description
	 * @return
	 */
	public boolean SendTextinAlert(String Text) {
		boolean _status = false;
		try {
			Alert alert = SwitchtoAlert();
			alert.sendKeys(Text);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			_status = true;
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			logger.error("Error occured while Closing the alrt window: " + e);
		}
		return _status;
	}

	public boolean isAlertPresent() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, GlobalVaraibles.Default_ExtMaxWait/1000);
			wait.until(ExpectedConditions.alertIsPresent());
			return true;
		} 
		catch (NoAlertPresentException Ex) {
			return false;
		}
	}
	private static void CopyInClipboard(String Text)
	{
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		StringSelection stringSelection = new StringSelection(Text);
		clipboard.setContents(stringSelection, null);
	}
	/**
	 *  Select the file from Windows FileUploader
	 *  FileName---FileName should be added with Path
	 * @param File
	 * @return boolean
	 */
	public boolean uploadWindowFile(String FileName)
	{
		logger.debug("~~~~~Entering into UploadFile method for uploading from windows file>> " + FileName + "~~~~~~~~");
		boolean _Status=false;
		try {
			MinWait();
			if (!FileName.contains(":")) {
				FileName = FileName.replace("./", "").replace("/", "\\");
				FileName = System.getProperty("user.dir") + "\\" + FileName;
			}
			
			CopyInClipboard("");
			CopyInClipboard(FileName);
			Robot robot = new Robot();
			 robot.keyPress(KeyEvent.VK_CONTROL);
			  robot.keyPress(KeyEvent.VK_V);
			  robot.keyRelease(KeyEvent.VK_V);
			  robot.keyRelease(KeyEvent.VK_CONTROL);
			  robot.keyPress(KeyEvent.VK_ENTER);
			  robot.keyRelease(KeyEvent.VK_ENTER);
			  logger.info(FileName + "File is uploaded successfully");
			 _Status=true;
		}
			catch(Exception e)
		{
				logger.error(FileName + ">>>>Error occured while Uploading the file from the window : " + e);
				AddScreenshortInReport(FileName + ">>>>Error occured while Uploading the file from the window : " + e);	
		}
				return _Status;
	}
	
	/**
	 * Upload file when element type is file
	 * @param FileName
	 * @param Description
	 * @param by
	 * @return
	 */
	public boolean UploadFile(String FileName, String Description, By... by) {
		logger.debug("~~~~~Entering into UploadFile method for uploading  file>> " + FileName + "~~~~~~~~");
		boolean _status = false;
		try {
			if (!FileName.contains(":")) {
				FileName = FileName.replace("./", "").replace("/", "\\");
				FileName = System.getProperty("user.dir") + "\\" + FileName;
			}
			if (by.length == 1)
				CreateWebelement(by[0]).sendKeys(FileName);
			else if (by.length == 2)
				CreateWebelement(by[0]).findElement(by[1]).sendKeys(FileName);
			else {
				logger.error("Unsupported: Upto 2 elements are supported but actual Elements size >>" + by.length);
				AddScreenshortInReport(
						"Unsupported: Upto 2 elements are supported but actual Elements size >>" + by.length);
				return false;
			}

			SetupFCTestReport.GetInstance().pass(Description + ">>> " + FileName);
			_status = true;
		} catch (Exception e) {
			logger.error(FileName + ">>>>Error occured while Uploading the file : " + e);
			AddScreenshortInReport(FileName + ">>>>Error occured while Uploading the file : " + e);
		}
		logger.debug("~~~~~Exiting from UploadFile method~~~~~~~~");
		return _status;
	}
	/**
	 * @author smallika
	 *
	 */
	private enum Browser {
		CHROME, FIREFOX, IE, SAFARI,HEADLESS,HEADLESSCHROME;
	}

	//==================================+++++++Usable KEYWORDS+++++==============================================================
	/**
	 * Functions or Keywords
	 */

	/**
	 * @param by
	 * @param Description
	 * @return boolean
	 */
	public boolean Click(By by, String Description) {
		boolean _status = false;
		logger.debug("~~~~~Entering into click method for: " + Description + "~~~~~~~~");
		WebElement wb=null;
		try {			
				wb=	CreateWebelement(by);
				WaitForElementToBeClickable(by);
				wb.click();
				SetupFCTestReport.GetInstance().pass(Description);
				logger.info(Description);
				_status = true;
			
		} catch (StaleElementReferenceException elementHasDisappeared) {
			logger.debug("Retry to create and click for element due to " + elementHasDisappeared);
			_status = Click(by, Description);
		} catch (Exception e) {
			logger.error(Description + ":Error occured while clicking :- " + e);
			AddScreenshortInReport(Description + ":Error occured while clicking :- " + e);
		}
		
		logger.debug("~~~~~Exiting from click method for " + Description + "~~~~~~~~");
		return _status;
	}

	/**
	 * @param by
	 * @param ElementText
	 * @param Description
	 * @return boolean
	 */
	public boolean ClickElementText(By by, String ElementText, String Description) {
		boolean _status = false;
		WebElement wb=null;
		try {
			logger.debug("~~~~~Entering to ClickElementText method for "+Description+"~~~~~~~~");
				wb = GetWebElementFromText(CreateWebElements(by), ElementText);
				wb.click();
				SetupFCTestReport.GetInstance().pass(Description + "=" + ElementText);
				logger.info(Description + "=" + ElementText);
				_status = true;
			
		}catch (StaleElementReferenceException elementHasDisappeared) {
			_status=ClickElementText(by,ElementText,Description);
		} 
		catch (Exception e) {

			logger.error(Description+":Error occured while clicking :- " + e);
			AddScreenshortInReport(Description+" :Error occured while Clicking for ElementText :- " +ElementText+ e);
		}
		logger.debug("~~~~~Exiting from ClickElementText method for "+Description+"~~~~~~~~");
		return _status;
	}
	

	/**
	 * @implNote both Locators should be in form of xpath only
	 * @param by
	 * @param SiblingElement
	 * @param ElementText
	 * @param Description
	 * @return boolean
	 */
	public boolean ClickElementTextByAncestor(By by, By Sibling, String ElementText, String Description) {
		boolean _status = false;
		try {
			logger.debug("~~~~~Entering to ClickElementTextByAncestor method for "+Description+"~~~~~~~~");
			WebElement wb=GetWebElementFromAncestor(by,Sibling,ElementText);
				wb.click();
				SetupFCTestReport.GetInstance().pass(Description + "=" + ElementText);
				logger.info(Description + "=" + ElementText);
				_status=true;
				
		}
		catch (StaleElementReferenceException elementHasDisappeared) {
			logger.debug(Description+"Retyring due to:: "+elementHasDisappeared.getMessage());
			ClickElementTextByAncestor(by,Sibling,ElementText,Description);
		} 
		catch (Exception e) {

			logger.error(Description+":Error occured while clicking :- " + e);
			AddScreenshortInReport(Description+" :Error occured while Clicking for ElementText :- " + e);
		}
		logger.debug("~~~~~Exiting from ClickElementTextByAncestor method for "+Description+"~~~~~~~~");
		return _status;
	}


	/**
	 * @param by
	 * @param Description
	 * @return boolean
	 */
	public boolean clickUsingJS(By by, String Description) {
		logger.debug("~~~~~~~~ Entered into clickUsingJS method~~~~~~~~~");
		boolean _status = false;
		try {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", CreateWebelement(by));
				SetupFCTestReport.GetInstance().pass(Description);
				logger.info(Description);
				_status = true;
		
		} 
		catch (StaleElementReferenceException elementHasDisappeared) {
			clickUsingJS(by,Description);
		}
		catch (Exception e) {
			logger.error(Description+" :Error occured while clicking :- " + e);
			AddScreenshortInReport(Description+" :Error occured while clickUsingJS :- " + e);
		}
		
		logger.debug("~~~~~Exiting from clickUsingJS method for "+Description+"~~~~~~~~");
		return _status;
	}

	/**
	 * @param by
	 * @param Text
	 * @param Description
	 * @return boolean
	 */
	public boolean SendKeys(By by, String Text, String Description) {
		boolean _status = false;
		try {
			logger.debug("~~~~~~~Entered into SendKeys method for[ "+Description+"~~~~~~~~");
			
				WebElement _wb = CreateWebelement(by);
				WaitForElementToBeClickable(by);
				_wb.click();
				_wb.clear();
				_wb.sendKeys(Text);
				if(by.toString().toLowerCase().contains("password")||by.toString().toLowerCase().contains("pwd")||Description.toLowerCase().contains("password"))
				{
					logger.info(Description + " = " + "*********");	
					SetupFCTestReport.GetInstance().pass(Description + " = " + "*********");
				}
				else
				{	logger.info(Description + " = " + Text);
				SetupFCTestReport.GetInstance().pass(Description + " = " + Text);
				
				}
				_status = true;
			
		}catch (StaleElementReferenceException elementHasDisappeared) {
			logger.warn(elementHasDisappeared);
			_status=SendKeys(by,Text,Description);
		} 
		catch (Exception e) {
			logger.error(Description+" :Error occured while Entering text :- " + e);
			AddScreenshortInReport(Description+" :Error occured while Entering the Text :- " + e);
		}
		
		logger.debug("~~~~~Exiting from SendKeys method for "+Description+"~~~~~~~~");
		return _status;
	}
	
	/**
	 * @param by
	 * @param ElementText
	 * @param StepDescription
	 * @return
	 */
	public boolean SendKeysforElementText(By by, String ElementText, String Text,String Description) {
		boolean _status = false;
		try {
			logger.debug("  Entered into SendKeysforElementText method for >>"+ Description);
			
				WebElement _wb = GetWebElementFromText(CreateWebElements(by), ElementText);
				_wb.click();
				_wb.clear();
				_wb.sendKeys(Text);
				SetupFCTestReport.GetInstance().pass(Description + " = " + Text);
				logger.info(Description + " = " + Text);
				_status = true;

		}catch (StaleElementReferenceException elementHasDisappeared) {
			SendKeysforElementText(by,ElementText,Text,Description);
		} 
		catch (Exception e) {

			logger.error(Description+":Error occured while SendKeysforElementText :- " + e);
			AddScreenshortInReport(Description+" :Error occured while Sending for ElementText :- " + e);
		}
		logger.debug("~~~~~Exiting from SendKeys method for "+Description+"~~~~~~~~");
		return _status;
	}
	public boolean SendKeysForElementTextFromAncestor(By by, By Siblings,String ElementText, String Text,String StepDescription) {
		boolean _status = false;
		try {
			logger.debug("  Entered into SendKeysforElementText method for >>"+ StepDescription);
				WebElement _wb = GetWebElementFromAncestor(by, Siblings, ElementText);
				if (_wb!=null) {
				_wb.click();
				_wb.clear();
				_wb.sendKeys(Text);
				SetupFCTestReport.GetInstance().pass(StepDescription + " = " + Text);
				logger.info(StepDescription + " = " + Text);
				_status = true;

			}
			logger.debug(":Left from SendKeysforElementText method");
		}catch (StaleElementReferenceException elementHasDisappeared) {
			SendKeysforElementText(by,ElementText,Text,StepDescription);
		} 
		catch (Exception e) {

			logger.error(StepDescription+":Error occured while SendKeysforElementText :- " + e);
			AddScreenshortInReport(StepDescription+" :Error occured while Sending for ElementText :- " + e);
		}
		return _status;
	}
	/**
	 * @param by
	 * @param Text
	 * @param Description
	 * @return boolean
	 */
	public boolean SendKeysUsingJS(By by, String Text, String Description) {
		boolean _status = false;
		try {
			logger.debug("  Entered into SendKeysThroughJS method");
				WebElement textBox = CreateWebelement(by);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", textBox);
				js.executeScript("arguments[0].value = '" + Text + "';", textBox);
				SetupFCTestReport.GetInstance().pass(Description + "= " + Text);
				logger.info(Description + " = " + Text);
				_status = true;
		} 
		catch (StaleElementReferenceException elementHasDisappeared) {
			logger.warn(elementHasDisappeared.getMessage());
			_status=SendKeysUsingJS(by,Text,Description);
		}
		catch (Exception e) {
			logger.error("  Error occured while Entering text :- " + e);
			AddScreenshortInReport(" Error occured while Entering the Text :- " + e);
		}
		return _status;
	}

	/**
	 * @param by
	 * @param Description
	 * @return
	 */
	public boolean ClearText(By by, String Description) {
		boolean _status = false;
		try {
			logger.debug("  Entered into ClearText method");
				CreateWebelement(by).clear();
				SetupFCTestReport.GetInstance().pass(Description);
				logger.info(Description );
				_status = true;
		}
		catch (StaleElementReferenceException elementHasDisappeared) {
			logger.warn(elementHasDisappeared.getMessage());
			_status =ClearText(by,Description);
		}
		catch (Exception e) {
			logger.error(Description+" :Error occured while Clearing the Text :- " + e);
			AddScreenshortInReport(Description+" :Error occured while Clearing the Text :- " + e);
		}
		return _status;
	}

	/**
	 * @param by
	 * @param Description
	 * @return boolean
	 */
	private boolean ScrollToViewElement(By by) {
		boolean _status = false;
		try {
			logger.debug("  Entered into ScrollToViewElement method");
			 {
				
		/*		JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);;", CreateWebelement(by));
				SetupFCTestReport.GetInstance().pass(Description);
			*/
				 Actions a = new Actions(driver);
				List<WebElement> elements = driver.findElements(by);
			      a.moveToElement(elements.get(0));
			      a.perform();
				logger.info("Scrolling to veiw the element" );
				//ExtMinWait();
				_status = true;
			}
			logger.debug(" Exiting from ScrollToViewElement method");
		}
		
		catch (Exception e) {
			logger.error("  Error occured while scrolling :- " + e);
			AddScreenshortInReport(":Error occured while Scrolling to view element :- " + e);
		}
		return _status;
	}

	public boolean ScrollToBottom(String Description) {
		boolean _status = false;
		try {
			logger.debug("  Entered into ScrollToBottom method");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,250)", "");
			//ExtMinWait();
			SetupFCTestReport.GetInstance().pass(Description);
			logger.info(Description);
			_status = true;

			logger.debug(" Exiting from ScrollToBottom method");
		}

		catch (Exception e) {
			logger.error("  Error occured while clicking :- " + e);
			AddScreenshortInReport(":Error occured while Scrolling to bottom of the page :- " + e);
		}
		return _status;
	}

	/**
	 * @param by
	 * @param Text
	 * @param Description
	 * @return boolean
	 */
	public boolean SelectValueFromDropdown(By by, String Text, String Description) {
		boolean _status = false;
		try {
			logger.debug(":Entered into SelectValueFromDropdown method");
				WebElement selectwebelement = CreateWebelement(by);

				Select selectItem = new Select(selectwebelement);
				selectItem.selectByVisibleText(Text);
				SetupFCTestReport.GetInstance().pass(Description + " = " + Text);
				logger.info(Description+ " ="+Text);
				_status = true;
			
		}
		catch (StaleElementReferenceException elementHasDisappeared) {
			_status =SelectValueFromDropdown(by,Text,Description);
		}
		catch (Exception e) {
			logger.error(":Error occured while selecting the drop down :- " + e);
			AddScreenshortInReport(":Error occured while selecting the drop down value :-  for["+Description +" ]"+ e);
		}

		logger.debug(":Left from the SelectValueFromDropdown method");
		return _status;
	}

	/**
	 * @param URL
	 * @return boolean
	 */
	public boolean LaunchApplication(String URL, String Description) {
		boolean _status = false;
		try {
			driver=	Driver(IniFile.GetInstance().ReadIniFile(
					GlobalVaraibles.ExecutionSection, "EnvironmentBrowser"));
			logger.debug("  Entered into LaunchApplication method");
			driver.get(URL);
			SetupFCTestReport.GetInstance().pass(Description + "=" + URL);
			logger.info(Description+ "=" + URL );
			_status = true;
			logger.debug(":Left from the LaunchApplication method");
		} catch (Exception e) {
			logger.error(":Error occured while launching the URL :- " + e);
			SetupFCTestReport.GetInstance().error(":Error occured while launching the URL :- " + e);
		}
		return _status;
	}

	/**
	 * @param by
	 * @param attribute
	 * @param attributevalue
	 * @return boolean
	 */
	public boolean VerifyAttributevalue(By by, String attribute, String attributevalue, String Description) {
		boolean _status = false;
		try {
			logger.debug("  Entered into VerifyAttributevalue method");
			String _Value = GetAttributeValue(by, attribute, Description);
			if (_Value.equalsIgnoreCase(attributevalue)) {
				SetupFCTestReport.GetInstance()
						.pass("Expected Value is [" + attributevalue + "] Actual Value is [" + _Value + "]");
				logger.info("Expected Value is [" + attributevalue + "] Actual Value is [" + _Value + "]" );
				_status = true;
			} else
				SetupFCTestReport.GetInstance()
						.fail("Expected Value is [" + attributevalue + "] but Actual Value is [" + _Value + "]");

			logger.debug("  Left from the VerifyAttributevalue method");
		} catch (Exception e) {
			logger.error(":Error occured while VerifyAttributes :- " + e);
			AddScreenshortInReport(":Error occured while VerifyAttributes :- for["+Description +" ]" + e);
		}
		return _status;
	}

	public String GetAttributeValue(By by, String attribute, String Description) {
		String _Value = "";
		try {
			logger.debug("  Entered into GetAttributeValue method");
				WebElement wb = CreateWebelement(by);
				_Value = wb.getAttribute(attribute);
				SetupFCTestReport.GetInstance().pass(Description + " =" + _Value);
				logger.info(Description + " =" + _Value);
			
			
		} catch (Exception e) {
			logger.error(":Error occured while GetAttributeValue :- " + e);
			AddScreenshortInReport(":Error occured while GetAttributeValue :- for["+Description +" ]" + e);
		}
		logger.debug(":Left from the GetAttributeValue method");
		return _Value;
	}

	/**
	 * @param by
	 * @param attribute
	 * @param StoreKey
	 * @param Description
	 * @return
	 */
	public boolean GetAttributeValueAndStore(By by, String attribute, String StoreKey, String Description) {
		boolean _status = false;
		try {
			logger.debug(":Entered into GetAttributeValueandStore method");
			String value = GetAttributeValue(by, attribute, Description);
			if (value != null && !value.isEmpty()) {
				IniFile.GetInstance().WriteIniFile(
						GlobalVaraibles.ExecutionSection, StoreKey, value);
				SetupFCTestReport.GetInstance().pass("[" + StoreKey + "] value is updated with [" + value
						+ "] in the section " + GlobalVaraibles.ExecutionSection);
				logger.info(Description);
				_status = true;
			} else
				AddScreenshortInReport(
						"Unable to find attribute value for locater: " + by + "and attribute: " + attribute);
			logger.debug(":Left from the GetAttributeValueandStore method");
		} catch (Exception e) {
			logger.error(":Error occured while GetAttributeValue :- " + e);
			AddScreenshortInReport(":Error occured while getting attribute value and store : for["+Description +" ]" + e);
		}
		return _status;

	}

	/**
	 * @param by
	 * @param Description
	 * @return
	 */
	public String GetText(By by, String StoreKey, String Description) {
		String _Value = null;
		try {
			logger.debug(":Entered into GetText method");
				WebElement wb = CreateWebelement(by);
				_Value = wb.getText();
				SetupFCTestReport.GetInstance().pass(Description + " = " + _Value);
				if (StoreKey != null && !StoreKey.isEmpty()) {
					IniFile.GetInstance().WriteIniFile(
							GlobalVaraibles.ExecutionSection, StoreKey, _Value);
					SetupFCTestReport.GetInstance()
							.pass("[" + StoreKey + "] value is updated with [" + _Value + "] in the section "
									+ GlobalVaraibles.ExecutionSection);
					logger.info(Description );
				}
			
			
		}catch (StaleElementReferenceException elementHasDisappeared) {
			logger.warn(elementHasDisappeared.getMessage());
			_Value=	GetText(by,StoreKey,Description);
		} 
		catch (Exception e) {
			logger.error(":Error occured while GetText :- " + e);
			AddScreenshortInReport("Error occured while Getting the Text ["+Description +" ]" + e);

		}
		logger.debug(">>>Exited from  GetText method");
		return _Value;
	}

	/**
	 * @param by
	 * @param Description
	 * @return
	 */
	public String GetText(By by, String Description) {
		return GetText(by, "", Description);
	}

	/**
	 * @param by
	 * @param Text
	 * @param Description
	 * @return
	 */
	public boolean ValidateText(By by, String Text, String Description) {
		boolean _status = false;
		String OutPut_text = GetText(by, Description);
		if (CommonScript.VerifyContent(Text, OutPut_text)) {
			SetupFCTestReport.GetInstance()
					.pass(Description + ":- Expected value [ " + Text + " ] and Actual Value [" + OutPut_text + " ]");
			logger.info(Description + ":- Expected value [ " + Text + " ] and Actual Value [" + OutPut_text + " ]");
			_status = true;
		} else {
			AddScreenshortInReport(Description + ":-Expected value [" + Text + " ] But Actual Value [ " + OutPut_text + " ]");
			logger.error(Description + "[Expected value] " + Text + "[But Actual Value] " + OutPut_text);
		}
		return _status;

	}

	/**
	 * @param by
	 * @param Text
	 * @param Description
	 * @return
	 */
	public boolean ValidatePartialText(By by, String Text, String Description) {
		boolean _status = false;
		String text = GetText(by, Description);
		if (text.contains(Text)) {
			SetupFCTestReport.GetInstance()
					.pass(Description + ":- Expected value [ " + Text + " ] and Actual Value [" + text + " ]");
			logger.info(Description  + ":- Expected value [ " + Text + " ] and Actual Value [" + text + " ]");
			_status = true;
		} else {
			AddScreenshortInReport(Description + ":-Expected value [" + Text + " ] But Actual Value [ " + text + " ]");
			logger.error(Description + "[Expected value] " + Text + "[But Actual Value] " + text);
		}
		return _status;

	}

	/**
	 * @param by
	 * @param Text
	 * @param Description
	 * @return
	 */
	public boolean VerifyTextfromElements(By by, String Text, String Description) {
		WebElement wb = null;
		try {
		if (WaitForPresenceAndVisibilityOfElements(by)) {
			ListIterator<WebElement> webelementsIterator = CreateWebElements(by).listIterator();
			while (webelementsIterator.hasNext()) {
				wb = webelementsIterator.next();
				if (wb.getText().contains(Text)) {
					SetupFCTestReport.GetInstance().pass(Description + "=" + wb.getText());
					logger.info(Description + "=" + wb.getText());
					return true;
				}
			}

		}
			AddScreenshortInReport("Unable to find the text: " + Text + " in locator: " + by);
			logger.error("Unable to find the text: " + Text + " in locator: " + by);
		}
		catch(Exception e)
		{
			AddScreenshortInReport("Error occured while verifying for [" + Description+"="+Text + " ]" + e);
		}
		return false;
	}

	public boolean VerifyNoTextfromElements(By by, String Text, String Description) {
		WebElement wb = null;
		try {
			ListIterator<WebElement> webelementsIterator = CreateWebElements(by).listIterator();
			while (webelementsIterator.hasNext()) {
				wb = webelementsIterator.next();
				if (wb.getText().contains(Text)) {
					AddScreenshortInReport("["+Text+"] is present in WebElement [" +by+ "] and full text ["+ wb.getText()+"]");
					logger.error("["+Text+"] is present in WebElement [" +by+ "] and full text ["+ wb.getText()+"]");
					return false;
				}
			}
		SetupFCTestReport.GetInstance().pass(Description+"="+Text);
		logger.info(Description+"="+Text);
		return true;
		}
		catch(Exception e)
		{
			AddScreenshortInReport("Error occured while verifying for [" + Description+"="+Text + " ]" + e);
			return false;
		}
		
	}
	public boolean VerifyListofTextFromElements(By by, String Values, String Description) {
		boolean _status = false;
		try {
			String[] ArrayofTexts = GetArrayofTextFrmElement(by, Description);
			String[] arrayofValues = Values.split("~#~");
			Arrays.sort(ArrayofTexts);
			Arrays.sort(arrayofValues);
			if (Arrays.deepEquals(ArrayofTexts, arrayofValues)) {
				SetupFCTestReport.GetInstance().pass(Description + "=" + Values);
				_status = true;
			} else
				AddScreenshortInReport("Mismatch in array of values and expected values: " + Values);
		} catch (Exception e) {
			AddScreenshortInReport("Error occured while verifying for[" + Description + " ]" + e);
		}
		return _status;
	}

	private String[] GetArrayofTextFrmElement(By by, String Description) {
		logger.debug("  Entered into GetArrayofTextFrmElement method");
		try {
		
				List<WebElement> ListofElements = CreateWebElements(by);
				int temp = ListofElements.size(), temp1 = 0;
				String[] ArrayofElementTxt = new String[temp];
				ListIterator<WebElement> webelementsIterator = ListofElements.listIterator();
				while (webelementsIterator.hasNext()) {

					ArrayofElementTxt[temp1] = webelementsIterator.next().getText();
					temp1++;
				}
				return ArrayofElementTxt;
			
		} catch (Exception e) {
			AddScreenshortInReport("Error occured while verifying the element: " + e);
		} 
		
		logger.debug("  Exiting from GetArrayofTextFrmElement method");
		return null;
	}

	public boolean NotExist(By by, String Description) {
		boolean _status = false;
		try {
			logger.debug("  Entered into NotExist method");
			WebDriverWait wait = new WebDriverWait(driver, GlobalVaraibles.Default_ExtMaxWait/1000);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			List<WebElement> wb = CreateWebElements(by);
			if (wb.size() ==0)
			{
				logger.info(Description);
				SetupFCTestReport.GetInstance().pass(Description);
				_status = true;
			}
			else {
				AddScreenshortInReport(Description+ ": found in locator: "+ by);

			}
			logger.debug("  Left from NotExist method");
		} catch (Exception e) {
			AddScreenshortInReport("Error occured while verifying ["+Description +" ]"+ e);

		}
		return _status;
	}

	public boolean Exist(By by, String Description) {
		boolean _status = false;
		try {
			logger.debug("  Entered into Exist method");

			if (WaitForPresenceAndVisibilityOfElements(by)) {
				SetupFCTestReport.GetInstance().pass(Description);
				logger.info(Description);
				_status = true;
			} else {
				AddScreenshortInReport(Description + ": is not found for locator: " + by);

			}
			logger.debug("  Left from Exist method");
		} catch (Exception e) {
			AddScreenshortInReport("Error occured while verifying  for["+Description +" ]" + e);

		}
		return _status;
	}
	
	public  By UpdateLocatorMechanism(By by,String ReplaceValue,String ReplacebleValue)
	{
		By NewBy = null;
		try {
			String locatortype=by.toString().split(":",2)[0];
			String locator=by.toString().split(":",2)[1];
			locator.replaceAll(ReplaceValue, ReplacebleValue);
			switch(locatortype.toUpperCase())
			{
				case "BY.XPATH":
					NewBy=By.xpath(locator);
					break;
				case "BY.ID":
					NewBy=By.id(locator);
					break;
				case "BY.CLASSNAME":
					NewBy=By.className(locator);
					break;
				case "BY.NAME":
					NewBy=By.name(locator);
					break;
				case "BY.CSSSELECTOR":
					NewBy=By.cssSelector(locator);
					break;
				case "BY.TAGNAME":
					NewBy=By.tagName(locator);
					break;
			}
		}
		catch(Exception e)
		{
			AddScreenshortInReport("Error occured while Updating " + e);
		}
		return NewBy;
	}

	
}
