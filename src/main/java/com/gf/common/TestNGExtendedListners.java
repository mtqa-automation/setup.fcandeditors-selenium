package com.gf.common;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import com.gf.setupFC.MenuTab;


public class TestNGExtendedListners implements  ITestListener {
	String Author = null;
	String TestTitle = null;
	String Category = null;
	String Suite=GlobalVaraibles.SuiteName;
	
	
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		ITestNGMethod m1 = result.getMethod();
		System.out.println("test");
		if (m1.getConstructorOrMethod().getMethod().isAnnotationPresent(SFCReportCategory.class)) {
			// initialize metadata information.
				this.Author = m1.getConstructorOrMethod().getMethod().getAnnotation(SFCReportCategory.class).Author();
				this.TestTitle = m1.getConstructorOrMethod().getMethod().getName();
				if(!m1.getConstructorOrMethod().getMethod().getAnnotation(SFCReportCategory.class).Suite().trim().isEmpty())
				this.Suite=	m1.getConstructorOrMethod().getMethod().getAnnotation(SFCReportCategory.class).Suite();
				else
				this.Suite=GlobalVaraibles.SuiteName;	
				this.Category = m1.getConstructorOrMethod().getMethod().getAnnotation(SFCReportCategory.class).Category();
			}
		CommonScript.ReportStart(Suite, TestTitle, Category, Author);
		GlobalVaraibles.Input=CommonScript.Testdata(TestTitle);
	
		
		
	}
	
	
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		GlobalVaraibles.TestResult="PASSED";
		CommonScript.ReportEnd();
		GenericWebObjects.GetInstance().CloseBrowser();
	}
	
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		GlobalVaraibles.TestResult="FAILED";
		CommonScript.ReportEnd();
		GenericWebObjects.GetInstance().CloseBrowser();
	}
	
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		GlobalVaraibles.TestResult="SKIPPED";
		CommonScript.ReportEnd();
		GenericWebObjects.GetInstance().CloseBrowser();
	}
	
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	
	}
	
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
	}
	
	public void onFinish(ITestContext context) {
	
		
	}


}
