package com.gf.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author smallika
 *
 */
public interface webdriverfunctions {
	
/**
 * @param BrowserName
 * @return WebDriver
 */
public WebDriver Driver(String BrowserName);
/**
 * @return String
 */
public String GetParentWindow();
/**
 * @return boolean
 */
public boolean SwitchtoParentWindow();
/**
 * @return boolean
 */
public boolean SwitchtoNewWindow();
/**
 * 
 */
public boolean MinWait();
/**
 * 
 */
public boolean MediumWait();
/**
 * 
 */
public boolean MaxWait();
/**
 * @param TestCaseID
 * @param Action
 */
public String CaptureScreenShot(String TestCaseID);
/**
 * @param by
 * @return List<WebElement>
 */
public List<WebElement> CreateWebElements(By by);
/**
 * @param by
 * @return boolean
 */
//public boolean WaitForPresenceAndVisibilityOfElement(By by);
/**
 * @param by
 * @return boolean
 */
public boolean WaitForPresenceAndVisibilityOfElements(By by);
/**
 * @param by
 * @return boolean
 */
public boolean IsElementPresentAndVisible(By by);
/**
 * @param by
 * @return boolean
 */
public boolean WaitForElementToBeClickable(By by);
/**
 * @param by
 * @return
 */
public boolean IsElementEnabled(By by);
/**
 * @return boolean
 */
public boolean CloseTab();

}
