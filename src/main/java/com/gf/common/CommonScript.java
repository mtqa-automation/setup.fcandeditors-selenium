package com.gf.common;

import java.util.Map;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

public class CommonScript {

	private static Logger logger = Logger.getLogger(CommonScript.class);

	public static Map<String, String> Testdata(String TestName) {
		String TestFolderName = TestName;
		GlobalVaraibles.TestScriptID = TestFolderName.split("__")[0];
		//System.out.println("JIRA testing");
		return TestDataReader.Readtestdata(GlobalVaraibles.TestScriptID);
		

	}

	/**
	 * @param SuiteName
	 * @param TestName
	 * @param Categories
	 * @param Author
	 * @return
	 */
	public static boolean ReportStart(String SuiteName, String TestName, String Categories, String Author) {
		try {

			SetupFCTestReport.Endtest();
			logger.info("*********** ###############[" + TestName + " ]::[Test Script execution Started ]"
					+ " ###############***********");
			SetupFCTestReport.RemoveInstance();
			if (TestName.split("__").length > 1)
				SetupFCTestReport.GetInstance(SuiteName, TestName.split("__")[0], TestName.split("__")[1], Categories,
						Author);
			else
				SetupFCTestReport.GetInstance(SuiteName, TestName.split("__")[0], "", Categories, Author);

			return true;
		} catch (Exception e) {
			logger.error("Unable to start Report Logging:-" + e);
			return false;
		}
	}

	/**
	 * @param SuiteName
	 * @param TestName
	 * @param TestType
	 * @return
	 */
	public static boolean ReportStart(String SuiteName, String TestName, String TestType) {
		return ReportStart(SuiteName, TestName, TestType, "");
	}

	public static void ReportEnd() {
		SetupFCTestReport.Endtest();
		logger.info("*********** ###############[Test Script execution End ]" + "###############*********");

	}

	public static void SuiteReportEnd() {
		SetupFCTestReport.Endtest();
		SetupFCTestReport.RemoveTestInstance();
		logger.info("***********" + " Test Script and Suite execution End" + "*********");
	}

	public static boolean VerifyContent(String ReferencePattern, String Referencematcher) {
		boolean _status = false;

		try {
			ReferencePattern = ReferencePattern.replaceAll("\\s|\n|\r|\t", "");
			Referencematcher = Referencematcher.replaceAll("\\s|\n|\r|\t", "");
			_status = Pattern.compile(ReferencePattern, Pattern.LITERAL).matcher(Referencematcher).find();
		} catch (Exception e) {
			logger.error("Error occured while verifying the content: " + e);
		}

		return _status;
	}

	public static boolean StringToBoolean(String Flag) {
		boolean flag = false;
		try {
			if (Flag != null && !Flag.isEmpty() && Flag.equalsIgnoreCase("TRUE"))
				flag = true;
		} catch (Exception e) {
			logger.error("Error occured while converting string to boolean and returning default flag False: " + e);
		}
		return flag;
	}
}
