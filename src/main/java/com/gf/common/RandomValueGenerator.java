package com.gf.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import org.apache.log4j.Logger;

public class RandomValueGenerator {
	private final static String charList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private final static String digitList = "1234567890";
	private final static int randomStringLength = 5;
	private static Logger logger = Logger.getLogger(RandomValueGenerator.class);
	
	public static void main(String[] args) {
		System.out.println(RandomValueGenerator.GenerateRandomNumber() + "--" + RandomValueGenerator.GenerateRandomString());
}
	private static int GetRandomNumber() {
		logger.debug("\nEntered into GetRandomNumber method");
		int randomInt = 0;
		Random randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(digitList.length());
		if (randomInt - 1 == -1) {
			logger.debug("Return Value for GetRandomNumber : " + randomInt);
			logger.debug("Left from GetRandomNumber1 method\n");
			return randomInt;
		} else {
			logger.debug("Return Value for GetRandomNumber : " + randomInt);
			logger.debug("Left from GetRandomNumber1 method\n");
			return randomInt - 1;
		}
	}

	public static String GenerateRandomString() {
		logger.debug("\nEntered into GenerateRandomString method");
		StringBuilder randStr = new StringBuilder();
		//randStr.append("AutoTest_");
		for (int index = 0; index < randomStringLength; index++) {
			int number = (int) (charList.length() * Math.random());
			char ch = charList.charAt(number);
			randStr.append(ch);
		}
		logger.debug("Return Value for GenerateRandomString : " + randStr.toString());
		logger.debug("Left from GenerateRandomString method\n");
		return randStr.toString();
	}

	public static String GenerateRandomNumber() {
		logger.debug("\nEntered into GenerateRandomNumber method");
		StringBuilder randStr = new StringBuilder();
		for (int index = 0; index < randomStringLength; index++) {
			int number = GetRandomNumber();
			char ch = digitList.charAt(number);
			randStr.append(ch);

		}
		logger.debug("Return Value for GenerateRandomNumber : " + randStr.toString());
		logger.debug("Left from GenerateRandomNumber method\n");
		return randStr.toString();
	}

	
	public static boolean AssignRandomValues() {
		
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		String formatDateTime = now.format(format);
		
		boolean _status=false;
		try {
			GlobalVaraibles.RandomAutoSpec +=RandomValueGenerator.GenerateRandomNumber()+RandomValueGenerator.GenerateRandomString();
			//GlobalVaraibles.RandomAutoSpec += formatDateTime;
			GlobalVaraibles.RandomAutoConfig +=RandomValueGenerator.GenerateRandomNumber()+RandomValueGenerator.GenerateRandomString();
			GlobalVaraibles.RandomValue1 +=RandomValueGenerator.GenerateRandomNumber()+RandomValueGenerator.GenerateRandomString();
			GlobalVaraibles.RandomValue2 +=RandomValueGenerator.GenerateRandomNumber()+RandomValueGenerator.GenerateRandomString();
			GlobalVaraibles.RandomValue3 +=RandomValueGenerator.GenerateRandomNumber();
			GlobalVaraibles.RandomValue4 +=RandomValueGenerator.GenerateRandomNumber();
			GlobalVaraibles.AutoG_Number1 =Integer.parseInt(RandomValueGenerator.GenerateRandomNumber());
			GlobalVaraibles.AutoG_LongNumber1 =Long.parseLong(RandomValueGenerator.GenerateRandomNumber());
			
			String SectionName=GlobalVaraibles.ExecutionSection;
			IniFile.GetInstance().WriteIniFile(SectionName, "EnvironmentRandomSpecName", GlobalVaraibles.RandomAutoSpec);
			IniFile.GetInstance().WriteIniFile(SectionName, "EnvironmentRandomConfigName", GlobalVaraibles.RandomAutoConfig);
			IniFile.GetInstance().WriteIniFile(SectionName, "EnvironmentRandomValue1", GlobalVaraibles.RandomValue1);
			IniFile.GetInstance().WriteIniFile(SectionName, "EnvironmentRandomValue2", GlobalVaraibles.RandomValue2);
			IniFile.GetInstance().WriteIniFile(SectionName, "EnvironmentRandomValue3", GlobalVaraibles.RandomValue3);
			IniFile.GetInstance().WriteIniFile(SectionName, "EnvironmentRandomValue4", GlobalVaraibles.RandomValue4);
			_status=true;
		}
		catch(Exception e)
		{
			logger.error("Error occurred while assigning the random values " + e);
		}
		return _status;
	}
}
