package com.gf.common;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class TestDataReader {
	private static Workbook workbook;
	private static Logger logger = Logger.getLogger(TestDataReader.class);
	private static String ExecutionSection = GlobalVaraibles.ExecutionSection;
	private static String TestDataExcelFile = GlobalVaraibles.TestDataFilePath+"//"+GlobalVaraibles.TestDataFileName;
			

	private static Map<String, String> readExceldata(String TestCaseID) {
		Map<String, String> Testdata = new LinkedHashMap<String, String>();
		try {

			Sheet testdataSheet = getExcelSheet(TestDataExcelFile,
			IniFile.GetInstance().ReadIniFile(ExecutionSection, "TestDataSheet"));
			int temp;
			Cell[] UniqueIDColumn = testdataSheet.getColumn(0);
			for (Cell cell : UniqueIDColumn)
				if (cell.getContents().trim().equalsIgnoreCase(TestCaseID)) {
					temp = cell.getRow();
					Cell[] HeaderRow = testdataSheet.getRow(0);
					Cell[] TestdataRow = testdataSheet.getRow(temp);
					int t1 = 0;
					for (Cell cell1 : TestdataRow) {
						if (cell1.getContents().startsWith("Environment"))
							Testdata.put(HeaderRow[t1].getContents().trim(),
									IniFile.GetInstance().ReadIniFile(ExecutionSection, cell1.getContents().trim()));
						else
							Testdata.put(HeaderRow[t1].getContents().trim(), cell1.getContents().trim());
						t1++;
					}
					return Testdata;
				}
			logger.warn("[ " + TestCaseID + " ]  is not found in Test data file");
		} catch (Exception e) {
			logger.error("Failed to read the test data for : " + TestCaseID + " due to :-" + e);
		}
		finally
		{
			if(workbook!=null)
				workbook.close();
		}
		return Testdata;
	}

	private static Sheet getExcelSheet(String FileName, String SheetName) {
		try {
			workbook = Workbook.getWorkbook(new File(FileName));
			Sheet sheet = workbook.getSheet(SheetName);
			return sheet;
		} catch (Exception e) {
			
			throw new SetupFCException("Failed while reading the excel sheet",e);
		}
	}

	public static Map<String, String> Readtestdata(String TestCaseID) {
		return readExceldata(TestCaseID);
	}
}
